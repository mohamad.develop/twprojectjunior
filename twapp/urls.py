from django.urls import path
from . import views


urlpatterns = [

    # Learner
    path('users/register', views.learner_register, name='learner_register'),
    path('users/verify_email', views.verify_by_email, name='verify_by_email'),
    path('users/authenticate', views.app_authenticate, name='authenticate'),
    path('users/send_verify_email', views.send_verify_email, name='send_verify_email'),
    path('users/change_password', views.app_change_password, name='change_password'),
    path('users/forget_password', views.forget_password, name='forget_password'),
    path('users/change_forgotten_password', views.change_forgotten_password, name='change_forgotten_password'),
    path('users/profile_pic', views.profile_pic, name='profile_pic'),

    path('api/learner/detail', views.learner_detail, name='learner_detail'),
    path('api/learner/wallet', views.learner_assets, name='learner_assets'),
    path('api/learner/courses', views.learner_courses, name='learner_courses'),
    path('api/learner/challenges', views.learner_challenges, name='learner_challenges'),

    path('api/learner/detail', views.learner_detail, name='learner_detail'),
    path('api/learner/wallet', views.learner_assets, name='learner_assets'),
    path('api/learner/courses', views.learner_courses, name='learner_courses'),
    path('api/learner/challenges', views.learner_challenges, name='learner_challenges'),

    # API: Explore
    path('api/content/explore', views.content_explore, name='content_explore'),
    path('api/content/explore/sessionless', views.sessionless_content_explore, name='sessionless_content_explore'),
    path('api/content/explore/sessionless2', views.sessionless_content_explore, name='sessionless_content_explore'),
    path('api/content/explore/beta', views.content_explore_beta, name='content_explore_beta'),

    # API: Profile
    path('api/learner/profile', views.learner_profile, name='learner_profile'),
    path('api/learner/profile_list', views.profile_list, name='profile_list'),
    path('api/learner/profile_pic', views.profile_pic, name='profile_pic'),
    path('api/learner/change_language', views.change_language, name='change_language'),
    path('api/learner/update', views.user_edit, name='user_edit'),
    path('api/learner/push_token', views.push_token, name='push_token'),

    # API: Challenge
    path('api/challenge/detail', views.challenge_info, name='challenge_info'),
    path('api/challenge/list_by_cat', views.category_challenges, name='category_challenges'),
    path('api/challenge/archived_list_by_cat', views.category_archived_challenges, name='category_archived_challenges'),
    path('api/challenge/make_submission', views.make_submission, name='make_submission'),

    # API: Ticket
    path('api/ticket/create', views.create_ticket, name='create_ticket'),
    path('api/ticket/list', views.ticket_list, name='ticket_list'),
    path('api/ticket/detail', views.ticket_detail, name='ticket_detail'),
    path('api/ticket/send', views.send_ticket_message, name='send_ticket_message'),
    path('api/ticket/allowed_course_instructors', views.allowed_course_instructors, name='allowed_course_instructors'),

    # API: SessionLess
    path('api/sessionless/explore', views.sessionLessExplore, name='sessionLessExplore'),
    path('api/sessionless/landing', views.sessionLessLanding, name='sessionLessLanding'),

    # API: Instructor
    path('api/instructor/all', views.all_instructors, name='all_instructors'),

    # API: Category
    path('api/category/all', views.all_categories, name='all_categories'),
    path('api/category/parents', views.parent_categories, name='parent_categories'),
    path('api/category/leafs', views.leaf_categories, name='leaf_categories'),

    # API: Content
    path('api/content/detail', views.content_info, name='contenet_info'),
    path('api/content/related', views.related_posts, name='related_posts'),
    path('api/content/comments', views.get_comments, name='get_comments'),
    path('api/content/add_comment', views.add_comment, name='add_comment'),
    path('api/content/list_by_cat', views.category_content, name='category_content'),
    path('api/content/mine_by_cat', views.category_content_mine, name='category_content'),
    path('api/content/mine', views.my_content, name='my_content'),
    path('api/content/create', views.create_content, name='create_content'),
    path('api/content/edit', views.content_edit, name='content_edit'),
    path('api/content/delete', views.content_delete, name='content_delete'),
    path('api/content/like_toggle', views.like_toggle, name='like_toggle'),

    # API: Tag
    path('api/tags/all', views.all_tags, name='all_tags'),
    path('api/skilltags/all', views.all_skill_tags, name='all_skill_tags'),

    # API: Episode
    path('api/episode/detail', views.app_episode_detail, name='app_episode_detail'),
    path('api/episode/next_link', views.app_episode_next, name='app_episode_next'),
    path('api/episode/like_toggle', views.episode_like_toggle, name='episode_like_toggle'),
    path('api/episode/stream_video', views.stream_video, name='stream_video'),
    path('api/episode/episodes_dummy', views.episodes_dummy, name='episodes_dummy'),

    # API: Question
    path('api/question/detail', views.app_example_detail, name='app_example_detail'),
    path('api/question/detail/items', views.app_example_item_detail, name='app_example_item_detail'),
    path('api/question/submit', views.app_submit_question, name='app_submit_question'),
    path('api/run/status', views.app_run_status, name='app_run_status'),

    # API: User
    path('api/user/app_google_auth', views.app_google_auth, name='app_google_auth'),
    path('api/user/app_google_auth_2', views.app_google_auth_2, name='app_google_auth_2'),
    path('api/user/all_notifications', views.all_notifications, name='all_notifications'),
    path('api/notification/toggle', views.toggle_notification, name='toggle_notification'),
    path('api/notification/send_push', views.send_push, name='send_push'),

    # API: Course
    path('api/course/all_by_category', views.all_courses, name='all_courses'),
    path('api/course/mine_by_category', views.learner_courses_by_cat, name='learner_courses_by_cat'),
    path('api/course/attend', views.attend_to_course, name='attend_to_course'),
    path('api/course/detail', views.app_course_detail, name='app_course_detail'),
    path('api/course/top', views.top_courses, name='top_courses'),

    # API: ShortCut
    path('api/shortcut/shortcut_questions', views.shortcut_questions, name='shortcut_questions'),
    path('api/shortcut/answer', views.shortcut_answer, name='shortcut_questions'),
    path('api/shortcut/apply', views.shortcut_apply, name='shortcut_questions'),

    # API: Social
    path('api/social/follow_toggle', views.follow_unfollow, name='follow_unfollow'),

    # API: Project
    path('api/project/create_project', views.create_project, name='create_project'),
    path('api/project/project_detail', views.project_detail, name='project_detail'),
    path('api/project/project_full_detail', views.project_full_detail, name='project_full_detail'),
    path('api/project/project_list', views.project_list, name='project_list'),
    path('api/project/my_project_list', views.my_project_list, name='my_project_list'),
    path('api/project/proposal_list', views.proposal_list, name='proposal_list'),
    path('api/project/my_proposals', views.my_proposals, name='my_proposals'),
    path('api/project/make_bid', views.make_bid, name='make_bid'),
    path('api/project/delete_proposal', views.delete_proposal, name='delete_proposal'),
    path('api/project/proposal_edit', views.proposal_edit, name='proposal_edit'),

    # API: Subject
    path('api/subject/subject_list', views.subject_list, name='subject_list'),
    path('api/subject/subject_detail', views.subject_detail, name='subject_detail'),

    # API: Webinar
    path('api/webinar/webinar_list', views.webinar_list, name='webinar_list'),
    path('api/webinar/my_webinars', views.my_webinars, name='my_webinars'),
    path('api/webinar/webinar_detail', views.webinar_detail, name='webinar_detail'),

    # API: Discuss
    path('api/discuss/create_thread', views.create_thread, name='create_thread'),
    path('api/discuss/answer_to_thread', views.answer_to_thread, name='answer_to_thread'),
    path('api/discuss/reply_to_answer', views.reply_to_answer, name='reply_to_answer'),
    path('api/discuss/discuss_vote', views.discuss_vote, name='discuss_vote'),
    path('api/discuss/thread_detail', views.thread_detail, name='thread_detail'),
    path('api/discuss/thread_list', views.thread_list, name='thread_list'),
    path('api/discuss/my_threads', views.my_threads, name='my_threads'),
    path('api/discuss/thread_edit', views.thread_edit, name='thread_edit'),
    path('api/discuss/thread_delete', views.thread_delete, name='thread_delete'),

    # API: Weekly League
    path('api/league/register_weekly_league', views.register_weekly_league, name='register_weekly_league'),
    path('api/league/weekly_league_leaderboard', views.weekly_league_leaderboard, name='weekly_league_leaderboard'),
    path('api/league/list_by_cat', views.weekly_league_list, name='weekly_league_list'),

    # API: Super League
    path('api/superleage/detail', views.super_league_detail, name='super_league_teams'),
    path('api/superleage/list_by_cat', views.super_league_list, name='super_league_list'),
    path('api/superleage/teams', views.super_league_teams, name='super_league_teams'),
    path('api/superleage/team/detail', views.super_league_team_detail, name='super_league_team_detail'),
    path('api/superleage/team/ceate_info', views.super_league_team_create_info, name='super_league_team_create_info'),
    path('api/superleage/team/create', views.super_league_create_team, name='super_league_create_team'),
    path('api/superleage/team/delete', views.super_league_team_delete, name='super_league_team_delete'),
    path('api/superleage/team/update', views.super_league_update_team, name='super_league_update_team'),
    path('api/superleage/team/leave', views.super_league_leave_team, name='super_league_leave_team'),
    path('api/superleage/team/updatelogo', views.super_league_team_logo, name='super_league_team_logo'),
    path('api/superleage/team/invite', views.super_league_invite_learner, name='super_league_invite_learner'),
    path('api/superleage/invite/accept', views.super_league_accept_invite, name='super_league_accept_invite'),
    path('api/superleage/invite/reject', views.super_league_reject_invite, name='super_league_reject_invite'),
    path('api/superleage/team/request', views.super_league_team_request, name='super_league_team_request'),
    path('api/superleage/request/cancel', views.super_league_cancel_request, name='super_league_cancel_request'),
    path('api/superleage/request/accept', views.super_league_accept_request, name='super_league_team_request'),
    path('api/superleage/request/reject', views.super_league_reject_request, name='super_league_team_request'),

    # API: Search
    path('api/search/simple', views.simple_search, name='simpleSearch'),

    # API: Payment
    path('api/payment/apply_code', views.apply_promo_code, name='apply_promo_code'),
    path('api/payment/create_code', views.create_code, name='create_code'),
    path('api/payment/course_invoice', views.course_invoice, name='course_invoice'),
    path('api/payment/subject_invoice', views.subject_invoice, name='subject_invoice'),
    path('api/payment/pay_link', views.pay_link, name='pay_link'),

    # API: Competition
    path('api/competition/detail', views.competition_detail, name='competition_detail'),
    path('api/competition/mine_by_category', views.category_competitions, name='category_competitions'),
    path('api/competition/create_info', views.pre_create_info, name='pre_create_info'),
    path('api/competition/create', views.create_competition, name='create_competition'),
    path('api/competition/cancel', views.competition_cancel, name='competition_cancel'),
    path('api/competition/accept', views.competition_accept, name='competition_accept'),
    path('api/competition/random', views.competition_random, name='competition_random'),
    path('api/competition/reject', views.competition_reject, name='competition_reject'),
    path('api/competition/start', views.competition_start, name='competition_start'),
    path('api/competition/invite', views.competition_invite, name='competition_invite'),
    path('api/competition/submit', views.competition_submit_question, name='competition_submit_question'),

]