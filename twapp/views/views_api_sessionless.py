from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Course, Content, Wallet, Category
from twapp.serializers import WalletSeralizer, \
    SessionLessContentSerializer, \
    CourseListSerializer, CategorySerializer

from django.utils import translation


@api_view(['GET'])
def sessionLessExplore(request):
    try:

        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

        final = {}

        # Contents
        contents = Content.objects.all().filter(status='published')[:5]
        serializer = SessionLessContentSerializer(contents, many=True)
        final["contents"] = serializer.data

        # featuredContents
        contents = Content.objects.all().filter(status='published',featured=True)[:2]
        serializer = SessionLessContentSerializer(contents, many=True)
        final["featuredContents"] = serializer.data

        # Courses
        courses = Course.objects.all().filter(verified=True,featured=True)[:3]
        serializer = CourseListSerializer(courses, many=True)
        final["courses"] = serializer.data

        # Categories
        categories = Category.objects.all().filter(isLeaf=True)
        serializer = CategorySerializer(categories, many=True)
        final["categories"] = serializer.data

        return Response(final, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
def sessionLessLanding(request):
    try:
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        final = {}

        # Contents
        contents = Content.objects.all().filter(status='published')[:5]
        serializer = SessionLessContentSerializer(contents, many=True)
        final["contents"] = serializer.data

        # featuredContents
        contents = Content.objects.all().filter(status='published',featured=True)[:2]
        serializer = SessionLessContentSerializer(contents, many=True)
        final["featuredContents"] = serializer.data

        try:
            userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
            learner = userSession.user.learner
            level = {}

            level['currentStreak'] = learner.dailyStreak
            level['currentForumActivity'] = learner.forumActivity
            level['currentPoints'] = learner.points
            level['maxForumActivity'] = 25
            level['maxPoints'] = 46
            level['maxStreak'] = 15
            level['percentage'] = 27

            final["levelStatistic"] = level

            # User
            user = {}
            user["username"] = learner.user.username
            user["learnerKey"] = learner.learnerKey
            user['firstName'] = learner.firstName
            user['lastName'] = learner.lastName
            user['isPro'] = learner.isPro
            user['isPrivate'] = learner.isPrivate
            user['profilePicture'] = learner.profilePicture.url
            user['profilePictureThumb'] = learner.profilePictureThumb.url
            user['level'] = learner.level
            final["user"] = user
            final["learnerKey"] = learner.learnerKey

            # Wallet
            wallet = Wallet.objects.all().filter(learner=learner)[0]
            serializer = WalletSeralizer(wallet, many=False)
            final["wallet"] = serializer.data


        except Exception as e:
            level = {}

            level['currentStreak'] = 0
            level['currentForumActivity'] = 0
            level['currentPoints'] = 0
            level['maxForumActivity'] = 25
            level['maxPoints'] = 46
            level['maxStreak'] = 15
            level['percentage'] = 27

            final["levelStatistic"] = level

            # User
            user = {}
            user["username"] = ""
            user["learnerKey"] = ""
            user['firstName'] = ""
            user['lastName'] = ""
            user['isPro'] = ""
            user['isPrivate'] = ""
            user['profilePicture'] = ""
            user['profilePictureThumb'] = ""
            user['level'] = ""
            final["user"] = user
            final["learnerKey"] = ""

            # Wallet
            final["wallet"] = ""


        return Response(final, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

