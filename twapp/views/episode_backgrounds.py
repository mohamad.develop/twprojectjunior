import os
import subprocess
from django.conf import settings
from moviepy.editor import VideoFileClip
import random
import string

def convert_to_hls(episode):
    # Set the output directory for HLS files
    output_dir = os.path.join(settings.MEDIA_ROOT, 'hls')
    os.makedirs(output_dir, exist_ok=True)

    # Set the input video file path
    input_file_path = episode.videoFile.path

    # Set the output filename and path for the HLS playlist
    # filename = os.path.splitext(os.path.basename(input_file_path))[0]
    characters = string.ascii_letters + string.digits
    filename = ''.join(random.choice(characters) for _ in range(45))
    playlist_path = os.path.join(output_dir, f'{filename}.m3u8')

    # Set the output path for the HLS segments
    segments_path = os.path.join(output_dir, f'{filename}_%05d.ts')

    # Construct the ffmpeg command for HLS conversion
    command = [
        'ffmpeg',
        '-i', input_file_path,
        '-c:v', 'libx264',
        '-crf', '23',
        '-preset', 'medium',
        '-c:a', 'aac',
        '-b:a', '128k',
        '-f', 'hls',
        '-hls_time', '10',
        '-hls_list_size', '0',
        '-max_muxing_queue_size','9999',
        '-hls_segment_filename', segments_path,
        playlist_path
    ]
    # Execute the ffmpeg command
    subprocess.run(command, check=True)

    episode.hls_created = True
    episode.hls_url = playlist_path.replace("/home/debian/TWProject","")
    episode.save()

    return True

# import os
# from django.conf import settings
# from moviepy.editor import VideoFileClip
#
# def convert_to_hls(episode):
#     # Set the output directory for HLS files
#     output_dir = os.path.join(settings.MEDIA_ROOT, 'hls')
#     os.makedirs(output_dir, exist_ok=True)
#
#     # Load the video clip
#     clip = VideoFileClip(episode.videoFile.path)
#
#     # Set the output filename and path for the HLS playlist
#     filename = os.path.splitext(os.path.basename(episode.videoFile.path))[0]
#     playlist_path = os.path.join(output_dir, f'{filename}.m3u8')
#
#     # Set the output path for the HLS segments
#     segments_path = os.path.join(output_dir, f'{filename}_%05d.ts')
#
#     # Convert the video to HLS format using FFmpeg
#     clip.write_hls(playlist_path, segments_path)
#
#     episode.hls_created=True
#     episode.hls_url=playlist_path
#     episode.save()
#
#     return True