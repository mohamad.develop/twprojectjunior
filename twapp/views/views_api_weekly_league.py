from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Course, LeagueRecord, League, Category
from twapp.serializers import LeagueSeralizer, LeagueRecordSerializer

from django.utils import translation


@api_view(['POST'])
def register_weekly_league(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        league = League.objects.all().filter(leagueKey=request.data['leagueKey'])[0]

        league.participants.add(learner)
        league.save()

        if LeagueRecord.objects.all().filter(league=league,learner=learner).exists():
            return Response({"status": "error", "object": "already registred"}, status=status.HTTP_401_UNAUTHORIZED)

        leagueRecord= LeagueRecord(league=league,learner=learner)
        leagueRecord.save()

        serializer = LeagueSeralizer(league, many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def weekly_league_leaderboard(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        league = League.objects.all().filter(leagueKey=request.data['leagueKey'])[0]

        ranks= LeagueRecord.objects.all().filter(league=league).order_by('rank')

        serializer = LeagueRecordSerializer(ranks, many=True,context={'learnerKey': learner.learnerKey})

        serializer_l = LeagueSeralizer(league,many=False,context={'learnerKey': learner.learnerKey})

        return Response({"league":serializer_l.data, "leaderboard":serializer.data}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def weekly_league_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]
        courses = Course.objects.all().filter(category=category,verified=True)

        wl_list = League.objects.all().filter(course__in=courses)

        serializer = LeagueSeralizer(wl_list, many=True, context={'learnerKey': learner.learnerKey})
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)