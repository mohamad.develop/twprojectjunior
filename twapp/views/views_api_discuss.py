import json
import threading

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Category, Tag, Thread, SubThread, ThreadReply, Log
from twapp.serializers import ThreadSerializer, \
    SubThreadSerializer, ReplySerializer, ThreadListSerializer
from .discssion_backgounds import update_discussion_stats

def tags_update(thread_, tags_):

    if not tags_ == '':
        tgs = json.loads(tags_)
        thread_.tags.clear()
        thread_.save()
        for item in tgs:
            tag_ = Tag.objects.all().filter(tagKey=item)[0]
            thread_.tags.add(tag_)
        thread_.save()
    else:
        thread_.tags.clear()
        thread_.save()


@api_view(['POST'])
def thread_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]
        thread.views = thread.views+1
        thread.save()

        serializer = ThreadSerializer(thread, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def thread_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        sort=request.data['sort']

        threads = Thread.objects.all().filter(category=category)

        if sort=="time":
            threads = Thread.objects.all().filter(category=category).order_by('time').reverse()
        elif sort=="votes":
            threads = Thread.objects.all().filter(category=category).order_by('upVotes').reverse()
        elif sort=="views":
            threads = Thread.objects.all().filter(category=category).order_by('views').reverse()

        st = threading.Thread(target=update_discussion_stats, args=(learner,))
        st.start()


        serializer = ThreadListSerializer(threads, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def my_threads(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        sort = request.data['sort']

        threads = Thread.objects.all().filter(category=category, learner=learner)

        if sort == "time":
            threads = Thread.objects.all().filter(category=category, learner=learner).order_by('time').reverse()
        elif sort == "votes":
            threads = Thread.objects.all().filter(category=category, learner=learner).order_by('upVotes').reverse()
        elif sort == "views":
            threads = Thread.objects.all().filter(category=category, learner=learner).order_by('views').reverse()

        serializer = ThreadListSerializer(threads, many=True, context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)





@api_view(['POST'])
def create_thread(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        keys=request.data.keys()

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        thread = Thread( body=request.data['description'], learner=learner, title=request.data['title'],
                                category=category)
        thread.save()

        if 'threadFile' in keys:
            file = request.data['threadFile']
            thread.threadFile=file
            thread.save()

        x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
        x.start()

        serializer = ThreadSerializer(thread, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def answer_to_thread(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        keys=request.data.keys()
        thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]

        subthread = SubThread(body=request.data['description'], learner=learner)
        subthread.save()

        if 'threadFile' in keys:
            file = request.data['threadFile']
            subthread.threadFile=file
            subthread.save()

        thread.subthreads.add(subthread)
        thread.save()

        # x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
        # x.start()

        serializer = SubThreadSerializer(subthread, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def reply_to_answer(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        keys=request.data.keys()
        thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]

        subthread = SubThread.objects.all().filter(subthreadKey=request.data['subthreadKey'])[0]

        rpl = ThreadReply(body=request.data['description'], learner=learner)
        rpl.save()

        if 'threadFile' in keys:
            file = request.data['threadFile']
            rpl.threadFile=file
            rpl.save()

        subthread.replies.add(rpl)
        subthread.save()

        # x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
        # x.start()

        serializer = ReplySerializer(rpl, many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def discuss_vote(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        type=request.data['voteType']
        side=request.data['voteSide']

        if type=='thread':
            try:
                thread = Thread.objects.all().filter(threadKey=request.data['key'])[0]

                if Log.objects.all().filter(action="Vote_"+side,thread=thread,learner=learner).exists():
                    return Response(
                        {"status": "OK", "object": "Already Voted!", "vote": (thread.upVotes - thread.downVotes)},
                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    Log.objects.all().filter(thread=thread,learner=learner).delete()

                if side=="up":
                    thread.upVotes=thread.upVotes +1
                    thread.save()
                elif side=="down":
                    thread.downVotes=thread.downVotes +1
                    thread.save()
                else:
                    return Response({"status": "error", "object": "Undefined Side!"},
                                    status=status.HTTP_400_BAD_REQUEST)

                log= Log(action="Vote_"+side,thread=thread,learner=learner)
                log.save()


                thread.upVotes = Log.objects.all().filter(action="Vote_up",thread=thread,learner=learner).count()
                thread.downVotes = Log.objects.all().filter(action="Vote_down",thread=thread,learner=learner).count()
                thread.save()

                return Response({"status": "OK", "object": "Voted!", "vote":(thread.upVotes-thread.downVotes)}, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)

        if type=='subthread':
            try:
                subthread = SubThread.objects.all().filter(subthreadKey=request.data['key'])[0]

                if Log.objects.all().filter(action="Vote_"+side,subThread=subthread,learner=learner).exists():
                    return Response(
                        {"status": "OK", "object": "Already Voted!", "vote": (subthread.upVotes - subthread.downVotes)},
                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    Log.objects.all().filter(subThread=subthread,learner=learner).delete()

                if side=="up":
                    subthread.upVotes=subthread.upVotes +1
                    subthread.save()
                elif side=="down":
                    subthread.downVotes=subthread.downVotes +1
                    subthread.save()
                else:
                    return Response({"status": "error", "object": "Undefined Side!"},
                                    status=status.HTTP_400_BAD_REQUEST)

                log= Log(action="Vote_"+side,subThread=subthread,learner=learner)
                log.save()

                subthread.upVotes = Log.objects.all().filter(action="Vote_up",subThread=subthread,learner=learner).count()
                subthread.downVotes = Log.objects.all().filter(action="Vote_down",subThread=subthread,learner=learner).count()
                subthread.save()

                return Response({"status": "OK", "object": "Voted!", "vote":(subthread.upVotes-subthread.downVotes)}, status=status.HTTP_200_OK)
            except Exception as e:
                # return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)
                return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)

        if type=='reply':
            try:
                rpl = ThreadReply.objects.all().filter(replyKey=request.data['key'])[0]

                if Log.objects.all().filter(action="Vote_"+side,threadReply=rpl,learner=learner).exists():
                    return Response({"status": "OK", "object": "Already Voted!", "vote": (rpl.upVotes - rpl.downVotes)},
                                    status=status.HTTP_400_BAD_REQUEST)
                else:
                    Log.objects.all().filter(threadReply=rpl, learner=learner).delete()

                if side=="up":
                    rpl.upVotes=rpl.upVotes +1
                    rpl.save()
                elif side=="down":
                    rpl.downVotes=rpl.downVotes +1
                    rpl.save()
                else:
                    return Response({"status": "error", "object": "Undefined Side!"},
                                    status=status.HTTP_400_BAD_REQUEST)

                log= Log(action="Vote_"+side,threadReply=rpl,learner=learner)
                log.save()

                rpl.upVotes = Log.objects.all().filter(action="Vote_up",threadReply=rpl, learner=learner).count()
                rpl.downVotes = Log.objects.all().filter(action="Vote_down",threadReply=rpl, learner=learner).count()
                rpl.save()

                return Response({"status": "OK", "object": "Voted!", "vote":(rpl.upVotes-rpl.downVotes)}, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)



    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def thread_edit(request):

    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]
        keys=request.data.keys()

        for key in keys:
            if key == "title":
                thread.title = request.data["title"]
                thread.save()
            if key == "body":
                thread.body = request.data["description"]
                thread.save()
            if key == "threadFile":
                file = request.data['threadFile']
                thread.contentFile = file
                thread.save()

        x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
        x.start()
        serializer = ThreadSerializer(thread, many=False,context={'learnerKey': learner_.learnerKey})

        return Response({"status":"OK","updated":keys,"object":serializer.data},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def thread_delete(request):

    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]
        thread.delete()
        return Response({"status":"OK","object":"deleted"},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

