from pyfcm import FCMNotification
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from TWProject import settings
from twapp.models import AppSession, Learner, Follow, Notification
from twapp.serializers import LearnerSeralizer

import threading
from .FCM_notif import create_notif

# To Remove
def create_follow_notif(learner, follower_learner):
    push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)

    info = push_service.get_registration_id_info(learner.pushToken)
    device_type = info

    message_title = "Techwich Test"
    message_body = " Body body body"
    registration_id = learner.pushToken
    data_message = {
        "title": "You have a new follower!",
        "body": "Keep up the great content and grow your community!",
        "type": 1,
        "context":
            {"learnerKey": follower_learner.learnerKey}
    }

    if info['platform'] == "ANDROID":
        result = push_service.notify_single_device(registration_id=registration_id, message_title=None,
                                                   message_body=None, data_message=data_message)
    else:
        result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                   message_body=message_body, data_message=data_message)
    return True

@api_view(['POST'])
def follow_unfollow(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
        follower_learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        if Follow.objects.all().filter(followerKey=follower_learner.learnerKey,followedKey=request.data['learnerKey']).exists():
            follow_status = Follow.objects.all().filter(followerKey=follower_learner.learnerKey,
                                                        followedKey=request.data['learnerKey'])
            follow_status.delete()

            return Response({"status":"unfollowed"}, status=status.HTTP_200_OK)

        else:
            follow=Follow(followerKey=follower_learner.learnerKey,followedKey=request.data['learnerKey'])
            follow.save()

            # Add Notification
            fu_learner=Learner.objects.all().filter(learnerKey=request.data['learnerKey'])[0]
            notification = Notification(toUser=fu_learner.user, icon=follower_learner.profilePicture,
                                        title=follower_learner.firstName + " " + follower_learner.lastName + " Followed you.",
                                        text=follower_learner.firstName + " " + follower_learner.learnerKey + " Followed you.")
            notification.save()

            sc = threading.Thread(target=create_notif, args=(fu_learner,
                                                             "You have a new follower!",
                                                             "Keep up the great content and grow your community!",
                                                             1,
                                                             {"learnerKey": follower_learner.learnerKey}))
            sc.start()

            return Response({"status":"followed"}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

