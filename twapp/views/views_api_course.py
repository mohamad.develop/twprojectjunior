from rest_framework.decorators import api_view

from twapp.models import AppSession, Category,Course, Attend,Log

from rest_framework import status
from rest_framework.response import Response
import threading

from twapp.serializers import LearnerSeralizer, AttendSerializer, CourseListSerializer, CourseDetailSerializer

from django.utils import translation


def createCourseAttned(learner,course):

    if not Log.objects.filter(action="unlocked", learner=learner, course=course).exists():
        first_lecture=course.courseLectures.all().order_by('-order').reverse()[0]
        first_episode=first_lecture.episodes.all().order_by('-order').reverse()[0]

        print(first_episode)
        print(first_lecture)
        lg = Log(action="unlocked", learner=learner, course=course, video=first_episode)
        lg.save()

    if not Attend.objects.filter(learner=learner, course=course).exists():
        attend = Attend(learner=learner, course=course)
        attend.save()

@api_view(['POST'])
def all_courses(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        courses = Course.objects.all().filter(category=category,verified=True)

        serializer = CourseListSerializer(courses, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def top_courses(request):
    try:
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

        courses = Course.objects.all().filter(verified=True).order_by('?')[:7]

        serializer = CourseListSerializer(courses, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def app_course_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)
        learner = userSession.user.learner
        course = Course.objects.all().filter(courseKey=request.data['courseKey'])[0]

        serializer = CourseDetailSerializer(course, many=False, context={'learnerKey': learner.learnerKey})
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def attend_to_course(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)
        learner = userSession.user.learner
        course = Course.objects.all().filter(courseKey=request.data['courseKey'])[0]

        create_attend = threading.Thread(target=createCourseAttned, args=(learner,course))
        create_attend.start()

        serializer = CourseDetailSerializer(course, many=False, context={'learnerKey': learner.learnerKey})
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def learner_courses_by_cat(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
        learner = userSession.user.learner
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]
        courses = Course.objects.all().filter(category=category,verified=True)

        attends = Attend.objects.all().filter(learner=learner, course__in=courses)
        serializer = AttendSerializer(attends, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

# @api_view(['POST'])
# def app_course_enroll(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         user_lang = request.headers['languageKey']
#         translation.activate(user_lang)
#
#         learner = userSession.user.learner
#         course = Course.objects.all().filter(courseKey=request.data['courseKey'])[0]
#
#         create_attend = threading.Thread(target=createAttned, args=(learner,episode))
#         create_attend.start()
#
#         serializer = CourseDetailSerializer(course, many=False, context={'learnerKey': learner.learnerKey})
#         return Response(serializer.data,status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
