from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Challenge, Category, Submission
from twapp.serializers import ChallengeSerializer, \
    ChallengeDetailSerializer


@api_view(['POST'])
def challenge_info(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        challenge = Challenge.objects.all().filter(challengeKey=request.data['challengeKey'])[0]
        serializer = ChallengeDetailSerializer(challenge, many=False, context={'learnerKey': learner.learnerKey})

        challenge.views = challenge.views + 1
        challenge.save()

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def category_challenges(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        challenges = Challenge.objects.all().filter(category=category)
        serializer = ChallengeSerializer(challenges, many=True, context={'learnerKey': learner.learnerKey})


        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def category_archived_challenges(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        challenges = Challenge.objects.all().filter(category=category,status=4)
        serializer = ChallengeSerializer(challenges, many=True)


        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def make_submission(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        file = request.data['attachment']
        challenge = Challenge.objects.all().filter(challengeKey=request.data['challengeKey'])[0]

        submission = Submission(attachment=file, description=request.data['description'], learner=learner,
                                challenge=challenge
                                )
        submission.save()

        serializer = ChallengeDetailSerializer(challenge, many=False)

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)
