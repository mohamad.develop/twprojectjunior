from rest_framework.decorators import api_view

from twapp.models import Example, AppSession, Run, Log, Attend, League, SuperLeague, LeagueRecord, Result

from rest_framework import status
from rest_framework.response import Response
import threading

from django.utils import translation

from twapp.serializers import ExampleDetailSerializer, RunSerializer, ExampleItemBasedDetailSerializer,FiguredOptionSerializer

def clean_text(text):
    ret=text.replace("\n","")
    ret=ret.strip()
    return ret.lower()


def leagueSuperLeagueRanks(course):
    weekly_leagues = League.objects.all().filter(course=course)
    super_leagues = SuperLeague.objects.all().filter(course=course)

    for wl in weekly_leagues:
        rec = LeagueRecord.objects.filter(league=wl).order_by("-score")
        print(rec)
        score_=rec[0].score
        rank_=1
        for r in rec:
            if score_!=r.score:
                rank_+=1
                score_ = r.score
            r.rank=rank_
            r.save()

    for sl in super_leagues:
        rec = Result.objects.filter(superLeague=sl).order_by("-score")
        score_=rec[0].score
        rank_=1
        for r in rec:
            if score_!=r.score:
                rank_+=1
                score_ = r.score

            r.rankChangedCount = abs(r.currentRank - rank_)
            r.rankStatus = (r.currentRank - rank_)/abs(r.currentRank - rank_)
            r.currentRank = rank_

            r.save()

    return True


def leagueSuperLeagueScores(learner, score, course):

    print(course)

    weekly_leagues = League.objects.all().filter(course=course)
    super_leagues = SuperLeague.objects.all().filter(course=course)

    print(weekly_leagues)
    print(super_leagues)

    try:
        for wl in weekly_leagues:
            if LeagueRecord.objects.all().filter(learner=learner, league=wl).exists():
                rec = LeagueRecord.objects.all().filter(learner=learner, league=wl)[0]
                rec.score = rec.score + score
                rec.save()
    except Exception as e:
        print(str(e))

    try:
        for sl in super_leagues:
            for tm in learner.learnerTeams.all():
                if Result.objects.all().filter(team=tm, superLeague=sl).exists():
                    rec = Result.objects.all().filter(team=tm, superLeague=sl)[0]
                    rec.score = rec.score + score
                    rec.save()
    except Exception as e:
        print(str(e))

    un = threading.Thread(target=leagueSuperLeagueRanks, args=(course,))
    un.start()

    return True

def unlockNextEpisode(learner,episode):
    lecture = episode.lecture_set.all()[0]
    course=lecture.parentCourse

    sibling_episodes=lecture.episodes.all().order_by('-order').reverse()
    sibling_lectures=course.courseLectures.all().order_by('-order').reverse()

    if episode.videoKey==sibling_episodes[len(sibling_episodes)-1].videoKey:
        next_lecture=lecture
        for i in range(0,len(sibling_lectures)):
            if sibling_lectures[i].lectureKey==lecture.lectureKey:
                next_lecture=sibling_lectures[i+1]
                break
        next_episode=next_lecture.episodes.all().order_by('-order').reverse()[0]
        lg = Log(action="unlocked", learner=learner, course=course, video=next_episode)
        lg.save()
    else:
        next_episode=episode
        for i in range(0,len(sibling_episodes)):
            if sibling_episodes[i].videoKey==episode.videoKey:
                next_episode=sibling_episodes[i+1]
                break
        lg = Log(action="unlocked", learner=learner, course=course, video=next_episode)
        lg.save()


def handleRunLogs(run):
    if run.verdict:

        if not Log.objects.filter(action="questionPassed", learner=run.learner, example=run.example).exists():
            lg = Log(action="questionPassed", learner=run.learner, example=run.example)
            lg.save()

            run.learner.points = run.learner.points + run.example.score
            run.learner.save()

            lecture = run.example.lecture_set.all()[0]

            sc = threading.Thread(target=leagueSuperLeagueScores, args=(run.learner,run.example.score,lecture.parentCourse))
            sc.start()

        lecture = run.example.lecture_set.all()[0]
        episode=run.example.relatedVideo


        if not Log.objects.filter(action="episodePassed", learner=run.learner, video=episode).exists():

            all_siblings=episode.EpisodeQuestions.all()
            all_sibling_passed=True

            for ex in all_siblings:
                if not Log.objects.filter(action="questionPassed", learner=run.learner, example=ex).exists():
                    all_sibling_passed = False
                    break

            if all_sibling_passed:
                lg = Log(action="episodePassed", learner=run.learner, video=episode)
                lg.save()

                un = threading.Thread(target=unlockNextEpisode, args=(run.learner,episode))
                un.start()

                all_passed = True
                for ep in lecture.episodes.all():
                    if not Log.objects.filter(action="episodePassed", learner=run.learner, video=ep).exists():
                        all_passed = False
                        break

                if all_passed:
                    if not Log.objects.filter(action="lessonPassed", learner=run.learner, lecture=lecture).exists():
                        lg = Log(action="lessonPassed", learner=run.learner, lecture=lecture)
                        lg.save()
                        attend = Attend.objects.all().filter(course=lecture.parentCourse, learner=run.learner)[0]
                        attend.lessonCompleted = attend.lessonCompleted + 1
                        attend.unlockedLessonOrder = lecture.order
                        attend.save()
    else:
        lg = Log(action="questionWrong", learner=run.learner, example=run.example)
        lg.save()
    return True


def handleRun(run):
    if run.example.type=='playground':
        run.verdict = (run.statusChecks == 2)
        if (run.statusChecks == 2):
            run.status = 'completed'
        run.save()
        h = threading.Thread(target=handleRunLogs, args=(run,))
        h.start()
        return True
    elif run.example.type=='multi':

        # print(run.code)
        # print(run.example.answer.split("\n"))

        user_answers = run.code
        true_answers = run.example.answer.split("\n")

        user_answers.sort()
        true_answers.sort()

        # print(user_answers)
        # print(true_answers)

        if len(true_answers) != len(user_answers):
            run.verdict = False
            run.status = 'completed'
            run.save()
            h = threading.Thread(target=handleRunLogs, args=(run,))
            h.start()
            return True

        final_verdict=True

        for i in range (0,len(user_answers)):
            if clean_text(user_answers[i]) != clean_text(true_answers[i]):
                final_verdict = False
                break

        run.verdict = final_verdict
        run.status = 'completed'
        run.save()
        h = threading.Thread(target=handleRunLogs, args=(run,))
        h.start()
        return True
    elif run.example.type=='single':
        run.verdict = (clean_text(run.code[0])==clean_text(run.example.answer))
        run.status = 'completed'
        run.save()
        h = threading.Thread(target=handleRunLogs, args=(run,))
        h.start()
        return True
    elif run.example.type=='pick':
        run.verdict = (clean_text(run.code[0].replace(" ","").replace(u'\xa0', u' '))==clean_text(run.example.answer.replace(" ","").replace(u'\xa0', u' ')))
        run.status = 'completed'
        run.save()
        h = threading.Thread(target=handleRunLogs, args=(run,))
        h.start()
        return True
    elif run.example.type=='blank':

        parts=run.example.statement.split('#')

        made_answer=""

        for i in range(0,len(parts)):
            if i%2 ==0:
                made_answer=made_answer+parts[i]
            else:
                made_answer=made_answer+run.code[int(i/2)]

        print(clean_text(run.example.statement).replace("#",""))
        print(clean_text(clean_text(made_answer)))

        run.verdict = (clean_text(made_answer)==clean_text(run.example.statement).replace("#",""))
        run.status = 'completed'
        run.save()
        h = threading.Thread(target=handleRunLogs, args=(run,))
        h.start()
        return True
    elif run.example.type=='descriptive':
        run.status = 'manual'
        run.save()
        # h = threading.Thread(target=handleRunLogs, args=(run,))
        # h.start()
        return True

    return False

@api_view(['POST'])
def app_example_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:
        learner = userSession.user.learner
        example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]

        serializer = ExampleDetailSerializer(example, many=False)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def app_example_item_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)

        learner = userSession.user.learner
        example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]

        serializer = ExampleItemBasedDetailSerializer(example, many=False)

        final=serializer.data

        items=[]
        if example.type != 'blank':
            items.append({"itemType": "simpleText", "typeCode": 1, "direction":"ltr", "isInput":False, "context":{"text":example.statement}})

        if example.figure:
            items.append({"itemType": "figure", "typeCode": 7, "direction":"ltr", "isInput":False, "context":{"url": example.figure.url}})

        if example.type == 'multi':
            if example.figOpts.all().count() >0:
                sl=FiguredOptionSerializer(example.figOpts.all(), many=True)
                items.append({"itemType": "figuredOptions", "typeCode": 5,"direction":"ltr","isInput":True, "context":{"optionList": sl.data}})
            else:
                splitted=example.options.split("\n")
                options=[o.replace('\r', '').strip() for o in splitted]
                items.append({"itemType": "options", "typeCode": 4, "direction":"ltr", "isInput":True, "context":{"optionList": options}})

        elif example.type == 'pick':
            splitted = example.options.split("\n")
            options = [o.replace('\r', '').strip() for o in splitted]
            items.append({"itemType": "pickContainer", "typeCode": 11, "direction":"ltr", "isInput":True, "context":{"optionList": options}})

        elif example.type == 'single':
            items.append({"itemType": "singleContainer", "typeCode": 9, "isInput":True, "direction":"ltr"})

        elif example.type == 'descriptive':
            items.append({"itemType": "textContainer", "typeCode": 10, "isInput":True, "direction":"ltr" })

        elif example.type == 'blank':
            items.append({"itemType": "blankContainer", "typeCode": 8, "direction":"ltr", "isInput":True, "context":{"text": example.statement}})

        elif example.type == 'playground':
            items.append({"itemType": "codeContainer", "typeCode": 6, "direction":"ltr", "isInput":True, "context":{"language":example.language, "initCode": example.initCode}})


        if example.hint != "":
            items.append({"itemType": "hint", "typeCode": 2, "direction":"ltr", "isInput":False, "context":{"text": example.hint}})

        if example.explanatoryAnswer != "" and example.explanatoryAnswer != "none":
            url_val="none"
            if example.explanatoryFigure:
                url_val = example.explanatoryFigure.url

            items.append(
                {"itemType": "explanatoryAnswer", "typeCode": 3, "direction":"ltr", "isInput":False, "context":{"text": example.explanatoryAnswer, "figure": url_val}})


        final["items"]=items
        return Response(final,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def app_example_items(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

        learner = userSession.user.learner
        example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]

        serializer = ExampleDetailSerializer(example, many=False)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)


@api_view(['POST'])
def app_submit_question(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        learner = userSession.user.learner
        example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]

        run=Run(learner=learner,example=example,code=request.data['answer'])
        run.save()

        r = threading.Thread(target=handleRun, args=(run,))
        r.start()

        serializer = RunSerializer(run, many=False)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def app_run_status(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:    

        run = Run.objects.all().filter(runKey=request.data['runKey'])[0]
        run.statusChecks=run.statusChecks+1
        run.save()

        # Fix : remove after code compiles are OK!!
        if run.status=='running':
            r = threading.Thread(target=handleRun, args=(run,))
            r.start()

        serializer = RunSerializer(run, many=False)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)