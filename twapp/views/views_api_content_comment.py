from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Content, ContentComment
from twapp.serializers import ContentCommentSerializer


@api_view(['POST'])
def add_comment(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        cmt= ContentComment(learner=learner,message=request.data['message'],content=content)
        cmt.save()
        serializer = ContentCommentSerializer(cmt, many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def get_comments(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        comments = ContentComment.objects.all().filter(content=content).order_by("-time")
        serializer = ContentCommentSerializer(comments, many=True,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

