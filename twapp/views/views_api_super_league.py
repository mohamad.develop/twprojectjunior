from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.utils import timezone

from twapp.models import AppSession, Learner, Follow, LeagueRecord, League, SuperLeague, Team, TeamInvite, \
    TeamRequest, Category

from twapp.serializers import LeagueSeralizer, LeagueRecordSerializer, \
    TeamSerializer, LearnerConnectionSerializer, SuperLeagueSeralizer, InviteSerializer, RequestSerializer

import threading
import json
from django.utils import translation

from .super_league_backgrounds import updateLearnerSuperLeagueStatuses
from .FCM_notif import create_notif

@api_view(['POST'])
def super_league_update_team(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        super_league=team.teamSuperLeague.all()[0]

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not team owner."}, status=status.HTTP_400_BAD_REQUEST)


        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not update team after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not update team 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        keys = request.data.keys()
        if "name" in keys:
            team.name = request.data["name"]
            team.save()

        ik=request.data['inviteKeys']

        if not ik == '':
            ikeys = json.loads(ik)
            for item in ikeys:
                try:
                    inv_learner = Learner.objects.all().filter(learnerKey=item)[0]
                    team_invite_ = TeamInvite(team=team, learner=inv_learner)
                    team_invite_.save()
                except Exception as e:
                    print(str(e))


        ak=request.data['acceptKeys']

        if not ak == '':
            akeys = json.loads(ak)
            for item in akeys:
                try:
                    acp_learner = Learner.objects.all().filter(learnerKey=item)[0]
                    acp_req = TeamRequest.objects.all().filter(learner=acp_learner)[0]
                    acp_req.status="accepted"
                    acp_req.save()
                    team.learners.add(acp_learner)
                    team.save()
                except Exception as e:
                    print(str(e))

        rk=request.data['rejectKeys']

        if not rk == '':
            rkeys = json.loads(rk)
            for item in rkeys:
                try:
                    rej_learner = Learner.objects.all().filter(learnerKey=item)[0]
                    rej_req = TeamRequest.objects.all().filter(learner=rej_learner)[0]
                    rej_req.status="rejected"
                    rej_req.save()
                except Exception as e:
                    print(str(e))

        dk=request.data['removeInviteKeys']

        if not dk == '':
            dkeys = json.loads(dk)
            for item in dkeys:
                try:
                    rem_learner = Learner.objects.all().filter(learnerKey=item)[0]
                    rem_req = TeamInvite.objects.all().filter(learner=rem_learner)[0]
                    rem_req.delete()
                except Exception as e:
                    print(str(e))

        super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()
        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def super_league_invite_learner(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        invited_learner = Learner.objects.all().filter(learnerKey=request.data['learnerKey'])[0]

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not team owner."}, status=status.HTTP_400_BAD_REQUEST)

        team_invite = TeamInvite(team=team, learner=invited_learner)
        team_invite.save()

        super_league=team.teamSuperLeague.all()[0]
        un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        un.start()


        serializer = TeamSerializer(team, many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def super_league_teams(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        super_league = SuperLeague.objects.all().filter(superLeagueKey=request.data['superLeagueKey'])[0]

        serializer = TeamSerializer(super_league.teams, many=True,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        super_league = SuperLeague.objects.all().filter(superLeagueKey=request.data['superLeagueKey'])[0]

        serializer_teams = TeamSerializer(super_league.teams, many=True,context={'learnerKey': learner.learnerKey})
        serializer_superleague = SuperLeagueSeralizer(super_league, many=False,context={'learnerKey': learner.learnerKey})


        un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        un.start()

        return Response({"superLeague":serializer_superleague.data,"teams":serializer_teams.data}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_team_create_info(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        super_league = SuperLeague.objects.all().filter(superLeagueKey=request.data['superLeagueKey'])[0]

        serializer_s = SuperLeagueSeralizer(super_league, many=False,context={'learnerKey': learner.learnerKey})

        following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        followings = []
        for fl in following_keys:
            print(fl.followedKey)
            flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
            followings.append(flr)
        serializer_l = LearnerConnectionSerializer(followings, many=True, context={'learnerKey': learner.learnerKey})

        return Response({"superLeague":serializer_s.data, "allowdLearners":serializer_l.data}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def super_league_leaderboard(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        league = League.objects.all().filter(leagueKey=request.data['leagueKey'])[0]

        ranks= LeagueRecord.objects.all().filter(league=league).order_by('rank')

        serializer = LeagueRecordSerializer(ranks, many=True,context={'learnerKey': learner.learnerKey})

        serializer_l = LeagueSeralizer(league,many=False,context={'learnerKey': learner.learnerKey})

        return Response({"league":serializer_l.data, "leaderboard":serializer.data}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def super_league_accept_invite(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        invite = TeamInvite.objects.all().filter(teamInviteKey=request.data['teamInviteKey'])[0]
        team= invite.team

        if invite.learner != learner:
            return Response({"status": "error", "object": "This invite is not for this user."}, status=status.HTTP_400_BAD_REQUEST)
        if invite.status != "new":
            return Response({"status": "error", "object": "Invitation accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)

        team.learners.add(invite.learner)
        team.save()

        invite.status="accepted"
        invite.save()

        super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_reject_invite(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        invite = TeamInvite.objects.all().filter(teamInviteKey=request.data['teamInviteKey'])[0]
        team= invite.team

        if invite.learner != learner:
            return Response({"status": "error", "object": "This invite is not for this user."}, status=status.HTTP_400_BAD_REQUEST)
        if invite.status != "new":
            return Response({"status": "error", "object": "Invitation accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)


        invite.status="rejected"
        invite.save()

        super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_accept_request(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        request = TeamRequest.objects.all().filter(teamRequestKey=request.data['teamRequestKey'])[0]
        team= request.team

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not the owner of team."}, status=status.HTTP_400_BAD_REQUEST)
        if request.status != "new":
            return Response({"status": "error", "object": "Request accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)

        team.learners.add(request.learner)
        team.save()

        request.status="accepted"
        request.save()


        sc = threading.Thread(target=create_notif, args=(request.learner,
                                                         "You've been added to "+request.team.name+" team!",
                                                         "You've been added to "+request.team.name+" in "+request.team.teamSuperLeague.title+". Let's work together and win!",
                                                         6,
                                                         {"competitionKey": competition.competitionKey}))
        sc.start()

        super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_reject_request(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        request = TeamRequest.objects.all().filter(teamRequestKey=request.data['teamRequestKey'])[0]
        team= request.team

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not the owner of team."}, status=status.HTTP_400_BAD_REQUEST)
        if request.status != "new":
            return Response({"status": "error", "object": "Request accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)

        request.status="rejected"
        request.save()

        super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_team_request(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        super_league=team.teamSuperLeague.all()[0]

        # if invite.learner != learner:
        #     return Response({"status": "error", "object": "This invite is not for this user."}, status=status.HTTP_400_BAD_REQUEST)
        # if invite.status != "new":
        #     return Response({"status": "error", "object": "Invitation accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)

        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not leave after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not leave 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        # delete former requests to this team
        TeamRequest.objects.all().filter(team=team, learner=learner).delete()

        team_rec = TeamRequest(team=team, learner=learner)
        team_rec.save()

        # super_league=team.teamSuperLeague.all()[0]
        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def super_league_team_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        requests = TeamRequest.objects.all().filter(team=team, status="new")
        invites = TeamInvite.objects.all().filter(team=team, status="new")

        team_serializer = TeamSerializer(team, many=False, context={'learnerKey': learner.learnerKey})
        request_serializer = RequestSerializer(requests, many=True, context={'learnerKey': learner.learnerKey})
        invite_serializer = InviteSerializer(invites, many=True, context={'learnerKey': learner.learnerKey})

        following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        followings = []
        for fl in following_keys:
            print(fl.followedKey)
            flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
            followings.append(flr)
        serializer_l = LearnerConnectionSerializer(followings, many=True, context={'learnerKey': learner.learnerKey})

        return Response(
            {"team": team_serializer.data, "requests": request_serializer.data, "invites": invite_serializer.data,
             "allowdLearners": serializer_l.data},
            status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_team_delete(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        super_league=team.teamSuperLeague.all()[0]

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not team owner."}, status=status.HTTP_400_BAD_REQUEST)


        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not delete team after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not delete team 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        team.delete()

        un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        un.start()

        return Response(
            {"status": "OK", "message": "Deleted"},
            status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def super_league_team_logo(request):
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        file=request.data['file']
        learner=sess.user.learner

        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        super_league=team.teamSuperLeague.all()[0]

        if team.ownerKey != learner.learnerKey:
            return Response({"status": "error", "object": "You are not team owner."}, status=status.HTTP_400_BAD_REQUEST)


        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not update team after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not update team 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        team.teamLogo = file
        team.save()

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        sl_list = SuperLeague.objects.all().filter(category=category)

        serializer = SuperLeagueSeralizer(sl_list, many=True, context={'learnerKey': learner.learnerKey})
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)