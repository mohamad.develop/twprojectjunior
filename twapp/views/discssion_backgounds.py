from twapp.models import Thread


def update_discussion_stats(learner_instance):
    print("Handing Discussion stats")

    learner_instance.forumActivity = Thread.objects.all().filter(learner=learner_instance).count()
    learner_instance.save()

    return True
