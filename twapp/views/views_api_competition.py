import threading

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Learner, Course, \
    Competition, Category, Follow, Example

from twapp.serializers import CompetitionSerializer, LearnerConnectionSerializer, CompetitionDetailSerializer, CategorySerializer

from django.utils import translation
from django.db.models import Q

from .FCM_notif import create_notif


def clean_text(text):
    ret=text.replace("\n","")
    ret=ret.strip()
    return ret.lower()

def choose_questions(competition_):
    try:
        course_ = competition_.course
        if len(competition_.questionKeys.split(",")) < 5:
            while len(competition_.questionKeys.split(",")) < 5:
                try:
                    lec=course_.courseLectures.all().order_by('?')[0]
                    episode_=lec.episodes.all().order_by('?')[0]
                    q= Example.objects.all().filter(relatedVideo=episode_,type='multi', ).order_by('?')[0]
                    print(q)
                    competition_.questionKeys=competition_.questionKeys+q.exampleKey+","
                    competition_.save()
                    competition_.questions.add(q)
                    competition_.save()
                except Exception as e:
                    print(str(e))
    except Exception as e:
        print(str(e))
    return




@api_view(['POST'])
def competition_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        x = threading.Thread(target=choose_questions, args=(competition,))
        x.start()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def competition_accept(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        if competition.opponentKey != learner.learnerKey or competition.status != 0:
            return Response({"status": "error",
                             "object": "You are not invited to this competition.",
                             "code":7011},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            competition.status = 1
            competition.save()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def create_competition(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        course = Course.objects.all().filter(courseKey=request.data['courseKey'])[0]
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        competition = Competition(course=course,category=category, learner=learner)
        competition.save()

        keys=request.data.keys()
        if 'opponentKey' in keys:
            competition.opponentKey = request.data['opponentKey']
            competition.save()
        else:
            competition.opponentKey = "7160d260-5fa3-40ce-b3bd-fdd008409144"
            competition.status = -1
            competition.save()

        invited_learner=Learner.objects.all().filter(learnerKey=request.data['opponentKey'])[0]

        print("invited_learner")
        print(invited_learner.pushToken)

        sc = threading.Thread(target=create_notif, args=(invited_learner,
                                                         "A Competition request has been sent to you!",
                                                         "Accept the request and put your skills to the test!",
                                                         4,
                                                         {"competitionKey": str(competition.competitionKey)}))
        sc.start()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def competition_reject(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        if competition.opponentKey != learner.learnerKey or competition.status != 0:
            return Response({"status": "error",
                             "object": "You are not invited to this competition or the competition is playing now.",
                             "code":7012},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            competition.status = 2
            competition.save()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def competition_reject(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        if competition.opponentKey != learner.learnerKey or competition.status != 0:
            return Response({"status": "error",
                             "object": "You are not invited to this competition or the competition is playing now.",
                             "code":7012},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            competition.status = 2
            competition.save()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def competition_random(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        if competition.learner.learnerKey != learner.learnerKey or competition.status != 2:
            return Response({"status": "error",
                             "object": "You can not request random opponent for this competition.",
                             "code":7015},
                            status=status.HTTP_400_BAD_REQUEST)
        elif Competition.objects.all().filter(course=competition.course, status=-1).exists():
            random_competition = Competition.objects.all().filter(course=competition.course, status=-1)[0]

            competition.opponentKey=random_competition.learner.learnerKey
            competition.status = 7
            competition.save()
            random_competition.delete()
        else:
            competition.status = -1
            competition.save()

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def competition_start(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        x = threading.Thread(target=choose_questions, args=(competition,))
        x.start()

        if competition.opponentKey == learner.learnerKey:
            competition.opponentStarted=True
            competition.save()

        elif competition.learner ==learner:
            competition.userStarted = True
            competition.save()


        else:
            return Response({"status": "error", "object": "Access Denied", "code":7010},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def competition_invite(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        if competition.opponentKey == learner.learnerKey:
            competition.opponentStarted=True
            competition.save()

            ks = competition.questionKeys.split(",")
            for k in ks:
                try:
                    q = Example.objects.all().filter(exampleKey=k)[0]
                    competition.questions.add(q)
                    competition.save()
                except:
                    print("end")

        elif competition.learner ==learner:
            competition.userStarted = True
            competition.save()
            try:
                q = Example.objects.all().filter(exampleKey=k)[0]
                competition.questions.add(q)
                competition.save()
            except:
                print("end")
        else:
            return Response({"status": "error", "object": "Access Denied"},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer = CompetitionDetailSerializer(competition, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def pre_create_info(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        all_parents = Category.objects.all().filter(isLeaf=False)
        final=[]
        serializer_c = CategorySerializer(all_parents, many=True)

        following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        followings = []
        for fl in following_keys:
            print(fl.followedKey)
            flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
            followings.append(flr)
        serializer_l = LearnerConnectionSerializer(followings, many=True, context={'learnerKey': learner.learnerKey})

        return Response({"parents":serializer_c.data, "allowdLearners":serializer_l.data}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def category_competitions(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
        user_lang = request.headers['languageKey']

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        competitions_all = Competition.objects.filter(Q(learner=learner, category=category) |
                                                      Q(opponentKey=learner.learnerKey, category=category))

        rejected_by_me = Competition.objects.filter(opponentKey=learner.learnerKey, category=category, status__in=[2,-1])

        competitions = competitions_all.exclude(pk__in=rejected_by_me.values('pk'))

        serializer = CompetitionSerializer(competitions, many=True, context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def competition_cancel(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        competition.delete()

        return Response({"status": "OK", "object": "canceled", "code":7013}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def competition_submit_question(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        learner = userSession.user.learner
        example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]
        competition = Competition.objects.all().filter(competitionKey=request.data['competitionKey'])[0]

        user_answers=request.data['answer']
        true_answers = example.answer.split("\n")

        user_answers.sort()
        true_answers.sort()

        if competition.opponentKey== learner.learnerKey:
            competition.opponentLastQuestionKey=example.exampleKey
            competition.save()
        else:
            competition.userLastQuestionKey=example.exampleKey
            competition.save()

        # print(user_answers)
        # print(true_answers)

        nextKey="-1"
        q_iter=iter(competition.questions.all())

        for q in q_iter:
            if q.exampleKey == request.data['exampleKey']:
                nextQ=next(q_iter,None)
                if nextQ==None:
                    nextKey="-1"
                else:
                    nextKey=nextQ.exampleKey
                break


        if len(true_answers) != len(user_answers):
            serializer = CompetitionSerializer(competition, many=False, context={'learnerKey': learner.learnerKey})
            return Response({"status": "submitted", "object": "Wrong Answer!",
                             "code": 7008, "nextKey": nextKey, "context":serializer.data},
                            status=status.HTTP_200_OK)



        final_verdict=True

        for i in range (0,len(user_answers)):
            if clean_text(user_answers[i]) != clean_text(true_answers[i]):
                final_verdict = False
                break

        if (final_verdict):
            if competition.opponentKey == learner.learnerKey:
                competition.opponentPoint = competition.opponentPoint + 1
                competition.save()
            else:
                competition.userPoint = competition.userPoint + 1
                competition.save()

            serializer = CompetitionSerializer(competition, many=False, context={'learnerKey': learner.learnerKey})
            return Response({"status": "submitted", "object": "True Answer!", "code":7009,
                             "nextKey":nextKey,"context":serializer.data}, status=status.HTTP_200_OK)

        serializer = CompetitionSerializer(competition, many=False, context={'learnerKey': learner.learnerKey})
        return Response({"status": "submitted", "object": "Wrong Answer!", "code": 7008,
                         "nextKey":nextKey, "context":serializer.data}, status=status.HTTP_200_OK)


    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)