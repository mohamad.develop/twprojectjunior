from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import Category
from twapp.serializers import CategorySerializer

from django.utils import translation


@api_view(['GET'])
def all_categories(request):
    try:
        # userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        # learner = userSession.user.learner
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        all_parents = Category.objects.all().filter(isLeaf=False).order_by("-parentingNum").reverse()

        final=[]

        for p in all_parents:
            all_cats = Category.objects.all().filter(parent=p)
            serializer = CategorySerializer(all_cats, many=True)
            serializer_ = CategorySerializer(p, many=False)
            final.append({"parent":serializer_.data,"subCategories":serializer.data})

        return Response(final,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def parent_categories(request):
    try:
        all_parents = Category.objects.all().filter(isLeaf=False).order_by("-parentingNum").reverse()
        serializer = CategorySerializer(all_parents, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def leaf_categories(request):
    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]
        leaf_parents = Category.objects.all().filter(isLeaf=True,parent=category)

        serializer = CategorySerializer(leaf_parents, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


