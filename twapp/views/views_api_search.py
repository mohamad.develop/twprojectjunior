from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import Course, Learner, Content, Video, Thread
from twapp.serializers import LearnerConnectionSerializer, SessionLessContentSerializer, \
    CourseListSerializer, EpisodeSearchSerializer, ThreadSearchSerializer

from django.utils import translation


@api_view(['POST'])
def simple_search(request):
    try:

        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        term = request.data['term']
        print(term)
        # page = request.data['page']
        # quantity = request.data['quantity']

        if len(term)<3:
            return Response({"status": "error", "object": "Search Term Must be at least 3 letters."}, status=status.HTTP_400_BAD_REQUEST)

        final = {}

        # Contents
        contents = Content.objects.all().filter(status='published', title__icontains=term) | Content.objects.all().filter(status='published', description__icontains=term)
        contents = contents.order_by('?')[:4]
        serializer = SessionLessContentSerializer(contents, many=True)
        final["contents"] = serializer.data

        # Courses
        courses = Course.objects.all().filter(verified=True, title__icontains=term) | Course.objects.all().filter(verified=True, description__icontains=term)
        courses = courses.order_by('?')[:4]
        serializer = CourseListSerializer(courses, many=True)
        final["courses"] = serializer.data

        # Episodes
        episodes = Video.objects.all().filter(videoTitle__icontains=term) | Video.objects.all().filter(videoDescription__icontains=term)
        episodes = episodes.order_by('?')[:4]
        serializer = EpisodeSearchSerializer(episodes, many=True)
        final["episodes"] = serializer.data

        # Learners
        learners = Learner.objects.all().filter(firstName__icontains=term) | Learner.objects.all().filter(lastName__icontains=term)
        learners = learners.order_by('?')[:4]
        serializer = LearnerConnectionSerializer(learners, many=True)
        final["learners"] = serializer.data

        # Threads
        threads = Thread.objects.all().filter(title__icontains=term) | Thread.objects.all().filter(body__icontains=term)
        threads = threads.order_by('?')[:4]
        serializer = ThreadSearchSerializer(threads, many=True)
        final["threads"] = serializer.data

        return Response(final, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

