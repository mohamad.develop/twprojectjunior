from rest_framework.decorators import api_view

from twapp.models import AppSession, Notification

from rest_framework import status
from rest_framework.response import Response

from pyfcm import FCMNotification
from django.conf import settings

from twapp.serializers import NotificationSerializer

from django.utils import translation


@api_view(['GET'])
def all_notifications(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

        notifs = Notification.objects.all().filter(toUser=userSession.user).order_by("-time")

        serializer = NotificationSerializer(notifs, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def send_push(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)

        info = push_service.get_registration_id_info(learner.pushToken)
        device_type = info


        message_title = "Techwich Test"
        message_body = " Body body body"
        registration_id = learner.pushToken
        data_message = {
            "title":"X followed you.",
            "body":"jghhjhj",
            "type":1,
            "context":
                {"learnerKey":"jhkjhjkhkjhkj"},
            "key1": "value1",
            "key2": "value2",
            "key3": "value3"
        }

        if info['platform'] == "ANDROID":
            result = push_service.notify_single_device(registration_id=registration_id, message_title=None,
                                                   message_body=None, data_message=data_message)
        else:
            result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                   message_body=message_body, data_message=data_message)

        if result['success']:
            return Response({"status": "SENT", "device_type": device_type}, status=status.HTTP_200_OK)
        else:
            # Notification failed
            error_message = result['results'][0]['error']
            return Response({"status": "error", "object": error_message, "serverKey": settings.FCM_SERVER_KEY,
                             "registration_id": learner.pushToken, "device_type": device_type},
                            status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def toggle_notification(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        notif = Notification.objects.all().filter(notificationKey=request.data['notificationKey'])[0]

        notif.read=not notif.read
        notif.save()

        serializer = NotificationSerializer(notif, many=False)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)
