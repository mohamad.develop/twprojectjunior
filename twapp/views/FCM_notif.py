from pyfcm import FCMNotification

from TWProject import settings


def create_notif(learner, title, body, type, context):
    try:

        push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)

        info = push_service.get_registration_id_info(learner.pushToken)
        device_type = info

        print('info')
        print(info['platform'])

        message_title = title
        message_body = body
        registration_id = learner.pushToken
        data_message = {
            "title": title,
            "body": body,
            "type": type,
            "context": context
        }

        if info['platform'] == "ANDROID":
            result = push_service.notify_single_device(registration_id=registration_id, message_title=None,
                                                           message_body=None, data_message=data_message)
            print('result')
            print(result)
        else:
            result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                           message_body=message_body, data_message=data_message)

    except Exception as e:
        print("FCM ERROR:"+str(e))
    return True
