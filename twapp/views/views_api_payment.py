import string
import random

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Course, PromoCode, Invoice, Subject
from twapp.serializers import CourseListSerializer, SubjectSerializer


@api_view(['POST'])
def apply_promo_code(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        try:
            promocode = PromoCode.objects.all().filter(token=request.data['token'])[0]
            invoice = Invoice.objects.all().filter(invoiceKey=request.data['invoiceKey'])[0]
        except:
            return Response({"status": "OK", "object": "PromoCode not found.", "code": 8002}, status=status.HTTP_200_OK)

        if promocode.status != "new":
            return Response({"status": "OK", "object": "PromoCode used already.", "code": 8003}, status=status.HTTP_200_OK)

        invoice.amount=invoice.amount-invoice.amount*promocode.percentage/100
        invoice.save()

        promocode.status="used"
        promocode.save()
        return Response({"status": "OK", "object": "applied", "newPrice":invoice.amount, "code":8001}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def course_invoice(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        course=Course.objects.all().filter(courseKey=request.data['courseKey'])[0]
        letters = string.ascii_lowercase
        invoice= Invoice(learner=learner, course=course, key=course.courseKey, amount=course.price, type='course')
        invoice.save()

        serializer = CourseListSerializer(course, many=False)

        return Response(
            {"invoiceKey": invoice.invoiceKey, "learnerKey":learner.learnerKey, "type": invoice.type , "price": course.price, "context":serializer.data},
            status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def subject_invoice(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        subject=Subject.objects.all().filter(subjectKey=request.data['subjectKey'])[0]
        letters = string.ascii_lowercase
        invoice= Invoice(learner=learner, key=subject.subjectKey, amount=subject.price, type='subject')
        invoice.save()

        serializer = SubjectSerializer(subject, many=False)

        return Response(
            {"invoiceKey": invoice.invoiceKey, "learnerKey":learner.learnerKey, "type": invoice.type , "price": subject.price, "context":serializer.data},
            status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def create_code(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        letters = string.ascii_lowercase
        tkn=''.join(random.choice(letters) for i in range(5))

        promocode=PromoCode(token=tkn, percentage=request.data['discount'])
        promocode.save()

        return Response({"status": "OK", "token": promocode.token}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def pay_link(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        import requests

        url = "https://nextpay.org/nx/gateway/token"

        payload = 'api_key=ca90d134-3dec-438d-8f44-49caf021edab&amount=74250&order_id=85NX85sgf427&callback_uri=https://techwich.net/sandbox?invoiceKey'
        headers = {
            'User-Agent': 'PostmanRuntime/7.26.8',
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        print(response.text)

        return Response(
            {"link": "https://techwich.net/sandbox?invoiceKey="+request.data['invoiceKey']},
            status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)