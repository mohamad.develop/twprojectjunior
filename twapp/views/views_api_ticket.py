from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Attend, Course, Ticket, Instructor, TicketMessage
from twapp.serializers import InstructorShortListSerializer, CourseShortSeralizer, TicketSerializer, TicketDetailSerializer, TicketMessageSerializer


@api_view(['POST'])
def create_ticket(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        ticket = Ticket(title=request.data['title'], message=request.data['message'], learner=learner)
        ticket.save()

        tck_msg = TicketMessage(message=request.data['message'], side='client', status='read' , ticket=ticket)
        tck_msg.save()

        keys=request.data.keys()

        if "instructorKey" in keys:
            instructor = Instructor.objects.all().filter(instructorKey=request.data['instructorKey'])[0]
            ticket.instructor=instructor
            ticket.save()

        if "type" in keys:
            ticket.type=request.data['type']
            ticket.save()

        if "courseKey" in keys:
            course = Course.objects.all().filter(courseKey=request.data['courseKey'])[0]
            ticket.course=course
            ticket.save()

        if "attachment" in keys:
            file = request.data['attachment']
            if file !='':
                ticket.attachment = file
                ticket.save()
                tck_msg.attachment = file
                tck_msg.save()

        serializer = TicketSerializer(ticket, many=False)

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def allowed_course_instructors(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        all_instructors = Instructor.objects.all()
        serializer = InstructorShortListSerializer(all_instructors, many=True)
        instructors = serializer.data

        attends = Attend.objects.all().filter(learner=learner)
        serializer = CourseShortSeralizer(attends, many=True)
        courses = serializer.data

        return Response({"instructors": instructors, "courses": courses}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def ticket_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        tickets = Ticket.objects.all().filter(type=request.data['type'], learner=learner).order_by("-time")
        serializer = TicketSerializer(tickets, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def ticket_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        ticket = Ticket.objects.all().filter(ticketKey=request.data['ticketKey'])[0]
        serializer = TicketDetailSerializer(ticket, many=False)

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def send_ticket_message(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        ticket = Ticket.objects.all().filter(ticketKey=request.data['ticketKey'])[0]

        tck_msg = TicketMessage(message=request.data['message'], side='message', status='read' , ticket=ticket)
        tck_msg.save()

        keys=request.data.keys()

        if 'attachment' in keys:
            file = request.data['attachment']
            ticket.attachment = file
            ticket.save()
            tck_msg.attachment = file
            tck_msg.save()

        serializer = TicketMessageSerializer(tck_msg, many=False)

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

