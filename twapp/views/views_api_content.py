import json
import threading
from PIL import Image

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Currency, Asset, Attend, Challenge, League, Learner, SuperLeague, Course, \
    Competition, Content, Category, Tag, Achievement, Follow, Wallet, Achievable, Log,SkillTag
from twapp.serializers import LearnerSeralizer, AssetSeralizer, AttendSerializer, ChallengeSerializer, LeagueSeralizer, \
    SuperLeagueSeralizer, CompetitionSerializer, ContentDetailSerializer, ContentSerializer, TagChildSeralizer, \
    AchievementSerializer, LearnerConnectionSerializer, WalletSeralizer, CourseListSerializer, SkillTagChildSeralizer, TeamListShortSerializer
import random, string

from .super_league_backgrounds import updateLearnerSuperLeagueStatuses


def tags_update(content_,tags_):

    if not tags_ == '':
        tgs = json.loads(tags_)
        content_.tags.clear()
        content_.save()
        for item in tgs:
            tag_ = Tag.objects.all().filter(tagKey=item)[0]
            content_.tags.add(tag_)
        content_.save()
    else:
        content_.tags.clear()
        content_.save()


def learner_assets_populate(learner_):
    currencies = Currency.objects.all()  # All currencies in app

    for cr in currencies:
        if not Asset.objects.all().filter(currency=cr , learner=learner_).exists():
            asset_=Asset(currency=cr , learner=learner_)
            asset_.save()

def learner_wallet_populate(learner_):
    if not Wallet.objects.all().filter(learner=learner_).exists():
            wallet_=Wallet(learner=learner_)
            wallet_.save()

def user_levels_populate(learner_):
    achievables = Achievable.objects.all().filter(general=True)

    for achievable in achievables:
        if not Achievement.objects.all().filter(achievable=achievable, learner=learner_).exists():
            achivement_ = Achievement(achievable=achievable, learner=learner_, value="0", icon=achievable.defaultIcon)
            achivement_.save()

def log_user_view(learner_):
    learner_.profileViews=learner_.profileViews+1
    learner_.save()

def update_like_count(content_):
    content_.likeCount= Log.objects.all().filter(action="LIKE", content=content_).count()
    content_.save()


def handle_content_image_size(content):
    if content.contentFile.url.endswith('.png'):
        im = Image.open(content.contentFile.url)
        print(content.contentFile.url)
        print(content.contentFile.url[:-4]+'_cmp.png')
        im.save(content.contentFile.url[:-4]+'_cmp.png', optimize=True, quality=65)
    if content.contentFile.url.endswith('.jpg'):
        print(content.contentFile.url)
        print(content.contentFile.url[:-4]+'_cmp.jpg')
        im = Image.open("/../.."+content.contentFile.url)
        im.save(content.contentFile.url[:-4]+'_cmp.jpg', optimize=True, quality=65)



@api_view(['POST'])
def create_content(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        file = request.data['contentFile']
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        content = Content(contentFile=file, description=request.data['description'], owner=learner, title=request.data['title'],
                                category=category)
        content.save()


        x = threading.Thread(target=tags_update, args=(content, request.data['tags'],))
        x.start()

        serializer = ContentSerializer(content, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def my_content(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        contents = Content.objects.all().filter(owner=learner)
        serializer = ContentSerializer(contents, many=True,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def category_content(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        contents = Content.objects.all().filter(category=category, status='published')
        serializer = ContentSerializer(contents, many=True,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def category_content_mine(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        contents = Content.objects.all().filter(category=category, owner=learner)
        serializer = ContentSerializer(contents, many=True,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def content_info(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        serializer = ContentDetailSerializer(content, many=False,context={'learnerKey': learner.learnerKey})

        content.views = content.views + 1
        content.save()

        x = threading.Thread(target=handle_content_image_size, args=(content,))
        x.start()

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def related_posts(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]

        related_contents= Content.objects.all().filter(category=content.category, status='published').order_by('?')[:5]
        serializer = ContentSerializer(related_contents, many=True,context={'learnerKey': learner.learnerKey})


        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def content_explore(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        sections = []

        learner = userSession.user.learner

        a = threading.Thread(target=learner_assets_populate, args=(learner,))
        a.start()
        p = threading.Thread(target=user_levels_populate, args=(learner,))
        p.start()
        v = threading.Thread(target=log_user_view, args=(learner,))
        v.start()
        w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        w.start()

        final = {}

        # Achievement

        achivements = Achievement.objects.all().filter(learner=learner)
        seralizer = AchievementSerializer(achivements, many=True)
        final["achievements"] = seralizer.data

        # Assets
        assets = Asset.objects.all().filter(learner=learner)
        serializer = AssetSeralizer(assets, many=True)
        final["assets"] = serializer.data

        # Connection
        # connections = Learner.objects.all().filter(friends__in=[learner])
        # # serializer = LearnerConnectionSerializer(connections, many=True, context={'learnerKey': learner.learnerKey})
        # # final["connections"] = serializer.data
        # final["connectionCount"] = connections.count()

        # Contents
        # contents = Content.objects.all().filter(owner=learner)
        # serializer = ContentSerializer(contents, many=True,context={'learnerKey': learner.learnerKey})
        # final["contents"] = serializer.data

        # Followers
        # followers_keys = Follow.objects.all().filter(followedKey=learner.learnerKey)
        # followers = []
        # for fl in followers_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followerKey)[0]
        #     followers.append(flr)
        # serializer = LearnerConnectionSerializer(followers, many=True, context={'learnerKey': learner.learnerKey})
        # final["followers"] = serializer.data
        # final["followerCount"] = Follow.objects.all().filter(followedKey=learner.learnerKey).count()

        # Following
        # following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        # followeings = []
        # for fl in following_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
        #     followeings.append(flr)
        # serializer = LearnerConnectionSerializer(followeings, many=True,  context={'learnerKey': learner.learnerKey})
        # final["following"] = serializer.data
        # final["followingCount"] = Follow.objects.all().filter(followerKey=learner.learnerKey).count()

        # Level

        level = {}

        level['currentStreak'] = learner.dailyStreak
        level['currentForumActivity'] = learner.forumActivity
        level['currentPoints'] = learner.points
        level['maxForumActivity'] = 25
        level['maxPoints'] = 46
        level['maxStreak'] = 15
        level['percentage'] = 27

        final["levelStatistic"] = level

        # User
        user = {}
        user["username"] = learner.user.username
        user["learnerKey"] = learner.learnerKey
        user['firstName'] = learner.firstName
        user['lastName'] = learner.lastName
        user['isPro'] = learner.isPro
        user['isPrivate'] = learner.isPrivate
        user['profilePicture'] = learner.profilePicture.url
        user['profilePictureThumb'] = learner.profilePictureThumb.url
        user['level'] = learner.level
        final["user"] = user
        final["learnerKey"] = learner.learnerKey

        # Wallet
        wallet = Wallet.objects.all().filter(learner=learner)[0]
        serializer = WalletSeralizer(wallet, many=False)
        final["wallet"] = serializer.data

        team_serializer=TeamListShortSerializer(learner.learnerTeams.all()[:3], many=True)
        final["teams"] = team_serializer.data

        sections.append(final)

        attends = Attend.objects.all().filter(learner=learner)
        serializer = AttendSerializer(attends, many=True)
        header = {'title': 'My Courses', 'showMore': True, "moreTitle": "All Course"}
        sections.append({'header': header, 'sectionId': "crs", 'type': 2, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        challenges = Challenge.objects.all().order_by('startTime').reverse()[:6]
        # challenges = Challenge.objects.all().order_by('?')[:7]
        serializer = ChallengeSerializer(challenges, many=True, context={'learnerKey': learner.learnerKey})
        header = {'title': 'Challenges', 'showMore': True, "moreTitle": "View all"}
        sections.append({'header': header, 'sectionId': "chl", 'type': 11, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        competitions = Competition.objects.all().filter(learner=learner) | Competition.objects.all().filter(opponentKey=learner.learnerKey)
        competitions= competitions.order_by('-id')[:5]
        serializer = CompetitionSerializer(competitions, many=True, context={'learnerKey': learner.learnerKey})
        header = {'title': 'Competitions', 'showMore': True, "moreTitle": "View all"}
        sections.append({'header': header, 'sectionId':"cmp", 'type': 16, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        league = League.objects.all().order_by('-startTime')[:6]
        serializer = LeagueSeralizer(league, many=True ,context={'learnerKey': learner.learnerKey})
        header = {'title': 'Weekly League', 'showMore': True, "moreTitle": "View all"}
        sections.append({'header': header, 'sectionId':"wlg_" + learner.learnerKey, 'type': 14, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        superleagues = SuperLeague.objects.all().order_by('-startTime')[:6]
        serializer = SuperLeagueSeralizer(superleagues, many=True ,context={'learnerKey': learner.learnerKey})
        header = {'title': 'Super League', 'showMore': True, "moreTitle": "View all"}
        sections.append({'header': header, 'sectionId':"slg_" + learner.learnerKey, 'type': 15, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        for sl in superleagues:
            slu = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,sl))
            slu.start()


        return Response(sections, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def sessionless_content_explore(request):
    try:
        # userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        # serializer = LearnerSeralizer(userSession.user.learner, many=False)

        sections = []
        #
        # learner = userSession.user.learner

        # a = threading.Thread(target=learner_assets_populate, args=(learner,))
        # a.start()
        # p = threading.Thread(target=user_levels_populate, args=(learner,))
        # p.start()
        # v = threading.Thread(target=log_user_view, args=(learner,))
        # v.start()
        # w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        # w.start()

        final = {}

        # # Achievement
        #
        # achivements = Achievement.objects.all().filter(learner=learner)
        # seralizer = AchievementSerializer(achivements, many=True)
        # final["achievements"] = seralizer.data

        # # Assets
        # assets = Asset.objects.all().filter(learner=learner)
        # serializer = AssetSeralizer(assets, many=True)
        # final["assets"] = serializer.data

        # # Connection
        # connections = Learner.objects.all().filter(friends__in=[learner])
        # serializer = LearnerConnectionSerializer(connections, many=True, context={'learnerKey': learner.learnerKey})
        # final["connections"] = serializer.data
        # final["connectionCount"] = connections.count()
        #
        # # Contents
        # contents = Content.objects.all().filter(owner=learner)
        # serializer = ContentSerializer(contents, many=True)
        # final["contents"] = serializer.data

        # # Followers
        # followers_keys = Follow.objects.all().filter(followedKey=learner.learnerKey)
        # followers = []
        # for fl in followers_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followerKey)[0]
        #     followers.append(flr)
        # serializer = LearnerConnectionSerializer(followers, many=True, context={'learnerKey': learner.learnerKey})
        # final["followers"] = serializer.data
        # final["followerCount"] = len(followers)

        # # Following
        # following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        # followeings = []
        # for fl in following_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
        #     followeings.append(flr)
        # serializer = LearnerConnectionSerializer(followeings, many=True,  context={'learnerKey': learner.learnerKey})
        # final["following"] = serializer.data
        # final["followingCount"] = len(followeings)
        #
        # # Level
        #
        # level = {}
        #
        # level['currentStreak'] = learner.dailyStreak
        # level['currentForumActivity'] = learner.forumActivity
        # level['currentPoints'] = learner.points
        # level['maxForumActivity'] = 25
        # level['maxPoints'] = 46
        # level['maxStreak'] = 15
        # level['percentage'] = 27
        #
        # final["levelStatistic"] = level

        # # User
        # user = {}
        # user["username"] = learner.user.username
        # user["learnerKey"] = learner.learnerKey
        # user['firstName'] = learner.firstName
        # user['lastName'] = learner.lastName
        # user['isPro'] = learner.isPro
        # user['isPrivate'] = learner.isPrivate
        # user['profilePicture'] = learner.profilePicture.url
        # user['level'] = learner.level
        # final["user"] = user
        # final["learnerKey"] = learner.learnerKey

        # # Wallet
        # wallet = Wallet.objects.all().filter(learner=learner)[0]
        # serializer = WalletSeralizer(wallet, many=False)
        # final["wallet"] = serializer.data
        #
        # sections.append(final)

        courses = Course.objects.all().filter(verified=True)[:7]
        serializer = CourseListSerializer(courses, many=True)
        header = {'title': 'Courses', 'showMore': True, "moreTitle": "All Course"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lc-" + dummy2, 'type': 2, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        challenges = Challenge.objects.all().order_by('startTime').reverse()[:7]
        # challenges = Challenge.objects.all().order_by('?')[:7]
        serializer = ChallengeSerializer(challenges, many=True)
        header = {'title': 'Challenges', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lch-" + dummy2, 'type': 11, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        # competitions = Competition.objects.all().filter(learner=learner)
        # serializer = CompetitionSerializer(competitions, many=True)
        # header = {'title': 'Competitions', 'showMore': True, "moreTitle": "View all"}
        # letters = string.ascii_letters
        # dummy = ''.join(random.choice(letters) for i in range(10))
        # dummy2 = ''.join(random.choice(letters) for i in range(10))
        # sections.append({'header': header, 'sectionId': dummy + "-lcmp-" + dummy2, 'type': 16, 'subtype': 2, 'data': '',
        #                  'list': serializer.data})

        league = League.objects.all()
        serializer = LeagueSeralizer(league, many=True,context={'learnerKey': learner.learnerKey})
        header = {'title': 'Weekly League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-llg-" + dummy2, 'type': 14, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        superleagues = SuperLeague.objects.all()
        serializer = SuperLeagueSeralizer(superleagues, many=True)
        header = {'title': 'Super League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-slg-" + dummy2, 'type': 15, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        return Response(sections, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
def sessionless_content_explore(request):
    try:
        # userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        # serializer = LearnerSeralizer(userSession.user.learner, many=False)

        sections = []
        #
        # learner = userSession.user.learner

        # a = threading.Thread(target=learner_assets_populate, args=(learner,))
        # a.start()
        # p = threading.Thread(target=user_levels_populate, args=(learner,))
        # p.start()
        # v = threading.Thread(target=log_user_view, args=(learner,))
        # v.start()
        # w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        # w.start()

        final = {}

        # # Achievement
        #
        # achivements = Achievement.objects.all().filter(learner=learner)
        # seralizer = AchievementSerializer(achivements, many=True)
        # final["achievements"] = seralizer.data

        # # Assets
        # assets = Asset.objects.all().filter(learner=learner)
        # serializer = AssetSeralizer(assets, many=True)
        # final["assets"] = serializer.data

        # # Connection
        # connections = Learner.objects.all().filter(friends__in=[learner])
        # serializer = LearnerConnectionSerializer(connections, many=True, context={'learnerKey': learner.learnerKey})
        # final["connections"] = serializer.data
        # final["connectionCount"] = connections.count()
        #
        # # Contents
        # contents = Content.objects.all().filter(owner=learner)
        # serializer = ContentSerializer(contents, many=True)
        # final["contents"] = serializer.data

        # # Followers
        # followers_keys = Follow.objects.all().filter(followedKey=learner.learnerKey)
        # followers = []
        # for fl in followers_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followerKey)[0]
        #     followers.append(flr)
        # serializer = LearnerConnectionSerializer(followers, many=True, context={'learnerKey': learner.learnerKey})
        # final["followers"] = serializer.data
        # final["followerCount"] = len(followers)

        # # Following
        # following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
        # followeings = []
        # for fl in following_keys:
        #     flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
        #     followeings.append(flr)
        # serializer = LearnerConnectionSerializer(followeings, many=True,  context={'learnerKey': learner.learnerKey})
        # final["following"] = serializer.data
        # final["followingCount"] = len(followeings)
        #
        # # Level
        #
        # level = {}
        #
        # level['currentStreak'] = learner.dailyStreak
        # level['currentForumActivity'] = learner.forumActivity
        # level['currentPoints'] = learner.points
        # level['maxForumActivity'] = 25
        # level['maxPoints'] = 46
        # level['maxStreak'] = 15
        # level['percentage'] = 27
        #
        # final["levelStatistic"] = level

        # # User
        # user = {}
        # user["username"] = learner.user.username
        # user["learnerKey"] = learner.learnerKey
        # user['firstName'] = learner.firstName
        # user['lastName'] = learner.lastName
        # user['isPro'] = learner.isPro
        # user['isPrivate'] = learner.isPrivate
        # user['profilePicture'] = learner.profilePicture.url
        # user['level'] = learner.level
        # final["user"] = user
        # final["learnerKey"] = learner.learnerKey

        # # Wallet
        # wallet = Wallet.objects.all().filter(learner=learner)[0]
        # serializer = WalletSeralizer(wallet, many=False)
        # final["wallet"] = serializer.data
        #
        # sections.append(final)

        courses = Course.objects.all().filter(verified=True)[:3]
        serializer = CourseListSerializer(courses, many=True)
        header = {'title': 'Courses', 'showMore': True, "moreTitle": "All Course"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lc-" + dummy2, 'type': 2, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        challenges = Challenge.objects.all().order_by('startTime').reverse()[:3]
        # challenges = Challenge.objects.all().order_by('?')[:7]
        serializer = ChallengeSerializer(challenges, many=True)
        header = {'title': 'Challenges', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lch-" + dummy2, 'type': 11, 'subtype': 2, 'data': '',
                         'list': serializer.data})


        contents = Content.objects.all().filter(status='published')[:3]
        serializer = ContentSerializer(courses, many=True)
        header = {'title': 'Contents', 'showMore': True, "moreTitle": "All Course"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lc-" + dummy2, 'type': 2, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        # competitions = Competition.objects.all().filter(learner=learner)
        # serializer = CompetitionSerializer(competitions, many=True)
        # header = {'title': 'Competitions', 'showMore': True, "moreTitle": "View all"}
        # letters = string.ascii_letters
        # dummy = ''.join(random.choice(letters) for i in range(10))
        # dummy2 = ''.join(random.choice(letters) for i in range(10))
        # sections.append({'header': header, 'sectionId': dummy + "-lcmp-" + dummy2, 'type': 16, 'subtype': 2, 'data': '',
        #                  'list': serializer.data})

        league = League.objects.all()
        serializer = LeagueSeralizer(league, many=True,context={'learnerKey': learner.learnerKey})
        header = {'title': 'Weekly League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-llg-" + dummy2, 'type': 14, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        superleagues = SuperLeague.objects.all()
        serializer = SuperLeagueSeralizer(superleagues, many=True)
        header = {'title': 'Super League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-slg-" + dummy2, 'type': 15, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        return Response(sections, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)



@api_view(['POST'])
def content_explore_beta(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.data['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)

        sections = []

        final = {}

        learner=userSession.user.learner

        # Achievement

        achivements=Achievement.objects.all().filter(learner=learner)
        seralizer=AchievementSerializer(achivements, many=True)
        final["achievements"] = seralizer.data

        # Assets
        assets= Asset.objects.all().filter(learner=learner)
        serializer = AssetSeralizer(assets, many=True)
        final["assets"]=serializer.data

        # Connection
        connections= Learner.objects.all().filter(friends__in=[learner])
        serializer = LearnerConnectionSerializer(connections, many=True, context={'learnerKey': learner.learnerKey})
        final["connections"]=serializer.data
        final["connectionCount"]=connections.count()

        # Contents
        contents= Content.objects.all().filter(owner=learner)
        serializer = ContentSerializer(contents, many=True,context={'learnerKey': learner.learnerKey})
        final["contents"]=serializer.data

        # Followers
        followers_keys= Follow.objects.all().filter(followedKey=learner.learnerKey)
        followers=[]
        for fl in followers_keys:
            flr=Learner.objects.all().filter(learnerKey=fl.followerKey)[0]
            followers.append(flr)
        serializer = LearnerConnectionSerializer(followers, many=True, context={'learnerKey': learner.learnerKey})
        final["followers"]=serializer.data
        final["followerCount"]=len(followers)

        # Following
        following_keys= Follow.objects.all().filter(followerKey=learner.learnerKey)
        followeings=[]
        for fl in following_keys:
            flr=Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
            followeings.append(flr)
        serializer = LearnerConnectionSerializer(followeings, many=True, context={'learnerKey': learner.learnerKey})
        final["following"]=serializer.data
        final["followingCount"]=len(followers)

        # Level

        level={}

        level['currentStreak'] = learner.dailyStreak
        level['currentForumActivity'] = learner.forumActivity
        level['currentPoints'] = learner.points
        level['maxForumActivity'] = 25
        level['maxPoints'] = 46
        level['maxStreak'] = 28
        level['percentage'] = 27

        final["levelStatistic"] = level

        # User
        user = {}
        user["username"] = learner.user.username
        user["learnerKey"] = learner.learnerKey
        user['firstName'] = learner.firstName
        user['lastName'] = learner.lastName
        user['isPro'] = learner.isPro
        user['isPrivate'] = learner.isPrivate
        user['profilePicture'] = learner.profilePicture.url
        user['level'] = learner.level
        final["user"] = user
        final["learnerKey"] = learner.learnerKey

        # Wallet
        wallet= Wallet.objects.all().filter(learner=learner)[0]
        serializer = WalletSeralizer(wallet, many=False)
        final["wallet"]=serializer.data

        sections.append(final)

        currencies = Currency.objects.all()  # All currencies in app

        # for cr in currencies:
        #     if not Asset.objects.all().filter(currency=cr , learner=learner).exists():
        #         asset_=Asset(currency=cr , learner=learner)
        #         asset_.save()

        attends = Attend.objects.all().filter(learner=learner)
        serializer = AttendSerializer(attends, many=True)
        header = {'title': 'My Courses', 'showMore': True, "moreTitle": "All Course"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lc-" + dummy2, 'type': 2, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        challenges = Challenge.objects.all()
        serializer = ChallengeSerializer(challenges, many=True)
        header = {'title': 'Challenges', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lch-" + dummy2, 'type': 11, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        serializer = CompetitionSerializer(attends, many=True)
        header = {'title': 'Competitions', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-lcmp-" + dummy2, 'type': 16, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        league = League.objects.all()
        serializer = LeagueSeralizer(league, many=True,context={'learnerKey': learner.learnerKey})
        header = {'title': 'Weekly League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-llg-" + dummy2, 'type': 14, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        superleagues = SuperLeague.objects.all()
        serializer = SuperLeagueSeralizer(superleagues, many=True)
        header = {'title': 'Super League', 'showMore': True, "moreTitle": "View all"}
        letters = string.ascii_letters
        dummy = ''.join(random.choice(letters) for i in range(10))
        dummy2 = ''.join(random.choice(letters) for i in range(10))
        sections.append({'header': header, 'sectionId': dummy + "-slg-" + dummy2, 'type': 15, 'subtype': 2, 'data': '',
                         'list': serializer.data})

        return Response(sections, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['POST'])
def like_toggle(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        serializer = ContentDetailSerializer(content, many=False,context={'learnerKey': learner.learnerKey})

        status_ret = "liked"
        isLiked_ret=True
        like_ret = 0

        # for content in Content.objects.all():
        #     try:
        #         content.save()
        #     except:
        #         print(".")

        if Log.objects.all().filter(content=content, action="LIKE", learner=learner).exists():
            status_ret = "unliked"
            isLiked_ret= False
            like_ret = content.likeCount - 1
            Log.objects.all().filter(content=content, action="LIKE", learner=learner).delete()
        else:
            status_ret = "liked"
            isLiked_ret= True
            like_ret = content.likeCount + 1
            log_ = Log(content=content, action="LIKE", learner=learner)
            log_.save()

        x = threading.Thread(target=update_like_count, args=(content,))
        x.start()

        return Response({"status": status_ret, "likeCount": like_ret , "isLiked":isLiked_ret}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def content_edit(request):

    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        keys=request.data.keys()

        for key in keys:
            if key == "title":
                content.title = request.data["title"]
                content.save()
            if key == "description":
                content.description = request.data["description"]
                content.save()
            if key == "contentFile":
                file = request.data['contentFile']
                content.contentFile = file
                content.save()

        x = threading.Thread(target=tags_update, args=(content, request.data['tags'],))
        x.start()
        serializer = ContentDetailSerializer(content, many=False,context={'learnerKey': learner_.learnerKey})

        return Response({"status":"OK","updated":keys,"object":serializer.data},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def content_delete(request):

    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        content = Content.objects.all().filter(contentKey=request.data['contentKey'])[0]
        content.delete()
        return Response({"status":"OK","object":"deleted"},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def all_tags(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:

        tags = Tag.objects.all().filter()
        serializer = TagChildSeralizer(tags, many=True)

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET'])
def all_skill_tags(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        tags = SkillTag.objects.all().filter()
        serializer = SkillTagChildSeralizer(tags, many=True)

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



