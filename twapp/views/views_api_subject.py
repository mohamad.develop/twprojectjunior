from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Category, Subject
from twapp.serializers import SubjectListSerializer, SubjectSerializer


@api_view(['POST'])
def subject_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        # sort=request.data['sort']

        subjects = Subject.objects.all().filter(category=category)

        # if sort=="time":
        #     threads = Thread.objects.all().filter(category=category).order_by('time')
        # elif sort=="votes":
        #     threads = Thread.objects.all().filter(category=category).order_by('upVotes')
        # elif sort=="views":
        #     threads = Thread.objects.all().filter(category=category).order_by('views')

        serializer = SubjectListSerializer(subjects, many=True, context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def subject_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        subject = Subject.objects.all().filter(subjectKey=request.data['subjectKey'])[0]
        serializer = SubjectSerializer(subject, many=False)

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

