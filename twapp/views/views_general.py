import string
import random

from django.shortcuts import render,redirect
from rest_framework.decorators import api_view
from django.contrib.auth.models import User

from twapp.models import Video, Instructor, Lecture

from rest_framework import status
from rest_framework.response import Response
from moviepy.editor import VideoFileClip
import threading


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def update_video_duration(video_obj, path):
    # clip = VideoFileClip("../../media/"+video_obj.videoFile.url)
    clip = VideoFileClip("http://"+path)
    dur_str=""
    dur=clip.duration
    dur_str=dur_str+str(int(dur/3600))+":"
    dur=int(dur%3600)
    dur_str=dur_str+str(int(dur/60))+":"+ str(int(dur%60))

    video_obj.videoDuration = dur_str
    video_obj.save()

@api_view(['GET'])
def low_speed(request):
    """
    :param request: ID
    :return: all detail of Expertise by ID.
    """
    return render(request, 'twapp/low-speed.html',{'message':"#", "ip":get_client_ip(request)})

@api_view(['GET'])
def coming_soon(request):
    """
    :param request: ID
    :return: all detail of Expertise by ID.
    """

    return render(request, 'twapp/coming-soon.html',{'message':"#"})

@api_view(['GET'])
def new_video(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        user = User.objects.all().filter(username=request.session.get('username'))[0]
        inst = Instructor.objects.all().filter(user=user)[0]

        return render(request, 'twapp/new-video.html',{'message':"#" , "instructor":inst})


@api_view(['POST'])
def commit_video(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        user = User.objects.all().filter(username=request.session.get('username'))[0]
        inst = Instructor.objects.all().filter(user=user)[0]

        try:
            print(request.data)
            user=User.objects.all().filter(username=request.session.get('username'))[0]
            vid=request.data['vid']

            title_value=request.data['title']
            if title_value == '':
                return render(request, 'twapp/new-course.html',
                              {'message': "You can not create a course without a title.", "instructor": inst})

            video=Video.objects.populate(True).create(videoFile=vid,videoTitle=title_value,videoDescription=request.data['desc'],videoOwner=user)

            video.save()

            cover=request.data['cover']

            if cover != '':
                video.profilePicture = cover

            video.save()


            return Response({"status":"OK","videoKey":video.videoKey},status=status.HTTP_201_CREATED)

        except Exception as e:
            print("!"*10+"add_interactive_item"+str(e))
            return render(request, 'twapp/error-500.html')

@api_view(['POST'])
def commit_video_lecture(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        try:
            print(request.data)
            user = User.objects.all().filter(username=request.session.get('username'))[0]
            vid = request.data['vid']

            title_value = request.data['title']

            if title_value == '':
                title_value="Video_"+''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(15))


            video = Video.objects.populate(True).create(videoFile=vid, videoTitle=title_value,
                                         videoDescription=request.data['desc'], videoOwner=user)

            video.save()

            if request.POST.get('free', "false") == "on":
                video.isFree = True
                video.save()
            else:
                video.isFree = False
                video.save()


            cover = request.data['cover']

            if cover != '':
                video.cover = cover
            video.save()

            lecture = Lecture.objects.all().filter(lectureKey=request.data['key'])[0]

            order_value=0
            if lecture.episodes.count() !=0:
                order_value= lecture.episodes.order_by('-order')[0].order+1
            video.order=order_value
            video.save()


            lecture.episodes.add(video)
            lecture.save()

            x = threading.Thread(target=update_video_duration, args=( video,request.get_host()+video.videoFile.url,))
            x.start()

            return Response({"status": "OK", "videoKey": video.videoKey}, status=status.HTTP_201_CREATED)

        except Exception as e:
            print("!" * 10 + "add_interactive_item" + str(e))
            return render(request, 'twapp/error-500.html')


@api_view(['POST'])
def commit_text_based_episode(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        try:
            print(request.data)
            user = User.objects.all().filter(username=request.session.get('username'))[0]
            # vid = request.data['vid']

            title_value = request.data['title']

            if title_value == '':
                title_value="Episode_"+''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(15))


            video = Video.objects.populate(True).create(videoTitle=title_value,
                                         videoDescription=request.data['desc'], videoOwner=user)

            video.save()

            if request.POST.get('free', "false") == "on":
                video.isFree = True
                video.save()
            else:
                video.isFree = False
                video.save()


            cover = request.data['cover']

            if cover != '':
                video.cover = cover
            video.save()

            lecture = Lecture.objects.all().filter(lectureKey=request.data['key'])[0]

            order_value=0
            if lecture.episodes.count() !=0:
                order_value= lecture.episodes.order_by('-order')[0].order+1
            video.order=order_value
            video.save()


            lecture.episodes.add(video)
            lecture.save()

            video.videoDuration="Text Based"
            video.save()

            return redirect("/video_detail?key=" + str(video.videoKey))

        except Exception as e:
            print("!" * 10 + "add_interactive_item" + str(e))
            return render(request, 'twapp/error-500.html')


@api_view(['POST'])
def commit_episode_cover(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        user = User.objects.all().filter(username=request.session.get('username'))[0]
        inst = Instructor.objects.all().filter(user=user)[0]

        videoKeyValue = request.data['key']
        video = Video.objects.all().filter(videoKey=videoKeyValue)[0]

        img = request.data['img']

        if img != '':
            video.cover = img
            video.save()

        return redirect("/video_detail?key=" + video.videoKey)

@api_view(['POST'])
def commit_episode_video(request):
    if request.session.get('panelSessionKey') is None:
        return render(request, 'twapp/login.html', {})
    else:
        user = User.objects.all().filter(username=request.session.get('username'))[0]
        inst = Instructor.objects.all().filter(user=user)[0]

        videoKeyValue = request.data['key']
        video = Video.objects.all().filter(videoKey=videoKeyValue)[0]

        vd = request.data['vid']

        if vd != '':
            video.videoFile = vd
            video.save()

        return redirect("/video_detail?key=" + video.videoKey)