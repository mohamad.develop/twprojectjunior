from django.utils import timezone

from twapp.models import TeamInvite, TeamRequest, LearnerSuperLeagueStatus


def updateLearnerSuperLeagueStatuses(learner, superleague):  # useless here
    if LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=superleague).exists():
        status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=superleague)[0]
    else:
        status_ = LearnerSuperLeagueStatus(learner=learner, superLeague=superleague)
        status_.save()

    teams = learner.learnerTeams.all()

    try:
    # if(1==1):
        if timezone.now() < superleague.startTime:

            # temporary set status to 3 (superLeague is open and Learner had no interact with the superLeague)
            status_.status = 3
            status_.save()

            for team in teams:
                if team.teamSuperLeague.all().exists():
                    team_sl = team.teamSuperLeague.all()[0]
                    if team_sl.superLeagueKey == superleague.superLeagueKey:
                        if team.learners.count() < superleague.teamsCount and team.ownerKey == learner.learnerKey:
                            # superLeague is open, Learner is the owner of team and team is not full
                            status_.status = 1
                            status_.team = team
                            status_.save()
                            return True
                        elif team.learners.count() < superleague.teamsCount and team.ownerKey != learner.learnerKey:
                            # superLeague is open, Learner is not the owner of team and team is not full
                            status_.status = 2
                            status_.team = team
                            status_.save()
                            return True
                        elif team.learners.count() == superleague.teamsCount and team.ownerKey != learner.learnerKey:
                            # superLeague is open, Learner is not the owner of team and team is full
                            status_.status = 4
                            status_.team = team
                            status_.save()
                            return True
                        elif team.learners.count() == superleague.teamsCount and team.ownerKey == learner.learnerKey:
                            # superLeague is open, Learner is the owner of team and team is full
                            status_.status = 5
                            status_.team = team
                            status_.save()
                            return True

            teamsOfSuperLeague=superleague.teams.all()

            for team in teamsOfSuperLeague:
                if TeamRequest.objects.all().filter(team=team, learner=learner, status="new").exists():
                    # superLeague is open, Learner requested to a team and request not accepted/rejected
                    status_.status = 6
                    status_.team = team
                    status_.save()
                    return True
                if TeamInvite.objects.all().filter(team=team, learner=learner , status="new").exists():
                    # superLeague is open, Learner invited to a team and invite not accepted/rejected
                    status_.status = 10
                    status_.team = team
                    status_.save()
                    return True

            status_.status = 3
            status_.save()
            return True
        elif timezone.now() > superleague.endTime:
            for team in teams:
                if team.teamSuperLeague.all().exists():
                    team_sl = team.teamSuperLeague.all()[0]
                    if team_sl.superLeagueKey == superleague.superLeagueKey:
                        status_.status = 8
                        status_.team = team
                        status_.save()
                        return True
            status_.status = 9
            status_.save()
            return True
        else:
            for team in teams:
                if team.teamSuperLeague.all().exists():
                    team_sl = team.teamSuperLeague.all()[0]
                    if team_sl.superLeagueKey == superleague.superLeagueKey:
                        if team.ownerKey == learner.learnerKey:
                            status_.status = 11
                            status_.team = team
                            status_.save()
                            return True
                        else:
                            status_.status = 12
                            status_.team = team
                            status_.save()
                            return True
        return True
    except Exception as e:
        print("---->" + str(e))
        status_.status = -1
        status_.save()
        return True