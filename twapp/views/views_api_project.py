import json
import threading

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Category, Project, SkillTag, Proposal

from twapp.serializers import ProjectSerializer, ProjectListSerializer, ProposalSerializer, ProjectDetailSerializer


def skill_tags_update(prj_, tags_):

    if not tags_ == '':
        tgs = json.loads(tags_)
        prj_.tags.clear()
        prj_.save()
        for item in tgs:
            tag_ = SkillTag.objects.all().filter(skillTagKey=item)[0]
            prj_.tags.add(tag_)
        prj_.save()
    else:
        prj_.tags.clear()
        prj_.save()


@api_view(['POST'])
def create_project(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        keys=request.data.keys()

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        project = Project(title=request.data['title'], creator=learner.user, description=request.data['description'],
                          minBudget=request.data['minBudget'], maxBudget=request.data['maxBudget'],
                          dueTime=request.data['dueTime'], category=category)
        project.save()

        if 'attachment' in keys:
            file = request.data['attachment']
            project.attachment=file
            project.save()

        x = threading.Thread(target=skill_tags_update, args=(project, request.data['tags'],))
        x.start()

        serializer = ProjectSerializer(project, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def make_bid(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        project = Project.objects.all().filter(projectKey=request.data['projectKey'])[0]

        proposal = Proposal(description=request.data['description'], bid=request.data['bid'],
                            duration=request.data['duration'], project=project, learner=learner)
        proposal.save()

        serializer = ProposalSerializer(proposal, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def project_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        project = Project.objects.all().filter(projectKey=request.data['projectKey'])[0]

        serializer = ProjectSerializer(project, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def project_full_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        project = Project.objects.all().filter(projectKey=request.data['projectKey'])[0]

        serializer = ProjectDetailSerializer(project, many=False ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)




@api_view(['POST'])
def project_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        sort=request.data['sort']

        projects = Project.objects.all().filter(category=category)

        if sort=="budget":
            projects = Project.objects.all().filter(category=category).order_by('maxBudget').reverse()
        elif sort=="bids":
            projects = Project.objects.all().filter(category=category).order_by('dueTime').reverse()
        elif sort=="time":
            projects = Project.objects.all().filter(category=category).order_by('creationTime').reverse()

        serializer = ProjectListSerializer(projects, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def my_project_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        # category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        # sort=request.data['sort']

        projects = Project.objects.all().filter(creator=learner.user)

        # if sort=="budget":
        #     projects = Project.objects.all().filter(category=category).order_by('maxBudget').reverse()
        # elif sort=="bids":
        #     projects = Project.objects.all().filter(category=category).order_by('dueTime').reverse()
        # elif sort=="time":
        #     projects = Project.objects.all().filter(category=category).order_by('creationTime').reverse()

        serializer = ProjectListSerializer(projects, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def proposal_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        project = Project.objects.all().filter(projectKey=request.data['projectKey'])[0]


        proposals = Proposal.objects.all().filter(project=project)

        # if sort=="budget":
        #     projects = Project.objects.all().filter(category=category).order_by('maxBudget').reverse()
        # elif sort=="bids":
        #     projects = Project.objects.all().filter(category=category).order_by('dueTime').reverse()
        # elif sort=="time":
        #     projects = Project.objects.all().filter(category=category).order_by('creationTime').reverse()

        serializer = ProposalSerializer(proposals, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def my_proposals(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        sort=request.data['sort']
        proposals = Proposal.objects.all().filter(learner=learner)

        if sort=="bid":
            proposals = Proposal.objects.all().filter(learner=learner).order_by('bid').reverse()
        elif sort=="time":
            proposals = Proposal.objects.all().filter(learner=learner).order_by('duration').reverse()

        serializer = ProposalSerializer(proposals, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def delete_proposal(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        proposal = Proposal.objects.all().filter(proposalKey=request.data['proposalKey'])[0]
        proposal.delete()
        return Response({"status":"OK","object":"deleted"},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def proposal_edit(request):

    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        proposal = Proposal.objects.all().filter(proposalKey=request.data['proposalKey'])[0]
        keys=request.data.keys()

        for key in keys:
            if key == "description":
                proposal.description = request.data["description"]
                proposal.save()
            if key == "duration":
                proposal.duration = request.data["duration"]
                proposal.save()
            if key == "bid":
                proposal.bid = request.data["bid"]
                proposal.save()

        serializer = ProposalSerializer(proposal, many=False,context={'learnerKey': learner_.learnerKey})

        return Response({"status":"OK","updated":keys,"object":serializer.data},status=status.HTTP_201_CREATED)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)



# @api_view(['POST'])
# def my_threads(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]
#
#         sort = request.data['sort']
#
#         threads = Thread.objects.all().filter(category=category, learner=learner)
#
#         if sort == "time":
#             threads = Thread.objects.all().filter(category=category, learner=learner).order_by('time').reverse()
#         elif sort == "votes":
#             threads = Thread.objects.all().filter(category=category, learner=learner).order_by('upVotes').reverse()
#         elif sort == "views":
#             threads = Thread.objects.all().filter(category=category, learner=learner).order_by('views').reverse()
#
#         serializer = ThreadListSerializer(threads, many=True, context={'learnerKey': learner.learnerKey})
#
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
#


# @api_view(['POST'])
# def answer_to_thread(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         keys=request.data.keys()
#         thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]
#
#         subthread = SubThread(body=request.data['description'], learner=learner)
#         subthread.save()
#
#         if 'threadFile' in keys:
#             file = request.data['threadFile']
#             subthread.threadFile=file
#             subthread.save()
#
#         thread.subthreads.add(subthread)
#         thread.save()
#
#         # x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
#         # x.start()
#
#         serializer = SubThreadSerializer(subthread, many=False ,context={'learnerKey': learner.learnerKey})
#
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

# @api_view(['POST'])
# def reply_to_answer(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         keys=request.data.keys()
#         thread = Thread.objects.all().filter(threadKey=request.data['threadKey'])[0]
#
#         subthread = SubThread.objects.all().filter(subthreadKey=request.data['subthreadKey'])[0]
#
#         rpl = ThreadReply(body=request.data['description'], learner=learner)
#         rpl.save()
#
#         if 'threadFile' in keys:
#             file = request.data['threadFile']
#             rpl.threadFile=file
#             rpl.save()
#
#         subthread.replies.add(rpl)
#         subthread.save()
#
#         # x = threading.Thread(target=tags_update, args=(thread, request.data['tags'],))
#         # x.start()
#
#         serializer = ReplySerializer(rpl, many=False,context={'learnerKey': learner.learnerKey})
#
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


# @api_view(['POST'])
# def discuss_vote(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         type=request.data['voteType']
#         side=request.data['voteSide']
#
#         if type=='thread':
#             try:
#                 thread = Thread.objects.all().filter(threadKey=request.data['key'])[0]
#
#                 if Log.objects.all().filter(action="Vote_"+side,thread=thread,learner=learner).exists():
#                     return Response(
#                         {"status": "OK", "object": "Already Voted!", "vote": (thread.upVotes - thread.downVotes)},
#                         status=status.HTTP_400_BAD_REQUEST)
#                 else:
#                     Log.objects.all().filter(thread=thread,learner=learner).delete()
#
#                 if side=="up":
#                     thread.upVotes=thread.upVotes +1
#                     thread.save()
#                 elif side=="down":
#                     thread.downVotes=thread.downVotes +1
#                     thread.save()
#                 else:
#                     return Response({"status": "error", "object": "Undefined Side!"},
#                                     status=status.HTTP_400_BAD_REQUEST)
#
#                 log= Log(action="Vote_"+side,thread=thread,learner=learner)
#                 log.save()
#
#
#                 thread.upVotes = Log.objects.all().filter(action="Vote_up",thread=thread,learner=learner).count()
#                 thread.downVotes = Log.objects.all().filter(action="Vote_down",thread=thread,learner=learner).count()
#                 thread.save()
#
#                 return Response({"status": "OK", "object": "Voted!", "vote":(thread.upVotes-thread.downVotes)}, status=status.HTTP_200_OK)
#             except Exception as e:
#                 return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)
#
#         if type=='subthread':
#             try:
#                 subthread = SubThread.objects.all().filter(subthreadKey=request.data['key'])[0]
#
#                 if Log.objects.all().filter(action="Vote_"+side,subThread=subthread,learner=learner).exists():
#                     return Response(
#                         {"status": "OK", "object": "Already Voted!", "vote": (subthread.upVotes - subthread.downVotes)},
#                         status=status.HTTP_400_BAD_REQUEST)
#                 else:
#                     Log.objects.all().filter(subThread=subthread,learner=learner).delete()
#
#                 if side=="up":
#                     subthread.upVotes=subthread.upVotes +1
#                     subthread.save()
#                 elif side=="down":
#                     subthread.downVotes=subthread.downVotes +1
#                     subthread.save()
#                 else:
#                     return Response({"status": "error", "object": "Undefined Side!"},
#                                     status=status.HTTP_400_BAD_REQUEST)
#
#                 log= Log(action="Vote_"+side,subThread=subthread,learner=learner)
#                 log.save()
#
#                 subthread.upVotes = Log.objects.all().filter(action="Vote_up",subThread=subthread,learner=learner).count()
#                 subthread.downVotes = Log.objects.all().filter(action="Vote_down",subThread=subthread,learner=learner).count()
#                 subthread.save()
#
#                 return Response({"status": "OK", "object": "Voted!", "vote":(subthread.upVotes-subthread.downVotes)}, status=status.HTTP_200_OK)
#             except Exception as e:
#                 # return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)
#                 return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)
#
#         if type=='reply':
#             try:
#                 rpl = ThreadReply.objects.all().filter(replyKey=request.data['key'])[0]
#
#                 if Log.objects.all().filter(action="Vote_"+side,threadReply=rpl,learner=learner).exists():
#                     return Response({"status": "OK", "object": "Already Voted!", "vote": (rpl.upVotes - rpl.downVotes)},
#                                     status=status.HTTP_400_BAD_REQUEST)
#                 else:
#                     Log.objects.all().filter(threadReply=rpl, learner=learner).delete()
#
#                 if side=="up":
#                     rpl.upVotes=rpl.upVotes +1
#                     rpl.save()
#                 elif side=="down":
#                     rpl.downVotes=rpl.downVotes +1
#                     rpl.save()
#                 else:
#                     return Response({"status": "error", "object": "Undefined Side!"},
#                                     status=status.HTTP_400_BAD_REQUEST)
#
#                 log= Log(action="Vote_"+side,threadReply=rpl,learner=learner)
#                 log.save()
#
#                 rpl.upVotes = Log.objects.all().filter(action="Vote_up",threadReply=rpl, learner=learner).count()
#                 rpl.downVotes = Log.objects.all().filter(action="Vote_down",threadReply=rpl, learner=learner).count()
#                 rpl.save()
#
#                 return Response({"status": "OK", "object": "Voted!", "vote":(rpl.upVotes-rpl.downVotes)}, status=status.HTTP_200_OK)
#             except Exception as e:
#                 return Response({"status": "error", "object": "Key Does not match VoteType!"}, status=status.HTTP_400_BAD_REQUEST)
#
#
#
#     except Exception as e:
#         return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
#
