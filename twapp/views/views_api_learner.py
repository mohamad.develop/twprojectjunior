from django.core.mail import send_mail
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.utils import translation

from twapp.models import AppSession, Asset, Currency, Attend, Challenge, Course, Achievable, Achievement, Learner, \
    Content, Follow, Wallet
from twapp.serializers import LearnerSeralizer, AssetSeralizer, AttendSerializer, ChallengeSerializer, \
    AchievementSerializer, LearnerConnectionSerializer, ContentSerializer, WalletSeralizer,SessionSeralizer

import threading
import requests
import json
import random
import string
from datetime import datetime
from urllib.request import urlopen

from .rule_exe import execute_level_rules

def otp_send(email,otp_str):
    send_mail(
        "Your Verification Code is " + otp_str,
        "Welcome to Tech Wich, technology learning platform. \nyour OTP: " + otp_str,
        'verification@techwich.net',
        [email],
        fail_silently=False)

def otp_send_forget(email,otp_str):
    send_mail(
        "Your OTP to change Password: " + otp_str,
        "Some one tried to change your TechWich account password \nuse this one time password to do this: " + otp_str+"\nIf you did not send any request just ignore this message.",
        'verification@techwich.net',
        [email],
        fail_silently=False)

def google_register_email(email,uname):
    send_mail(
        "Welcome to Tech Wich ",
        "You are logged in to Twitter for the first time through your Google Account.\nWe recommend that you always use the same login method.\nHowever, you are also assigned a username, the information of which is displayed below.\n"+uname,
        'verification@techwich.net',
        [email],
        fail_silently=False)


def learner_assets_populate(learner_):
    currencies = Currency.objects.all()  # All currencies in app

    for cr in currencies:
        if not Asset.objects.all().filter(currency=cr , learner=learner_).exists():
            asset_=Asset(currency=cr , learner=learner_)
            asset_.save()

def learner_wallet_populate(learner_):
    if not Wallet.objects.all().filter(learner=learner_).exists():
            wallet_=Wallet(learner=learner_)
            wallet_.save()

def user_levels_populate(learner_):
    achievables = Achievable.objects.all().filter(general=True)

    for achievable in achievables:
        if not Achievement.objects.all().filter(achievable=achievable, learner=learner_).exists():
            achivement_ = Achievement(achievable=achievable, learner=learner_, value="0", icon=achievable.defaultIcon)
            achivement_.save()

def log_user_view(learner_):
    learner_.profileViews=learner_.profileViews+1
    learner_.save()


@api_view(['POST'])
def profile_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)

        learner = userSession.user.learner

        # learners__=Learner.objects.all()
        #
        # for lnr in learners__:
        #     try:
        #         lnr.save()
        #     except:
        #         print('a')

        if request.data['tab'] =="follower":
            # Followers
            followers_keys = Follow.objects.all().filter(followedKey=learner.learnerKey)
            followers = []
            for fl in followers_keys:
                flr = Learner.objects.all().filter(learnerKey=fl.followerKey)[0]
                followers.append(flr)
            serializer = LearnerConnectionSerializer(followers, many=True, context={'learnerKey': learner.learnerKey})
            return Response(serializer.data, status=status.HTTP_200_OK)

        if request.data['tab'] =="following":
            # Following
            following_keys = Follow.objects.all().filter(followerKey=learner.learnerKey)
            followeings = []
            for fl in following_keys:
                print(fl.followedKey)
                flr = Learner.objects.all().filter(learnerKey=fl.followedKey)[0]
                followeings.append(flr)
            serializer = LearnerConnectionSerializer(followeings, many=True, context={'learnerKey': learner.learnerKey})
            return Response(serializer.data, status=status.HTTP_200_OK)

        if request.data['tab'] =="suggestions":
            # Following
            suggests = Learner.objects.all().order_by('?')[:10]
            serializer = LearnerConnectionSerializer(suggests, many=True, context={'learnerKey': learner.learnerKey})
            return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def learner_profile(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        try:
            learner=Learner.objects.all().filter(learnerKey=request.data['learnerKey'])[0]
            learner_cur = userSession.user.learner
        except:
            learner = userSession.user.learner
            learner_cur = userSession.user.learner

        a = threading.Thread(target=learner_assets_populate, args=(learner,))
        a.start()
        p = threading.Thread(target=user_levels_populate, args=(learner,))
        p.start()
        v = threading.Thread(target=log_user_view, args=(learner,))
        v.start()
        w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        w.start()

        level_thread = threading.Thread(target=execute_level_rules,args=(learner,))
        level_thread.start()

        final = {}

        # Achievement

        achivements=Achievement.objects.all().filter(learner=learner)
        seralizer=AchievementSerializer(achivements, many=True)
        final["achievements"] = seralizer.data

        # Assets
        assets= Asset.objects.all().filter(learner=learner)
        serializer = AssetSeralizer(assets, many=True)
        final["assets"]=serializer.data

        # Connection
        # connections= Learner.objects.all().filter(friends__in=[learner])
        # serializer = LearnerConnectionSerializer(connections, many=True, context={'learnerKey': learner.learnerKey})
        # final["connections"]=serializer.data
        final["connectionCount"]=Learner.objects.all().filter(friends__in=[learner]).count()

        # Contents
        contents = Content.objects.all().filter(owner=learner).order_by("-time")
        serializer = ContentSerializer(contents, many=True, context={'learnerKey': learner.learnerKey})
        final["contents"] = serializer.data

        # Followers
        final["followerCount"] = Follow.objects.all().filter(followedKey=learner.learnerKey).count()

        # Following
        final["followingCount"]= Follow.objects.all().filter(followerKey=learner.learnerKey).count()

        # Level

        level={}

        level['currentStreak'] = learner.dailyStreak
        level['currentForumActivity'] = learner.forumActivity
        level['currentPoints'] = learner.points
        level['maxForumActivity'] = 25
        level['maxPoints'] = 46
        level['maxStreak'] = 15
        level['percentage'] = 27

        final["levelStatistic"] = level

        # User
        user = {}
        user["username"] = learner.user.username
        user["learnerKey"] = learner.learnerKey
        user['firstName'] = learner.firstName
        user['lastName'] = learner.lastName
        user['isPro'] = learner.isPro
        user['isPrivate'] = learner.isPrivate
        user['profilePicture'] = learner.profilePicture.url
        user['level'] = learner.level
        user['bio'] = learner.bio
        user['birthday'] = learner.birthday
        user['email'] = learner.user.email
        final["user"] = user
        final["learnerKey"] = learner.learnerKey

        # Wallet
        wallet= Wallet.objects.all().filter(learner=learner)[0]
        serializer = WalletSeralizer(wallet, many=False)
        final["wallet"]=serializer.data

        final["isFollower"] = Follow.objects.all().filter(followedKey=learner_cur.learnerKey,
                                                          followerKey=learner.learnerKey).exists()

        final["isFollowing"] = Follow.objects.all().filter(followerKey=learner_cur.learnerKey,
                                                           followedKey=learner.learnerKey).exists()

        return Response(final,status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def learner_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:

        x = threading.Thread(target=user_levels_populate, args=(userSession.user.learner))
        x.start()

        user = {}
        user["username"] = userSession.user.username
        user["learnerKey"] = userSession.user.learner.learnerKey
        user['firstName'] = userSession.user.learner.firstName
        user['lastName'] = userSession.user.learner.lastName
        user['birthday'] = userSession.user.learner.birthday
        user['gender'] = userSession.user.learner.gender
        user['isPro'] = userSession.user.learner.isPro
        user['email'] = userSession.user.email
        user['profilePicture'] = userSession.user.learner.profilePicture.url
        final = {}
        final["bio"] = userSession.user.learner.bio
        final["isPrivate"] = userSession.user.learner.isPrivate
        final["friendsCount"] = len(userSession.user.learner.friends.all())
        final["playGroundsCount"] = 0
        final["state"] = 0
        final['dailyStreak'] = userSession.user.learner.dailyStreak
        final['level'] = userSession.user.learner.level
        final['forumActivity'] = userSession.user.learner.forumActivity
        final['points'] = userSession.user.learner.points
        final["user"] = user

        # tickets=Ticket.objects.all().filter(learner=sess.user.learner)
        # serializer = TicketSeralizer(tickets, many=True)
        # final["tickets"]=serializer.data
        #
        # following=sess.user.learner.friends.all()
        # followingserializer = ProfileSeralizer(following, many=True, context={'selfLearner': sess.user.learner})
        # final["followings"]=followingserializer.data
        #
        # follower=Learner.objects.all().filter(friends__in=[sess.user.learner])
        # followerserializer = ProfileSeralizer(follower, many=True, context={'selfLearner': sess.user.learner})
        # final["followers"]=followerserializer.data

        return Response(final,status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def learner_assets(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:

        learner = userSession.user.learner

        currencies = Currency.objects.all()

        for cr in currencies:
            if not Asset.objects.all().filter(currency=cr , learner=learner).exists():
                asset_=Asset(currency=cr , learner=learner)
                asset_.save()

        assets= Asset.objects.all().filter(learner=learner)
        serializer = AssetSeralizer(assets, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def learner_courses(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        learner = userSession.user.learner
        attends = Attend.objects.all().filter(learner=learner)
        serializer = AttendSerializer(attends, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def learner_challenges(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        serializer = LearnerSeralizer(userSession.user.learner, many=False)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        learner = userSession.user.learner

        challenges = Challenge.objects.all().filter(course_in=Course.objects.all().filter(Attend.objects.all().filter(learner=learner).values('course_set')))

        serializer = ChallengeSerializer(challenges, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def app_google_auth(request):
    response = requests.get("https://oauth2.googleapis.com/tokeninfo?id_token="+request.data['token'])
    txt = response.text
    print(txt)
    print(type(txt))
    data= json.loads(txt)

    print(data)

    if User.objects.all().filter(email=data['email']).exists():

        user=User.objects.all().filter(email=data['email'])[0]
        print(user)
        AppSession.objects.filter(user=user).delete()

        session = AppSession.objects.create(isActive=True, token=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147)))
        session.user = user
        session.save()
        serializer = SessionSeralizer(session, many=False)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    else:
        user=User(username=data["given_name"]+"_"+data["family_name"]+"_"+''.join(
            random.choice(string.digits) for _ in range(3)),email=data["email"],first_name=data["given_name"],
                  last_name=data["family_name"])
        user.save()

        # x = threading.Thread(target=google_register_email, args=(data['email'],user.username))
        # x.start()

        learner=Learner(firstName=data["given_name"],
                  lastName=data["family_name"],user=user)
        learner.save()

        img_temp = NamedTemporaryFile(delete=True)
        img_temp.write(urlopen(data['picture']).read())
        img_temp.flush()
        img_filename=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147))+".jpg"

        learner.profilePicture.save(img_filename, File(img_temp))
        #
        # learner.profilePicture.url=data['picture']
        # learner.save()

        a = threading.Thread(target=learner_assets_populate, args=(learner,))
        a.start()
        p = threading.Thread(target=user_levels_populate, args=(learner,))
        p.start()
        v = threading.Thread(target=log_user_view, args=(learner,))
        v.start()
        w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        w.start()

        session = AppSession.objects.create(isActive=True, token=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147)))
        session.user = user
        session.save()
        serializer = SessionSeralizer(session, many=False)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    return Response(data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def app_google_auth_2(request):
    response = requests.get("https://www.googleapis.com/oauth2/v3/userinfo?access_token="+request.data['token'])
    txt = response.text
    print(txt)
    print(type(txt))
    data= json.loads(txt)

    print(data)

    if User.objects.all().filter(email=data['email']).exists():

        user=User.objects.all().filter(email=data['email'])[0]
        print(user)
        AppSession.objects.filter(user=user).delete()

        session = AppSession.objects.create(isActive=True, token=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147)))
        session.user = user
        session.save()
        serializer = SessionSeralizer(session, many=False)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    else:
        user=User(username=data["given_name"]+"_"+data["family_name"]+"_"+''.join(
            random.choice(string.digits) for _ in range(3)),email=data["email"],first_name=data["given_name"],
                  last_name=data["family_name"])
        user.save()

        # x = threading.Thread(target=google_register_email, args=(data['email'],user.username))
        # x.start()

        learner=Learner(firstName=data["given_name"],
                  lastName=data["family_name"],user=user)
        learner.save()

        img_temp = NamedTemporaryFile(delete=True)
        img_temp.write(urlopen(data['picture']).read())
        img_temp.flush()
        img_filename=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147))+".jpg"

        learner.profilePicture.save(img_filename, File(img_temp))
        #
        # learner.profilePicture.url=data['picture']
        # learner.save()

        a = threading.Thread(target=learner_assets_populate, args=(learner,))
        a.start()
        p = threading.Thread(target=user_levels_populate, args=(learner,))
        p.start()
        v = threading.Thread(target=log_user_view, args=(learner,))
        v.start()
        w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        w.start()

        session = AppSession.objects.create(isActive=True, token=''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(147)))
        session.user = user
        session.save()
        serializer = SessionSeralizer(session, many=False)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    return Response(data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def profile_pic(request):
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        file=request.data['file']
        learner=sess.user.learner
        print(file)
        learner.profilePicture=file
        learner.save()

        serializer = LearnerSeralizer(learner, many=False)

        return Response(serializer.data,status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def change_language(request):
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner=sess.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        learner.settings_lang=request.data['language']
        learner.save()

        serializer = LearnerSeralizer(learner, many=False)

        return Response(serializer.data,status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def push_token(request):
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        learner.pushToken=request.data['pushToken']
        learner.save()

        serializer = LearnerSeralizer(learner, many=False)

        return Response(serializer.data,status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def user_edit(request):
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner_=sess.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
    try:
        serializer = LearnerSeralizer(sess.user.learner, many=False)
        keys=request.data.keys()
        for key in keys:
            if key=="gender":
                sess.user.learner.gender=request.data["gender"]
                sess.user.learner.save()
            if key=="firstName":
                sess.user.learner.firstName=request.data["firstName"]
                sess.user.learner.save()
            if key=="lastName":
                sess.user.learner.lastName=request.data["lastName"]
                sess.user.learner.save()
            if key=="bio":
                sess.user.learner.bio=request.data["bio"]
                sess.user.learner.save()
            if key=="isPrivate":
                sess.user.learner.isPrivate=request.data["isPrivate"]
                sess.user.learner.save()
            if key == "birthday":
                date_=datetime.strptime(request.data["birthday"], "%Y-%m-%d").date()
                sess.user.learner.birthday = date_
                sess.user.learner.save()
            if key=="email":
                if User.objects.all().filter(email=request.data["email"]).exists():
                    user_by_email=User.objects.all().filter(email=request.data["email"])[0]
                    if user_by_email.learner.learnerKey != sess.user.learner.learnerKey:
                        return Response({"status": "error", "object": "Email is in use by another learner."}, status=status.HTTP_400_BAD_REQUEST)
                sess.user.email=request.data["email"]
                sess.user.save()
            if key=="username":
                if User.objects.all().filter(username=request.data["username"]).exists():
                    user_by_username=User.objects.all().filter(username=request.data["username"])[0]
                    if user_by_username.learner.learnerKey != sess.user.learner.learnerKey:
                        return Response({"status": "error", "object": "Username is in use by another learner."}, status=status.HTTP_400_BAD_REQUEST)
                sess.user.username=request.data["username"]
                sess.user.save()
        return Response({"status":"OK","updated":keys},status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)
