from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.utils import timezone

from twapp.models import AppSession, Learner, SuperLeague, Result, Team , TeamInvite, TeamRequest, LearnerSuperLeagueStatus

from twapp.serializers import SuperLeagueSeralizer

import json


def updateLearnerSuperLeagueStatuses(learner,superleague): #useless here
    if LearnerSuperLeagueStatus.objects.all().filter(learner=learner,superLeague=superleague).exists():
        status_=LearnerSuperLeagueStatus.objects.all().filter(learner=learner,superLeague=superleague)[0]
    else:
        status_=LearnerSuperLeagueStatus(learner=learner,superLeague=superleague)
        status_.save()

    teams = learner.learnerTeams.all()

    try:
    # if (1==1):
        if timezone.now() > superleague.startTime and timezone.now() < superleague.endTime:
            for team in teams:
                team_sl = team.teamSuperLeague.all()[0]
                if team_sl.superLeagueKey == superleague.superLeagueKey:
                    if team.learners.count() < superleague.teamsCount and team.ownerKey == learner.learnerKey:
                        status_.status = 1
                        status_.team = team
                        status_.save()
                        return True
                    elif team.learners.count() < superleague.teamsCount and team.ownerKey != learner.learnerKey:
                        status_.status = 2
                        status_.team = team
                        status_.save()
                        return True
                    elif team.learners.count() == superleague.teamsCount and team.ownerKey != learner.learnerKey:
                        status_.status = 4
                        status_.team = team
                        status_.save()
                        return True
                    elif team.learners.count() == superleague.teamsCount and team.ownerKey == learner.learnerKey:
                        status_.status = 5
                        status_.team = team
                        status_.save()
                        return True
                if TeamRequest.objects.all().filter(team=team, learner=learner).exists():
                    status_.status = 6
                    status_.save()
                    return True
            status_.status = 3
            status_.save()
            return True
        elif timezone.now() > superleague.endTime:
            for team in teams:
                team_sl = team.teamSuperLeague.all()[0]
                if team_sl.superLeagueKey == superleague.superLeagueKey:
                    status_.status = 8
                    status_.team = team
                    status_.save()
                    return True
            status_.status = 9
            status_.save()
            return True
        else:
            for team in teams:
                team_sl = team.teamSuperLeague.all()[0]
                if team_sl.superLeagueKey == superleague.superLeagueKey:
                    if team.ownerKey == learner.learnerKey:
                        status_.status = 11
                        status_.team = team
                        status_.save()
                        return True
                    else:
                        status_.status = 12
                        status_.team = team
                        status_.save()
                        return True

            status_.status = 10
            status_.save()
            return True
        status_.status = 0
        status_.save()
        return True
    except Exception as e:
        print("---->"+str(e))
        status_.status = -1
        status_.save()
        return True


@api_view(['POST'])
def super_league_create_team(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        super_league = SuperLeague.objects.all().filter(superLeagueKey=request.data['superLeagueKey'])[0]
        team_logo_file=request.data['teamLogo']

        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not create team after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not create team 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        if team_logo_file =='':
            return Response({"status": "error", "object": "Team Logo is required."}, status=status.HTTP_400_BAD_REQUEST)

        team = Team(name=request.data['name'], ownerKey=learner.learnerKey, course=super_league.course,
                    category=super_league.course.category)
        team.save()

        team.teamLogo=team_logo_file
        team.save()

        team.learners.add(learner)
        team.save()

        print(request.data['inviteKeys'])

        ik=request.data['inviteKeys']

        if not ik == '':
            ikeys = json.loads(ik)
            for item in ikeys:
                inv_learner = Learner.objects.all().filter(learnerKey=item)[0]
                team_invite_ = TeamInvite(team=team, learner=inv_learner)
                team_invite_.save()

        super_league.teams.add(team)
        super_league.save()

        res=Result(team=team,superLeague=super_league)
        res.save()

        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_cancel_request(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:

        request = TeamRequest.objects.all().filter(teamRequestKey=request.data['teamRequestKey'])[0]
        team= request.team
        super_league=team.teamSuperLeague.all()[0]

        if request.status != "new":
            return Response({"status": "error", "object": "Request accepted or rejected already."}, status=status.HTTP_400_BAD_REQUEST)

        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not cancel requests after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not cancel requests 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        request.status = "canceled"
        request.save()

        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def super_league_leave_team(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        team = Team.objects.all().filter(teamKey=request.data['teamKey'])[0]
        super_league=team.teamSuperLeague.all()[0]

        if not learner in team.learners.all():
            return Response({"status": "error", "object": "You are not a member of this team."}, status=status.HTTP_400_BAD_REQUEST)

        if super_league.startTime < timezone.now():
            return Response({"status": "error", "object": "You can not leave after super league started."}, status=status.HTTP_400_BAD_REQUEST)

        dft = super_league.startTime - timezone.now()
        dft_str= str(dft)

        if (not "day" in dft_str) or ("-" in dft_str):
            return Response({"status": "error", "object": "You can not leave 24 hours before super league start."},
                            status=status.HTTP_400_BAD_REQUEST)

        team.learners.remove(learner)
        team.save()

        # un = threading.Thread(target=updateLearnerSuperLeagueStatuses, args=(learner,super_league))
        # un.start()

        updateLearnerSuperLeagueStatuses(learner,super_league)

        serializer = SuperLeagueSeralizer(team.teamSuperLeague.all().last(), many=False,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

