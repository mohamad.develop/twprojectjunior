from rest_framework.decorators import api_view

from twapp.models import Lecture, AppSession, ShortcutTry

from rest_framework import status
from rest_framework.response import Response

from django.utils import translation

from twapp.serializers import ExampleDetailSerializer


def handleRun(run):
    # Fix
    if run.example.type=='playground':
        run.verdict = (run.statusChecks == 2)
        if (run.statusChecks == 2):
            run.status = 'completed'
        run.save()
        return True
    if run.example.type=='multi':
        print("x"*10+"Answer: "+run.code[0])
        print("x"*10+"True: "+run.example.answer)
        run.verdict = (run.code[0]==run.example.answer)
        run.status = 'completed'
        run.save()
        return True
    run.verdict = (False)
    run.status = 'completed'
    run.save()
    return False

@api_view(['POST'])
def shortcut_questions(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
        translation.activate(user_lang)
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        lesson = Lecture.objects.all().filter(lectureKey=request.data['lectureKey'])[0]
        shtry = ShortcutTry(learner=learner,lesson=lesson,shortcut=lesson.shortcut)
        shtry.save()

        questions=lesson.examples.all().filter(type='multi')[:lesson.shortcut.questionCount]

        serializer = ExampleDetailSerializer(questions, many=True)
        return Response({"tryKey":shtry.shortcutTryKey,"questions":serializer.data},status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def shortcut_answer(request):
    return Response({"status":"OK"}, status=status.HTTP_200_OK)
    # try:
    #     userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
    #     user_lang = request.headers['languageKey']
    #     translation.activate(user_lang)
    #
    #     learner = userSession.user.learner
    #     example = Example.objects.all().filter(exampleKey=request.data['exampleKey'])[0]
    #
    #     run=Run(learner=learner,example=example,code=request.data['answer'])
    #     run.save()
    #
    #     r = threading.Thread(target=handleRun, args=(run,))
    #     r.start()
    #
    #     serializer = RunSerializer(run, many=False)
    #     return Response(serializer.data,status=status.HTTP_200_OK)
    #
    # except Exception as e:
    #     return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)


@api_view(['POST'])
def shortcut_apply(request):
    return Response({"status":"OK", "passed":False}, status=status.HTTP_200_OK)
    # try:
    #     userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
    #     user_lang = request.headers['languageKey']
    #     translation.activate(user_lang)
    #
    #     learner = userSession.user.learner
    #     run = Run.objects.all().filter(runKey=request.data['runKey'])[0]
    #     run.statusChecks=run.statusChecks+1
    #     run.save()
    #
    #     # Fix : remove after code compiles are OK!!
    #     if run.status=='running':
    #         r = threading.Thread(target=handleRun, args=(run,))
    #         r.start()
    #
    #     serializer = RunSerializer(run, many=False)
    #     return Response(serializer.data,status=status.HTTP_200_OK)
    #
    # except Exception as e:
    #     return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)