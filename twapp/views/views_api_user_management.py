import string
import random

from rest_framework.decorators import api_view
from django.contrib.auth.models import User

from twapp.models import Learner, OTP, AppSession, Tag, Achievement, Wallet, Achievable, Currency, Asset

from rest_framework import status
from rest_framework.response import Response
import threading
from django.conf import settings
from django.core.mail import send_mail

import requests
from django.utils import translation

from twapp.serializers import SessionSeralizer, \
    UserSerializer


def tags_update(content_,tags_):

    if not tags_ == '':
        tgs = json.loads(tags_)
        content_.tags.clear()
        content_.save()
        for item in tgs:
            tag_ = Tag.objects.all().filter(tagKey=item)[0]
            content_.tags.add(tag_)
        content_.save()
    else:
        content_.tags.clear()
        content_.save()


def learner_assets_populate(learner_):
    currencies = Currency.objects.all()  # All currencies in app

    for cr in currencies:
        if not Asset.objects.all().filter(currency=cr , learner=learner_).exists():
            asset_=Asset(currency=cr , learner=learner_)
            asset_.save()

def learner_wallet_populate(learner_):
    if not Wallet.objects.all().filter(learner=learner_).exists():
            wallet_=Wallet(learner=learner_)
            wallet_.save()

def user_levels_populate(learner_):
    achievables = Achievable.objects.all().filter(general=True)

    for achievable in achievables:
        if not Achievement.objects.all().filter(achievable=achievable, learner=learner_).exists():
            achivement_ = Achievement(achievable=achievable, learner=learner_, value="0", icon=achievable.defaultIcon)
            achivement_.save()

def log_user_view(learner_):
    learner_.profileViews=learner_.profileViews+1
    learner_.save()


def otp_send(email,otp_str):
    send_mail(
        "Your Verification Code is " + otp_str,
        "Welcome to Tech Wich, technology learning platform. \nyour OTP: " + otp_str,
        'verification@techwich.net',
        [email],
        fail_silently=False)

def otp_send_forget(email,otp_str):
    send_mail(
        "Your OTP to change Password: " + otp_str,
        "Some one tried to change your TechWich account password \nuse this one time password to do this: " + otp_str+"\nIf you did not send any request just ignore this message.",
        'verification@techwich.net',
        [email],
        fail_silently=False)

def google_register_email(email,uname):
    send_mail(
        "Welcome to Tech Wich ",
        "You are logged in to Twitter for the first time through your Google Account.\nWe recommend that you always use the same login method.\nHowever, you are also assigned a username, the information of which is displayed below.\n"+uname,
        'verification@techwich.net',
        [email],
        fail_silently=False)


@api_view(['POST'])
def send_verify_email(request):
    try:
        # sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        usr_ = User.objects.all().filter(email=request.data['email']).reverse()[0]
        OTP.objects.filter(user=usr_).delete()
        otp_=OTP.objects.create(user=usr_)
        otp_.otp=str(random.randint(1000, 9999))
        otp_.save()

        x = threading.Thread(target=otp_send, args=(request.data['email'],otp_.otp))
        x.start()
        return Response({"status": "OK", "sentTo": request.data['email']}, status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def verify_by_email(request):
    try:
        usr_ = User.objects.all().filter(username=request.data['username'])[0]
        otp_=OTP.objects.filter(user=usr_)[0]
        if request.data['otp']==otp_.otp:
            session = AppSession.objects.create(isActive=True, token=''.join(
                random.choice(string.ascii_uppercase + string.digits) for _ in range(90)))
            session.user = usr_
            session.save()
            serializer = SessionSeralizer(session, many=False)
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({"status":"ok","message":"wrong otp","array":[],"object":{}},status=status.HTTP_406_NOT_ACCEPTABLE)
    except Exception as e:
        return Response({"status":"error","message":"Invalid Data","array":[],"object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def forget_password(request):
    try:
        user = User.objects.all().filter(email=request.data['email'])[0]
        try:
            user.otp.delete()
        except:
            print("was none")

        otp = OTP.objects.create(user=user)
        otp.save()
        x = threading.Thread(target=otp_send_forget, args=(request.data['email'],otp.otp))
        x.start()
        return Response({"status":"OK","sentTo":request.data['email']},status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"status":"error","object":{str(e)}},status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def app_change_password(request):
    print(request.headers['sessionKey'])
    try:
        sess = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        if sess.user.check_password(request.data['current']):
            sess.user.set_password(request.data['newPassword'])
            sess.user.save()
            return Response({"status": "ok"}, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({"status": "ok", "message": "wrong password", "array": [], "object": {}},status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        return Response({"status":"error","message":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def change_forgotten_password(request):
    try:
        user = User.objects.all().filter(email=request.data['email'])[0]
        otp = OTP.objects.all().filter(user=user)[0]
        # if otp.otp==request.data['otp']:
        if request.data['otp']==otp.otp:
            user.set_password(request.data['newPassword'])
            user.save()
            otp.delete()
            return Response({"status": "ok"}, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({},status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        return Response({"status":"error","message":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def learner_register(request):
    serialized = UserSerializer(data=request.data)

    if User.objects.all().filter(email=request.data['email']).exists():
        return Response({"status":"error","message":"Username is Taken or Email is in use.","array":[],"object":{}},status=status.HTTP_400_BAD_REQUEST)

    if serialized.is_valid():
        print(serialized.validated_data)
        usr=serialized.save()
        print(usr)

        learner = Learner.objects.create(user=usr)

        # med.get_remote_image("https://ui-avatars.com/api/?background=random&name="+usr.username+"&size=256")
        url = "https://ui-avatars.com/api/?background=random&name="+usr.username+"&size=256"
        req = requests.get(url)
        # Adjust the names and paths below to fit your project
        filename = '/profilepics/'+usr.username+'_prpc.jpg'
        with open(settings.MEDIA_DIR + filename, "wb+") as f:
            f.write(req.content)

        learner.profilePicture=filename[1:]
        learner.firstName=usr.username
        learner.save()

        a = threading.Thread(target=learner_assets_populate, args=(learner,))
        a.start()
        p = threading.Thread(target=user_levels_populate, args=(learner,))
        p.start()
        v = threading.Thread(target=log_user_view, args=(learner,))
        v.start()
        w = threading.Thread(target=learner_wallet_populate, args=(learner,))
        w.start()

        return Response({"status":"ok","message":"registered successfully.","array":[],"object":{}},status=status.HTTP_201_CREATED)
    else:
        return Response({"status":"error","message":"Username is Taken or Email is in use.","array":[],"object":{}},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def app_authenticate(request):
    try:
        if "languageKey" in request.headers.keys():
            translation.activate(request.headers['languageKey'])
        user=User.objects.all().filter(username=request.data['username'])[0]
        # try:
        #     user.session.isActive=False
        #     user.session.delete()
        # except:
        #     print("was none")

        if user.check_password(request.data['password']):
            AppSession.objects.filter(user=user).delete()
            session = AppSession.objects.create(isActive=True, token=''.join(
                random.choice(string.ascii_uppercase + string.digits) for _ in range(90))+"-up")
            session.user = user
            session.save()
            serializer = SessionSeralizer(session, many=False)
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({"status":"ok","message":"wrong password","array":[],"object":{}},status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
            return Response({"status":"error","message":str(e),"array":[],"object":{}},status=status.HTTP_401_UNAUTHORIZED)

