from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from twapp.models import AppSession, Category, Webinar
from twapp.serializers import WebinarListSerializer, WebinarEnrollment, WebinarSerializer


@api_view(['POST'])
def webinar_list(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        # sort=request.data['sort']

        webinars = Webinar.objects.all().filter(category=category)

        # if sort=="time":
        #     threads = Thread.objects.all().filter(category=category).order_by('time')
        # elif sort=="votes":
        #     threads = Thread.objects.all().filter(category=category).order_by('upVotes')
        # elif sort=="views":
        #     threads = Thread.objects.all().filter(category=category).order_by('views')

        serializer = WebinarListSerializer(webinars, many=True, context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def my_webinars(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)

    try:

        category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]

        # sort=request.data['sort']


        sub = WebinarEnrollment.objects.all().filter(learner=learner).order_by('?')
        webinars = []
        for s in sub:
            webinars.append(s.webinar)

        serializer = WebinarListSerializer(webinars, many=True ,context={'learnerKey': learner.learnerKey})

        return Response(serializer.data, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def webinar_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        webinar = Webinar.objects.all().filter(webinarKey=request.data['webinarKey'])[0]
        serializer = WebinarSerializer(webinar, many=False)

        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)





# @api_view(['GET'])
# def my_content(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         contents = Content.objects.all().filter(owner=learner)
#         serializer = ContentSerializer(contents, many=True)
#
#
#         return Response(serializer.data,status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
#
#
#
# @api_view(['POST'])
# def category_content(request):
#     try:
#         userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
#         learner = userSession.user.learner
#
#         category = Category.objects.all().filter(categoryKey=request.data['categoryKey'])[0]
#
#         contents = Content.objects.all().filter(category=category)
#         serializer = ContentSerializer(contents, many=True)
#
#
#         return Response(serializer.data,status=status.HTTP_200_OK)
#
#     except Exception as e:
#         return Response({"status":"error","object":str(e)},status=status.HTTP_401_UNAUTHORIZED)
#
#
#
