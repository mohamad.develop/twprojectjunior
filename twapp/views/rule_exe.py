from twapp.models import Rule
from django.utils import timezone


def execute_all_rules():
    print("start executing")

    all_rules=Rule.objects.all()

    for rule in all_rules:
        print(rule.entity)

    return True


def execute_level_rules(learner_instance):
    print("start level executing")

    all_rules = Rule.objects.all().filter(entity="learner",property="score")

    for rule in all_rules:
        if rule.achievable.title=="Level":
            if learner_instance.points >= rule.lowerBound and learner_instance.points <= rule.upperBound:
                learner_instance.level=rule.givenValue
                learner_instance.save()

    return True


def handle_streak(learner_instance):
    print("Handing Steaks")
    today = timezone.now().date()
    yesterday = today - timezone.timedelta(days=1)

    today_string = today.strftime('%Y-%m-%d')
    yesterday_string = yesterday.strftime('%Y-%m-%d')
    print(today_string)
    print(yesterday_string)
    if learner_instance.lastStreak == yesterday_string:
        learner_instance.dailyStreak = learner_instance.dailyStreak + 1
        learner_instance.save()

    learner_instance.lastStreak = today_string
    learner_instance.save()

    return True
