from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from TWProject import settings
from twapp.models import AppSession, Log, \
    Example
from twapp.serializers import Video, EpisodeDetailSerializer

import threading
from django.utils import translation
from .rule_exe import handle_streak

from django.conf import settings
import mimetypes
import os

from django.http import HttpResponse

from .episode_backgrounds import convert_to_hls

# def createAttned(learner,episode):
#     lecture = episode.lecture_set.all()[0]
#     course = lecture.parentCourse
#
#     lg = Log(action="unlocked", learner=run.learner, example=run.example)
#     lg.save()
#
#     if not Attend.objects.filter(learner=learner, course=course).exists():
#         attend = Attend(learner=learner, course=course)
#         attend.save()
#

def update_like_count(episode_):
    episode_.likeCount= Log.objects.all().filter(action="LIKE", video=episode_).count()
    episode_.save()


def unlockNextEpisode(learner,episode):
    lecture = episode.lecture_set.all()[0]
    course=lecture.parentCourse

    sibling_episodes=lecture.episodes.all().order_by('-order').reverse()
    sibling_lectures=course.courseLectures.all().order_by('-order').reverse()

    if episode.videoKey==sibling_episodes[len(sibling_episodes)-1].videoKey:
        next_lecture=lecture
        for i in range(0,len(sibling_lectures)):
            if sibling_lectures[i].lectureKey==lecture.lectureKey:
                next_lecture=sibling_lectures[i+1]
                break
        next_episode=next_lecture.episodes.all().order_by('-order').reverse()[0]
        lg = Log(action="unlocked", learner=learner, course=course, video=next_episode)
        lg.save()
    else:
        next_episode=episode
        for i in range(0,len(sibling_episodes)):
            if sibling_episodes[i].videoKey==episode.videoKey:
                next_episode=sibling_episodes[i+1]
                break
        lg = Log(action="unlocked", learner=learner, course=course, video=next_episode)
        lg.save()


@api_view(['POST'])
def app_episode_detail(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)

        learner = userSession.user.learner
        episode = Video.objects.all().filter(videoKey=request.data['episodeKey'])[0]

        if not episode.hls_created:
            st = threading.Thread(target=convert_to_hls, args=(episode,))
            st.start()

        # update views count
        episode.views=episode.views+1
        episode.save()

        if Example.objects.all().filter(relatedVideo=episode).count() ==0:

            log_passed=Log(action="episodePassed", learner=learner, video=episode)
            log_passed.save()

            un = threading.Thread(target=unlockNextEpisode, args=(learner, episode))
            un.start()

        # create_attend = threading.Thread(target=createAttned, args=(learner,episode))
        # create_attend.start()

        st = threading.Thread(target=handle_streak, args=(learner,))
        st.start()

        serializer = EpisodeDetailSerializer(episode, many=False , context={'learnerKey': learner.learnerKey})
        return Response(serializer.data,status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def app_episode_next(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        user_lang = request.headers['languageKey']
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        translation.activate(user_lang)

        learner = userSession.user.learner
        episode = Video.objects.all().filter(videoKey=request.data['episodeKey'])[0]

        if episode.EpisodeQuestions.all().count() !=0:
            return Response({"link": "/question?key=" + episode.EpisodeQuestions.all()[0].exampleKey}, status=status.HTTP_200_OK)

        lecture = episode.lecture_set.all()[0]
        course = lecture.parentCourse

        sibling_episodes = lecture.episodes.all().order_by('-order').reverse()
        sibling_lectures = course.courseLectures.all().order_by('-order').reverse()

        if episode.videoKey == sibling_episodes[len(sibling_episodes) - 1].videoKey:
            next_lecture = lecture
            for i in range(0, len(sibling_lectures)):
                if sibling_lectures[i].lectureKey == lecture.lectureKey:
                    next_lecture = sibling_lectures[i + 1]
                    break
            next_episode = next_lecture.episodes.all().order_by('-order').reverse()[0]
        else:
            next_episode = episode
            for i in range(0, len(sibling_episodes)):
                if sibling_episodes[i].videoKey == episode.videoKey:
                    next_episode = sibling_episodes[i + 1]
                    break

        return Response({"link":"/videoEpisode?key="+next_episode.videoKey},status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status":"error","object":str(e)},status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def episode_like_toggle(request):
    try:
        userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        learner = userSession.user.learner
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        episode = Video.objects.all().filter(videoKey=request.data['episodeKey'])[0]
        serializer = EpisodeDetailSerializer(episode, many=False , context={'learnerKey': learner.learnerKey})

        status_ret = "liked"
        isLiked_ret=True
        like_ret = 0


        if Log.objects.all().filter(video=episode, action="LIKE", learner=learner).exists():
            status_ret = "unliked"
            isLiked_ret= False
            like_ret = episode.likeCount - 1
            Log.objects.all().filter(video=episode, action="LIKE", learner=learner).delete()
        else:
            status_ret = "liked"
            isLiked_ret= True
            like_ret = episode.likeCount + 1
            log_ = Log(video=episode, action="LIKE", learner=learner)
            log_.save()

        x = threading.Thread(target=update_like_count, args=(episode,))
        x.start()

        return Response({"status": status_ret, "likeCount": like_ret , "isLiked":isLiked_ret}, status=status.HTTP_200_OK)

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def stream_video(request):

    try:
        # userSession = AppSession.objects.all().filter(token=request.headers['sessionKey'])[0]
        # learner = userSession.user.learner
        a=1
    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    try:
        # Retrieve the video object based on the video_id
        try:
            video = Video.objects.all().filter(videoKey=request.GET.get('episodeKey'))[0]
        except Video.DoesNotExist:
            return HttpResponse("Video not found", status=404)

        # Get the full path of the video file
        video_path = os.path.join(settings.MEDIA_ROOT, str(video.videoFile))

        # Check if the file exists
        if not os.path.exists(video_path):
            return HttpResponse("Video not found", status=404)

        # Set the content type header based on the file's MIME type
        content_type, _ = mimetypes.guess_type(video_path)

        # Set the Content-Disposition header to inline
        response = HttpResponse(content_type=content_type)
        response['Content-Disposition'] = 'inline'

        # Set the Cache-Control header to no-cache
        response['Cache-Control'] = 'no-cache'

        # Open the video file in binary read mode
        with open(video_path, 'rb') as f:
            response.write(f.read())

        return response

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)





@api_view(['POST'])
def episodes_dummy(request):

    try:

        videos = Video.objects.all().order_by('?')
        n=0
        for episode in videos:
            if not episode.hls_created and n<10 and episode.videoDuration != "Text Based":
                st = threading.Thread(target=convert_to_hls, args=(episode,))
                st.start()
                n=n+1

        text_based_count=Video.objects.all().filter(videoDuration= "Text Based").count()
        created=Video.objects.all().filter(hls_created=True).count()
        all_count=Video.objects.all().count()
        return Response({"status": "OK",
                         "text_based_count":text_based_count,
                         "video_based_count":all_count-text_based_count,
                         "created":created,
                         "all_count":all_count,
                         }, status=status.HTTP_200_OK)

        return response

    except Exception as e:
        return Response({"status": "error", "object": str(e)}, status=status.HTTP_400_BAD_REQUEST)

