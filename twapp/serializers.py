from django.contrib.humanize.templatetags.humanize import naturaltime
from rest_framework import serializers
from twapp.models.models_main import Category, Learner, AppSession, Notification, Course, Comment, Tag, Lecture, Ticket, \
    Example, Thread, SubThread, Competition, Asset, Currency, Attend, Challenge, League, \
    SuperLeague, Achievement, Log, Content, Wallet, Result, Instructor, Submission, Video, Benefit, Run, Follow, \
    ThreadReply, WebinarEnrollment, Webinar, Subject, LeagueRecord, Project, Proposal, SkillTag, FiguredOption, Team, \
    TeamInvite, TeamRequest, LearnerSuperLeagueStatus, ContentComment, TicketMessage
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.utils import timezone
import re

UserModel = get_user_model()


# def updateLearnerSuperLeagueStatuses(learner, superleague):  # useless here
#     if LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=superleague).exists():
#         status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=superleague)[0]
#     else:
#         status_ = LearnerSuperLeagueStatus(learner=learner, superLeague=superleague)
#         status_.save()
#
#     teams = learner.learnerTeams.all()
#
#     try:
#     # if(1==1):
#         if timezone.now() < superleague.startTime:
#
#             # temporary set status to 3 (superLeague is open and Learner had no interact with the superLeague)
#             status_.status = 3
#             status_.save()
#
#             for team in teams:
#                 team_sl = team.teamSuperLeague.all()[0]
#                 if team_sl.superLeagueKey == superleague.superLeagueKey:
#                     if team.learners.count() < superleague.teamsCount and team.ownerKey == learner.learnerKey:
#                         # superLeague is open, Learner is the owner of team and team is not full
#                         status_.status = 1
#                         status_.team = team
#                         status_.save()
#                         return True
#                     elif team.learners.count() < superleague.teamsCount and team.ownerKey != learner.learnerKey:
#                         # superLeague is open, Learner is not the owner of team and team is not full
#                         status_.status = 2
#                         status_.team = team
#                         status_.save()
#                         return True
#                     elif team.learners.count() == superleague.teamsCount and team.ownerKey != learner.learnerKey:
#                         # superLeague is open, Learner is not the owner of team and team is full
#                         status_.status = 4
#                         status_.team = team
#                         status_.save()
#                         return True
#                     elif team.learners.count() == superleague.teamsCount and team.ownerKey == learner.learnerKey:
#                         # superLeague is open, Learner is the owner of team and team is full
#                         status_.status = 5
#                         status_.team = team
#                         status_.save()
#                         return True
#
#             teamsOfSuperLeague=superleague.teams.all()
#
#             for team in teamsOfSuperLeague:
#                 if TeamRequest.objects.all().filter(team=team, learner=learner).exists():
#                     # superLeague is open, Learner requested to a team and request not accepted/rejected
#                     status_.status = 6
#                     status_.save()
#                     return True
#                 if TeamInvite.objects.all().filter(team=team, learner=learner).exists():
#                     # superLeague is open, Learner invited to a team and invite not accepted/rejected
#                     status_.status = 10
#                     status_.save()
#                     return True
#
#             status_.status = 3
#             status_.save()
#             return True
#         elif timezone.now() > superleague.endTime:
#             for team in teams:
#                 team_sl = team.teamSuperLeague.all()[0]
#                 if team_sl.superLeagueKey == superleague.superLeagueKey:
#                     status_.status = 8
#                     status_.team = team
#                     status_.save()
#                     return True
#             status_.status = 9
#             status_.save()
#             return True
#         else:
#             for team in teams:
#                 team_sl = team.teamSuperLeague.all()[0]
#                 if team_sl.superLeagueKey == superleague.superLeagueKey:
#                     if team.ownerKey == learner.learnerKey:
#                         status_.status = 11
#                         status_.team = team
#                         status_.save()
#                         return True
#                     else:
#                         status_.status = 12
#                         status_.team = team
#                         status_.save()
#                         return True
#             return True
#         status_.status = 0
#         status_.save()
#         return True
#     except Exception as e:
#         print("---->" + str(e))
#         status_.status = -1
#         status_.save()
#         return True


class NotificationSerializer(serializers.ModelSerializer):
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    class Meta:
        model = Notification
        fields = ('notificationKey', 'title', 'text', 'read', 'time', 'type', 'icon', 'fuzzyTime')
        depth = 1

class CategorySerializer(serializers.ModelSerializer):

    parentingNumOfParent = serializers.SerializerMethodField(source='parentingNumOfParent')

    def get_parentingNumOfParent(self, obj):
        if obj.isLeaf:
            return obj.parent.parentingNum
        else:
            return -1


    class Meta:
        model = Category
        fields = ('categoryKey', 'categoryName', 'isLeaf', 'icon' , 'description', 'parentingNum', 'parentingNumOfParent')
        depth = 1

class RunSerializer(serializers.ModelSerializer):
    class Meta:
        model = Run
        fields = ('runKey', 'verdict', 'status', 'results')
        depth = 1

class BenefitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Benefit
        fields = ('name', 'lightIcon', 'darkIcon')
        depth = 1

class ExampleListSerializer(serializers.ModelSerializer):
    isPassed = serializers.SerializerMethodField(source='isPassed')

    def get_isPassed(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Log.objects.filter(action="questionPassed", learner=learner, example=obj).exists()

    class Meta:
        model = Example
        fields = ('exampleKey', 'title', 'type', 'isPassed')
        depth = 1



class EpisodeFullSerializer(serializers.ModelSerializer):

    #used in course detail service

    episodeType = serializers.SerializerMethodField(source='episodeType')
    episodeTitle = serializers.SerializerMethodField(source='episodeTitle')
    episodeKey = serializers.SerializerMethodField(source='episodeKey')
    questions = serializers.SerializerMethodField(source='questions')
    locked = serializers.SerializerMethodField(source='locked')
    score = serializers.SerializerMethodField(source='score')

    def get_episodeType(self, obj):
        if obj.videoDuration=="Text Based":
            return "textBased"
        return "videoBased"

    def get_episodeTitle(self, obj):
        return obj.videoTitle

    def get_episodeKey(self, obj):
        return obj.videoKey

    def get_locked(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return not Log.objects.filter(action="unlocked", learner=learner, video=obj).exists()

    # Fix
    def get_score(self, obj):
        return 12

    def get_questions(self, obj):
        qs= Example.objects.all().filter(relatedVideo=obj)
        s = ExampleListSerializer(qs, many=True, context={'learnerKey': self.context.get("learnerKey")})
        return s.data


    class Meta:
        model = Video
        fields = ('episodeKey','episodeTitle', 'episodeType','videoDuration','locked','questions','score')
        depth = 1


class EpisodeSearchSerializer(serializers.ModelSerializer):
    #used in course detail service
    episodeType = serializers.SerializerMethodField(source='episodeType')
    episodeTitle = serializers.SerializerMethodField(source='episodeTitle')
    episodeKey = serializers.SerializerMethodField(source='episodeKey')

    def get_episodeType(self, obj):
        if obj.videoDuration=="Text Based":
            return "textBased"
        return "videoBased"

    def get_episodeTitle(self, obj):
        return obj.videoTitle

    def get_episodeKey(self, obj):
        return obj.videoKey


    class Meta:
        model = Video
        fields = ('episodeKey','episodeTitle', 'episodeType')
        depth = 1

class WinnersSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='name')
    level = serializers.SerializerMethodField(source='level')

    def get_name(self, obj):
        return obj.learner.firstName + " " + obj.learner.lastName

    def get_level(self, obj):
        return obj.learner.level

    class Meta:
        model = Submission
        fields = ('name', 'level', 'score')
        depth = 1


class userChildSeralizer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('icon')
        depth = 1

class enrollementSeralizer(serializers.ModelSerializer):

    icon = serializers.SerializerMethodField(source='icon')

    def get_icon(self, obj):
        return obj.learner.profilePicture

    class Meta:
        model = WebinarEnrollment
        fields = ('icon','webinarEnrollmentKey')
        depth = 1


class InstructorListSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Instructor
        fields = ('instructorKey', 'firstName', 'lastName', 'bio', 'profilePicture')
        depth = 1


class InstructorShortListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instructor
        fields = ('instructorKey', 'firstName', 'lastName')
        depth = 1


class LearnerConnectionSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField(source='username')
    isFollower = serializers.SerializerMethodField(source='isFollower')
    isFollowing = serializers.SerializerMethodField(source='isFollowing')

    def get_username(self, obj):
        return obj.user.username

    def get_isFollower(self, obj):
        return Follow.objects.all().filter(followedKey=self.context.get("learnerKey"),followerKey=obj.learnerKey).exists()

    def get_isFollowing(self, obj):
        return Follow.objects.all().filter(followerKey=self.context.get("learnerKey"), followedKey=obj.learnerKey).exists()

    class Meta:
        model = Learner
        fields = (
        'profilePicture', 'learnerKey', 'isPrivate', 'level', 'firstName', 'lastName', 'username', 'isFollowing',
        'isFollower')
        depth = 1

class LearnerShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = Learner
        fields = ('profilePicture', 'firstName', 'lastName','level', 'settings_lang')
        depth = 1


class AchievementSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField(source='title')

    def get_title(self, obj):
        return obj.achievable.title

    class Meta:
        model = Achievement
        fields = ('icon', 'title', 'value')
        depth = 1


class ContentSerializer(serializers.ModelSerializer):
    isLiked = serializers.SerializerMethodField(source='isLiked')
    isMine = serializers.SerializerMethodField(source='isMine')
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')
    owner = LearnerConnectionSerializer(read_only=True, many=False)
    category = CategorySerializer(read_only=True, many=False)
    parentCategory = serializers.SerializerMethodField(source='parentCategory')

    def get_isLiked(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Log.objects.all().filter(content=obj, action="LIKE", learner=learner).exists()

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_isMine(self, obj):
        return obj.owner.learnerKey == self.context.get("learnerKey")

    class Meta:
        model = Content
        fields = (
        'contentKey', 'title', 'time','fuzzyTime', 'owner', 'isLiked', 'isMine', 'likeCount', 'contentType', 'contentFile',
        'category', 'parentCategory',)
        depth = 1


class SessionLessContentSerializer(serializers.ModelSerializer):
    # owner = LearnerConnectionSerializer(read_only=True, many=False)
    # category = CategorySerializer(read_only=True, many=False)
    # parentCategory = serializers.SerializerMethodField(source='parentCategory')
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')
    summary = serializers.SerializerMethodField(source='summary')


    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_summary(self, obj):
        try:
            return obj.summary[:200]+"..."
        except:
            CLEANR = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
            cleantext = re.sub(CLEANR, '', obj.description)
            return cleantext[:200]+"..."


    class Meta:
        model = Content
        fields = (
        'contentKey', 'title', 'time','fuzzyTime', 'contentFile','summary',
        'coverImage')
        depth = 1


class ReplySerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(source='votes')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    voteStatus = serializers.SerializerMethodField(source='voteStatus')

    def get_votes(self, obj):
        return obj.upVotes - obj.downVotes

    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_voteStatus(self,obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if Log.objects.all().filter(action="Vote_up",threadReply=obj,learner=learner).exists():
            return "up"
        if Log.objects.all().filter(action="Vote_down",threadReply=obj,learner=learner).exists():
            return "down"
        return "none"

    class Meta:
        model = ThreadReply
        fields = ('replyKey', 'body', 'time', 'learner', 'votes', 'views', 'threadFile', 'voteStatus', 'threadFile')
        depth = 1

class SubThreadSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(source='votes')
    replyCount = serializers.SerializerMethodField(source='replyCount')
    voteStatus = serializers.SerializerMethodField(source='voteStatus')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    replies = ReplySerializer(read_only=True, many=True)

    def get_votes(self, obj):
        return obj.upVotes - obj.downVotes

    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_replyCount(self, obj):
        return len(obj.replies.all())

    def get_voteStatus(self,obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if Log.objects.all().filter(action="Vote_up",subThread=obj,learner=learner).exists():
            return "up"
        if Log.objects.all().filter(action="Vote_down",subThread=obj,learner=learner).exists():
            return "down"
        return "none"

    class Meta:
        model = SubThread
        fields = ('subthreadKey', 'body', 'time', 'learner', 'votes', 'views','replyCount','voteStatus', 'threadFile', 'replies','threadFile')
        depth = 1

class ThreadSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(source='votes')
    isMine = serializers.SerializerMethodField(source='isMine')
    answersCount = serializers.SerializerMethodField(source='answersCount')
    voteStatus = serializers.SerializerMethodField(source='voteStatus')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    category = CategorySerializer(read_only=True, many=False)
    parentCategory = serializers.SerializerMethodField(source='parentCategory')
    subthreads = SubThreadSerializer(read_only=True, many=True)
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_votes(self, obj):
        return obj.upVotes - obj.downVotes

    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_answersCount(self, obj):
        return len(obj.subthreads.all())

    def get_voteStatus(self,obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if Log.objects.all().filter(action="Vote_up",thread=obj,learner=learner).exists():
            return "up"
        if Log.objects.all().filter(action="Vote_down",thread=obj,learner=learner).exists():
            return "down"
        return "none"

    def get_isMine(self, obj):
        return obj.learner.learnerKey == self.context.get("learnerKey")

    class Meta:
        model = Thread
        fields = (
        'threadKey', 'title', 'body', 'time', 'learner', 'votes', 'views', 'answersCount', 'threadFile', 'voteStatus',
        'category', 'parentCategory', 'subthreads','threadFile', 'isMine', 'fuzzyTime')
        depth = 1


class ThreadListSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(source='votes')
    answersCount = serializers.SerializerMethodField(source='answersCount')
    voteStatus = serializers.SerializerMethodField(source='voteStatus')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_votes(self, obj):
        return obj.upVotes - obj.downVotes

    def get_answersCount(self, obj):
        return len(obj.subthreads.all())

    def get_voteStatus(self,obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if Log.objects.all().filter(action="Vote_up",thread=obj,learner=learner).exists():
            return "up"
        if Log.objects.all().filter(action="Vote_down",thread=obj,learner=learner).exists():
            return "down"
        return "none"

    class Meta:
        model = Thread
        fields = (
        'threadKey', 'title', 'body', 'time', 'fuzzyTime', 'learner', 'votes', 'views', 'answersCount', 'voteStatus')
        depth = 1


class ThreadSearchSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(source='votes')
    answersCount = serializers.SerializerMethodField(source='answersCount')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_votes(self, obj):
        return obj.upVotes - obj.downVotes

    def get_answersCount(self, obj):
        return len(obj.subthreads.all())


    class Meta:
        model = Thread
        fields = (
        'threadKey', 'title', 'body', 'time', 'fuzzyTime', 'learner', 'votes', 'views', 'answersCount')
        depth = 1


class TagChildSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('tagKey', 'title')
        depth = 1


class SkillTagChildSeralizer(serializers.ModelSerializer):
    class Meta:
        model = SkillTag
        fields = ('skillTagKey', 'title')
        depth = 1


class ProjectListSerializer(serializers.ModelSerializer):
    tags = SkillTagChildSeralizer(read_only=True, many=True)

    bidCount = serializers.SerializerMethodField(source='bidCount')

    def get_bidCount(self, obj):
        return Proposal.objects.all().filter(project=obj).count()


    class Meta:
        model = Project
        fields = (
        'projectKey', 'title', 'description', 'creationTime', 'dueTime', 'status', 'minBudget', 'maxBudget', 'attachment','tags', 'bidCount')
        depth = 1


class ProjectSerializer(serializers.ModelSerializer):
    tags = SkillTagChildSeralizer(read_only=True, many=True)

    # dueTime = serializers.SerializerMethodField(source='dueTime')

    # def get_dueTime(self, obj):
    #     return int(obj.dueTime).timestamp() * 1000

    class Meta:
        model = Project
        fields = (
        'projectKey', 'title', 'description', 'creationTime', 'dueTime', 'status', 'minBudget', 'maxBudget', 'attachment', 'tags')
        depth = 1

class ProjectDetailSerializer(serializers.ModelSerializer):
    tags = SkillTagChildSeralizer(read_only=True, many=True)
    proposals = serializers.SerializerMethodField(source='proposals')

    def get_proposals(self, obj):
        all_proposals=Proposal.objects.all().filter(project=obj)
        s=ProposalSerializer(all_proposals, many=True)
        return s.data

    class Meta:
        model = Project
        fields = (
        'projectKey', 'title', 'description', 'creationTime', 'dueTime', 'status', 'minBudget', 'maxBudget', 'attachment', 'tags', 'proposals')
        depth = 1

class WebinarListSerializer(serializers.ModelSerializer):
    enrolledCount = serializers.SerializerMethodField(source='enrolledCount')
    enrolledList = serializers.SerializerMethodField(source='enrolledList')

    def get_enrolledList(self, obj):
        sub = WebinarEnrollment.objects.all().filter(webinar=obj).order_by('?')[:3]
        ret = []
        for s in sub:
            ret.append(s.learner.profilePicture.url)
        return ret

    def get_enrolledCount(self, obj):
        return len(WebinarEnrollment.objects.all().filter(webinar=obj))

    class Meta:
        model = Webinar
        fields = ('webinarKey', 'title', 'cover', 'description', 'presenters', 'startTime', 'endTime', 'enrolledCount',
                  'enrolledList', 'price', 'tax')
        depth = 1





class WebinarSerializer(serializers.ModelSerializer):
    enrolledCount = serializers.SerializerMethodField(source='enrolledCount')
    enrolledList = serializers.SerializerMethodField(source='enrolledList')
    presentersProfilePicture = serializers.SerializerMethodField(source='presentersProfilePicture')

    def get_enrolledList(self, obj):
        sub = WebinarEnrollment.objects.all().filter(webinar=obj).order_by('?')[:3]
        ret = []
        for s in sub:
            ret.append(s.learner.profilePicture.url)
        return ret

    def get_enrolledCount(self, obj):
        return len(WebinarEnrollment.objects.all().filter(webinar=obj))


    def get_presentersProfilePicture(self, obj):
        return None


    class Meta:
        model = Webinar
        fields = ('webinarKey', 'title', 'presentersProfilePicture', 'presenters', 'startTime', 'cover', 'endTime', 'enrolledCount','enrolledList')
        depth = 1


class ContentDetailSerializer(serializers.ModelSerializer):
    isLiked = serializers.SerializerMethodField(source='isLiked')
    isMine = serializers.SerializerMethodField(source='isMine')
    owner = LearnerConnectionSerializer(read_only=True, many=False)
    category = CategorySerializer(read_only=True, many=False)
    tags = TagChildSeralizer(read_only=True, many=True)
    parentCategory = serializers.SerializerMethodField(source='parentCategory')
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_isLiked(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Log.objects.all().filter(content=obj, action="LIKE", learner=learner).exists()

    def get_parentCategory(self, obj):
        s=CategorySerializer(obj.category.parent, many=False)
        return s.data

    def get_isMine(self, obj):
        return obj.owner.learnerKey == self.context.get("learnerKey")

    class Meta:
        model = Content
        fields = (
            'contentKey', 'parentCategory', 'fuzzyTime', 'contentLanguage',
            'title', 'time', 'description', 'owner', 'isLiked', 'isMine', 'likeCount',
            'contentType', 'contentFile', 'category', 'tags')
        depth = 1


class ContentCommentSerializer(serializers.ModelSerializer):
    isMine = serializers.SerializerMethodField(source='isMine')
    learner = LearnerConnectionSerializer(read_only=True, many=False)
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_isMine(self, obj):
        return obj.learner.learnerKey == self.context.get("learnerKey")

    class Meta:
        model = ContentComment
        fields = (
            'contentCommentKey', 'learner','fuzzyTime', 'time', 'message',  'isMine')
        depth = 1


class WalletSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ('walletKey', 'deposit')
        depth = 1


class LearnerSeralizer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField(source='username')

    def get_username(self, obj):
        return obj.user.username

    class Meta:
        model = Learner
        fields = ('profilePicture', 'firstName', 'lastName', 'level', 'username', 'isPrivate', 'learnerKey', 'settings_lang')
        depth = 1


class CurrencySeralizer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('currencyName', 'currencyKey', 'icon')
        depth = 1


class AssetSeralizer(serializers.ModelSerializer):
    currencyName = serializers.SerializerMethodField(source='currencyName')
    darkIcon = serializers.SerializerMethodField(source='darkIcon')
    lightIcon = serializers.SerializerMethodField(source='lightIcon')

    def get_currencyName(self, obj):
        return obj.currency.currencyName

    def get_darkIcon(self, obj):
        return obj.currency.darkIcon.url

    def get_lightIcon(self, obj):
        return obj.currency.lightIcon.url

    class Meta:
        model = Asset
        fields = ('currencyName', 'lightIcon', 'darkIcon', 'value')
        depth = 1


class LessonFullSerializer(serializers.ModelSerializer):
    episodes = serializers.SerializerMethodField(source='episodes')
    shortcut = serializers.SerializerMethodField(source='shortcut')
    isPassed = serializers.SerializerMethodField(source='isPassed')
    # lightIcon = serializers.SerializerMethodField(source='lightIcon')
    #
    def get_episodes(self, obj):
        eps = obj.episodes.all().order_by('-order').reverse()
        s = EpisodeFullSerializer(eps, many=True,  context={'learnerKey': self.context.get("learnerKey")})
        return s.data

    def get_shortcut(self, obj):
        if obj.shortcut != None:
            return True
        return False

    def get_isPassed(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Log.objects.all().filter(action="lessonPassed", learner=learner, lecture=obj).exists()


    # def get_lightIcon(self, obj):
    #     return obj.currency.lightIcon.url

    class Meta:
        model = Lecture
        fields = ('lectureKey','title', 'episodes', 'shortcut', 'isPassed')
        depth = 1



class CourseListSerializer(serializers.ModelSerializer):
    likeCount = serializers.SerializerMethodField(source='likeCount')
    commentCount = serializers.SerializerMethodField(source='commentCount')
    episodeCount = serializers.SerializerMethodField(source='episodeCount')
    hour = serializers.SerializerMethodField(source='hour')
    rate = serializers.SerializerMethodField(source='rate')
    summary = serializers.SerializerMethodField(source='summary')

    category = serializers.SerializerMethodField(source='category')
    instructor = serializers.SerializerMethodField(source='instructor')


    def get_category(self, obj):
        serializer = CategorySerializer(obj.category, many=False)
        return serializer.data

    def get_likeCount(self, obj):
        return len(obj.likers.all())

    def get_commentCount(self, obj):
        comments = Comment.objects.all().filter(course=obj)
        return len(comments)

    def get_episodeCount(self, obj):
        lectures=obj.courseLectures.all()
        res=0
        for l in lectures:
            res=res+l.episodes.count()
        return res

    def get_hour(self, obj):
        return 10

    def get_rate(self, obj):
        return 4.5

    def get_instructor(self, obj):
        s = InstructorListSeralizer(obj.instructor_set.all()[0], many=False)
        return s.data


    def get_summary(self, obj):
        try:
            return obj.description[:200]+"..."
        except:
            return ""

    class Meta:
        model = Course
        fields = (
            "courseKey", "title", "color", "likeCount", "commentCount", 'icon', 'category', 'episodeCount', 'hour',
            'price', 'summary', 'offPrice', 'rate', 'instructor')
        depth = 1


class CourseDetailSerializer(serializers.ModelSerializer):
    likeCount = serializers.SerializerMethodField(source='likeCount')
    commentCount = serializers.SerializerMethodField(source='commentCount')
    episodeCount = serializers.SerializerMethodField(source='episodeCount')
    lessonCount = serializers.SerializerMethodField(source='lessonCount')
    subscribed = serializers.SerializerMethodField(source='subscribed')

    hour = serializers.SerializerMethodField(source='hour')
    rate = serializers.SerializerMethodField(source='rate')

    category = serializers.SerializerMethodField(source='category')
    instructor = serializers.SerializerMethodField(source='instructor')
    lessons = serializers.SerializerMethodField(source='lessons')
    benefits = serializers.SerializerMethodField(source='benefits')

    isAttended = serializers.SerializerMethodField(source='isAttended')


    def get_category(self, obj):
        serializer = CategorySerializer(obj.category, many=False)
        return serializer.data

    def get_likeCount(self, obj):
        return len(obj.likers.all())

    def get_commentCount(self, obj):
        comments = Comment.objects.all().filter(course=obj)
        return len(comments)

    def get_episodeCount(self, obj):
        lectures=obj.courseLectures.all()
        res=0
        for l in lectures:
            res=res+l.episodes.count()
        return res

    def get_lessonCount(self, obj):
        res=obj.courseLectures.all().count()
        return res

    #Fix
    def get_hour(self, obj):
        return 10

    #Fix
    def get_rate(self, obj):
        return 4.5

    def get_isAttended(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Attend.objects.all().filter(learner=learner, course=obj).exists()

    def get_subscribed(self, obj):
        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return Attend.objects.all().filter(learner=learner, course=obj).exists()


    def get_instructor(self, obj):
        s = InstructorListSeralizer(obj.instructor_set.all()[0], many=False)
        return s.data

    def get_lessons(self, obj):
        lectures = obj.courseLectures.all().order_by('-order').reverse()
        s = LessonFullSerializer(lectures, many=True, context={'learnerKey': self.context.get("learnerKey")})
        return s.data

    def get_benefits(self, obj):
        s = BenefitSerializer(obj.benefits, many=True)
        return s.data

    class Meta:
        model = Course
        fields = (
            "courseKey", "title", "subscribed", "description", "color", "lessonCount", "likeCount", "commentCount",
            'icon', 'category','isAttended',
            'episodeCount', 'hour', 'price', 'benefits', 'cover',
            'offPrice', 'rate', 'instructor', 'lessons')
        depth = 1


class CourseChildSerializerInSubject(serializers.ModelSerializer):
    likeCount = serializers.SerializerMethodField(source='likeCount')
    commentCount = serializers.SerializerMethodField(source='commentCount')
    episodeCount = serializers.SerializerMethodField(source='episodeCount')
    lessonCount = serializers.SerializerMethodField(source='lessonCount')
    subscribed = serializers.SerializerMethodField(source='subscribed')

    hour = serializers.SerializerMethodField(source='hour')
    rate = serializers.SerializerMethodField(source='rate')

    category = serializers.SerializerMethodField(source='category')
    instructor = serializers.SerializerMethodField(source='instructor')


    def get_category(self, obj):
        serializer = CategorySerializer(obj.category, many=False)
        return serializer.data

    def get_likeCount(self, obj):
        return len(obj.likers.all())

    def get_commentCount(self, obj):
        comments = Comment.objects.all().filter(course=obj)
        return len(comments)

    def get_episodeCount(self, obj):
        lectures=obj.courseLectures.all()
        res=0
        for l in lectures:
            res=res+l.episodes.count()
        return res

    def get_lessonCount(self, obj):
        res=obj.courseLectures.all().count()
        return res

    #Fix
    def get_hour(self, obj):
        return 10

    #Fix
    def get_rate(self, obj):
        return 4.5

    #Fix
    def get_subscribed(self, obj):
        return False


    def get_instructor(self, obj):
        s = InstructorListSeralizer(obj.instructor_set.all()[0], many=False)
        return s.data



    class Meta:
        model = Course
        fields = (
            "courseKey", "title", "subscribed", "description", "color", "lessonCount", "likeCount", "commentCount",
            'icon', 'category', 'episodeCount', 'hour', 'price', 'cover', 'offPrice', 'rate', 'instructor')
        depth = 1



class CourseChildSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('courseKey', 'title', 'icon')


class RankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('currentRank', 'rankChangedCount', 'rankStatus')


# class CategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('categoryKey', 'categoryName', 'icon')


class AttendSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(source='category')
    lessonCount = serializers.SerializerMethodField(source='lessonCount')
    title = serializers.SerializerMethodField(source='title')
    courseKey = serializers.SerializerMethodField(source='courseKey')
    icon = serializers.SerializerMethodField(source='icon')
    percentage = serializers.SerializerMethodField(source='percentage')
    episodeCount = serializers.SerializerMethodField(source='episodeCount')
    hour = serializers.SerializerMethodField(source='hour')
    rate = serializers.SerializerMethodField(source='rate')
    offPrice = serializers.SerializerMethodField(source='offPrice')
    price = serializers.SerializerMethodField(source='price')

    def get_category(self, obj):
        serializer = CategorySerializer(obj.course.category, many=False)
        return serializer.data

    def get_courseKey(self, obj):
        return obj.course.courseKey

    def get_title(self, obj):
        return obj.course.title

    def get_offPrice(self, obj):
        return obj.course.offPrice

    def get_price(self, obj):
        return obj.course.price


    def get_icon(self, obj):
        return obj.course.icon.url

    def get_lessonCount(self, obj):
        return len(obj.course.courseLectures.all())

    def get_episodeCount(self, obj):
        lectures=obj.course.courseLectures.all()
        res=0
        for l in lectures:
            res=res+l.episodes.count()
        return res

    def get_percentage(self, obj):
        if obj.course.courseLectures.all().count() == 0:
            return 0
        else:
            return int(obj.lessonCompleted / len(obj.course.courseLectures.all()) * 100)

    def get_hour(self, obj):
        return 10

    def get_rate(self, obj):
        return 4.5

    class Meta:
        model = Attend
        fields = (
        'courseKey', 'title', 'icon', 'category', 'lessonCompleted', 'lessonCount', 'percentage', 'episodeCount',
        'hour', 'price', 'offPrice','rate')
        depth = 1


# class CourseListSerializer(serializers.ModelSerializer):
#     lessonCount = serializers.SerializerMethodField(source='lessonCount')
#     category = serializers.SerializerMethodField(source='category')
#
#     def get_lessonCount(self, obj):
#         return len(obj.lecture_set.all())
#
#     def get_category(self, obj):
#         serializer = CategorySerializer(obj.category, many=False)
#         return serializer.data
#
#     class Meta:
#         model = Course
#         fields = ('courseKey', 'title', 'icon', 'category',  'lessonCount')


class CourseShortSeralizer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField(source='title')
    courseKey = serializers.SerializerMethodField(source='courseKey')

    def get_title(self, obj):
        return obj.course.title

    def get_courseKey(self, obj):
        return obj.course.courseKey

    class Meta:
        model = Attend
        fields = ('courseKey', 'title')
        depth = 1


class ChallengeDetailSerializer(serializers.ModelSerializer):
    tags = TagChildSeralizer(read_only=True, many=True)
    company = serializers.SerializerMethodField(source='company')
    submissionCount = serializers.SerializerMethodField(source='submissionCount')
    submitterProfiles = serializers.SerializerMethodField(source='submitterProfiles')
    formattedTime = serializers.SerializerMethodField(source='formattedTime')
    countingTime = serializers.SerializerMethodField(source='countingTime')
    winners = serializers.SerializerMethodField(source='winners')

    def get_company(self, obj):
        return {
            "name": obj.company,
            "icon": obj.icon.url
        }

    def get_submissionCount(self, obj):
        return Submission.objects.all().filter(challenge=obj).count()

    def get_submitterProfiles(self, obj):
        sub = Submission.objects.all().filter(challenge=obj).order_by('?')[:3]
        ret = []
        for s in sub:
            ret.append(s.learner.profilePicture.url)
        return ret

    def get_winners(self, obj):
        sub = Submission.objects.all().filter(challenge=obj).order_by('score')
        s = WinnersSerializer(sub, many=True)
        return s.data

    def get_countingTime(self, obj):
        dft=obj.startTime-timezone.now()

        if timezone.now() < obj.endTime:
            dft = obj.endTime - timezone.now()

        final_str=str(dft)
        final_str=final_str.split('.')[0]
        return final_str

    def get_formattedTime(self, obj):
        if timezone.now() < obj.startTime:
            return  obj.startTime.strftime('%b %d, %Y %H:%M:%S')
        return  obj.endTime.strftime('%b %d, %Y %H:%M:%S')


    class Meta:
        model = Challenge
        fields = (
        'challengeKey', 'statement', 'status', 'title', 'cover',
        'icon', 'gift', 'subtitle', 'endTime', 'tags', 'company', 'countingTime',
        'formattedTime','submissionCount', 'submitterProfiles','winners')
        depth = 1


class ChallengeSerializer(serializers.ModelSerializer):
    tags = TagChildSeralizer(read_only=True, many=True)
    leftTime = serializers.SerializerMethodField(source='leftTime')
    point = serializers.SerializerMethodField(source='point')
    status = serializers.SerializerMethodField(source='status')
    countingTime = serializers.SerializerMethodField(source='countingTime')
    formattedTime = serializers.SerializerMethodField(source='formattedTime')
    submissionCount = serializers.SerializerMethodField(source='submissionCount')
    submitterProfiles = serializers.SerializerMethodField(source='submitterProfiles')


    def get_status(self, obj):

        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]

        if timezone.now() > obj.endTime:
            return 4

        if timezone.now() < obj.startTime:
            return 1

        if Submission.objects.all().filter(learner=learner, challenge=obj).exists():
            return 3

        return 2

    def get_countingTime(self, obj):
        dft=obj.startTime-timezone.now()

        if timezone.now() < obj.endTime:
            dft = obj.endTime - timezone.now()

        final_str=str(dft)
        final_str=final_str.split('.')[0]
        return final_str

    def get_submissionCount(self, obj):
        return Submission.objects.all().filter(challenge=obj).count()

    def get_leftTime(self, obj):
        return obj.endTime - obj.startTime

    def get_point(self, obj):
        return 0

    def get_submitterProfiles(self, obj):
        sub = Submission.objects.all().filter(challenge=obj).order_by('?')[:3]
        ret = []
        for s in sub:
            ret.append(s.learner.profilePicture.url)
        return ret

    def get_formattedTime(self, obj):
        if timezone.now() < obj.startTime:
            return  obj.startTime.strftime('%b %d, %Y %H:%M:%S')
        return  obj.endTime.strftime('%b %d, %Y %H:%M:%S')


    class Meta:
        model = Challenge
        fields = (
            'challengeKey', 'title', "subtitle", 'gift', 'icon', "tags", "company", "countingTime", "formattedTime",
            "leftTime", "endTime", "startTime", "submissionCount", "submitterProfiles", "cover",
            "status", "point")
        depth = 1


class LeagueSeralizer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='name')
    courseKey = serializers.SerializerMethodField(source='courseKey')
    status = serializers.SerializerMethodField(source='status')
    icon = serializers.SerializerMethodField(source='icon')
    rank = serializers.SerializerMethodField(source='rank')
    timeInterval = serializers.SerializerMethodField(source='timeInterval')
    countingTime = serializers.SerializerMethodField(source='countingTime')

    def get_name(self, obj):
        return obj.course.title

    def get_courseKey(self, obj):
        return obj.course.courseKey

    def get_icon(self, obj):
        return obj.course.icon.url

    def get_timeInterval(self, obj):
        return obj.startTime.strftime('%b %d')+" to "+obj.endTime.strftime('%b %d')

    def get_countingTime(self, obj):
        dft=obj.startTime-timezone.now()

        if timezone.now() < obj.endTime:
            dft = obj.endTime - timezone.now()

        final_str=str(dft)
        final_str=final_str.split('.')[0]
        return final_str

    def get_status(self, obj):
        # 0 learner not allowed for this league.
        # 1 learner allowed in this league. Not registered yet.
        # 2 learner registered in the league.
        # 3 league finished and learner did not participate.
        # 4 league finished and learner participated.

        learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]

        if timezone.now() > obj.endTime:
            if LeagueRecord.objects.all().filter(learner=learner,league=obj).exists():
                return 4
            return 3

        if learner.level<obj.level:
            return 0

        if LeagueRecord.objects.all().filter(learner=learner, league=obj).exists():
            return 2
        return 1

    def get_rank(self, obj):
        # learner=Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        #
        # rank_=LeagueRecord.objects.all().filter(learner=learner,league=obj)[0].rank
        return {"currentRank": 1,
                "rankChangedCount": 63,
                "rankStatus": 0
                }

    class Meta:
        model = League
        fields = ('name', 'leagueKey', 'courseKey' , 'level', 'rank', 'hint', 'status', 'timeInterval', 'countingTime','startTime', 'endTime', 'status', 'icon',
                  'nextLeagueTime')
        depth = 1

class LeagueRecordSerializer(serializers.ModelSerializer):
    learnerName = serializers.SerializerMethodField(source='learnerName')
    learnerKey = serializers.SerializerMethodField(source='learnerKey')
    profilePicture = serializers.SerializerMethodField(source='profilePicture')
    level = serializers.SerializerMethodField(source='level')

    def get_learnerName(self, obj):
        return obj.learner.firstName+" "+obj.learner.lastName

    def get_learnerKey(self, obj):
        return obj.learner.learnerKey

    def get_profilePicture(self, obj):
        return obj.learner.profilePicture.url

    def get_level(self, obj):
        return obj.learner.level

    class Meta:
        model = LeagueRecord
        fields = ('learnerName', 'learnerKey', 'profilePicture', 'score', 'rank','level')
        depth = 1


class TeamSerializer(serializers.ModelSerializer):
    members = serializers.SerializerMethodField(source='members')
    isOwned = serializers.SerializerMethodField(source='isOwned')
    isInvited = serializers.SerializerMethodField(source='isInvited')
    isRequested = serializers.SerializerMethodField(source='isRequested')
    isMember = serializers.SerializerMethodField(source='isMember')
    isClosed = serializers.SerializerMethodField(source='isClosed')
    teamInviteKey = serializers.SerializerMethodField(source='teamInviteKey')
    teamRequestKey = serializers.SerializerMethodField(source='teamRequestKey')

    def get_members(self, obj):
        s = LearnerSeralizer(obj.learners.all(), many=True)
        return s.data

    def get_isOwned(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return obj.ownerKey == learner.learnerKey

    def get_isInvited(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return TeamInvite.objects.filter(status="new", learner=learner, team=obj).exists()

    def get_isRequested(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return TeamRequest.objects.filter(status="new", learner=learner, team=obj).exists()

    def get_teamInviteKey(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if TeamInvite.objects.filter(status="new", learner=learner, team=obj).exists():
            return TeamInvite.objects.filter(status="new", learner=learner, team=obj)[0].teamInviteKey
        return "none"

    def get_teamRequestKey(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        if TeamRequest.objects.filter(status="new", learner=learner, team=obj).exists():
            return TeamRequest.objects.filter(status="new", learner=learner, team=obj)[0].teamRequestKey
        return "none"

    def get_isMember(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return learner in obj.learners.all()

    def get_isClosed(self, obj):
        super_league = obj.teamSuperLeague.all()[0]
        if obj.learners.all().count == super_league.teamsCount:
            return True
        else:
            return False

    class Meta:
        model = Team
        fields = (
            'name', 'teamKey', 'members', 'teamLogo', 'isMember', 'isOwned', 'isInvited', 'isRequested', 'isClosed',
            'teamInviteKey', 'teamRequestKey')
        depth = 1


class SuperLeagueSeralizer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='name')
    team = serializers.SerializerMethodField(source='team')
    learnerTeam = serializers.SerializerMethodField(source='learnerTeam')
    teamKey = serializers.SerializerMethodField(source='teamKey')
    status = serializers.SerializerMethodField(source='status')
    icon = serializers.SerializerMethodField(source='icon')
    rank = serializers.SerializerMethodField(source='rank')
    points = serializers.SerializerMethodField(source='points')
    isStarted = serializers.SerializerMethodField(source='isStarted')

    timeLeft = serializers.SerializerMethodField(source='timeLeft')
    countingTime = serializers.SerializerMethodField(source='countingTime')
    formattedTime = serializers.SerializerMethodField(source='formattedTime')


    def get_countingTime(self, obj):
        dft=obj.startTime-timezone.now()

        if timezone.now() < obj.endTime:
            dft = obj.startTime - timezone.now()

        final_str=str(dft)
        final_str=final_str.split('.')[0]
        return final_str

    def get_isStarted(self, obj):
        if timezone.now() > obj.startTime and timezone.now() < obj.endTime:
            return True
        return False

    def get_status(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        try:
            status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=obj)[0]
            return status_.status
        except:
            return 3

    def get_name(self, obj):
        return obj.title

    def get_timeLeft(self, obj):
        return obj.endTime - obj.startTime

    def get_team(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        try:
            status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=obj)[0]
            s = LearnerSeralizer(status_.team.learners.all(), many=True)
            return s.data
        except:
            return []

    def get_learnerTeam(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        try:
            status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=obj)[0]
            s = TeamSerializer(status_.team, many=False, context={'learnerKey': learner.learnerKey})
            return s.data
        except:
            return []

    def get_teamKey(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        try:
            status_ = LearnerSuperLeagueStatus.objects.all().filter(learner=learner, superLeague=obj)[0]
            return status_.team.teamKey
        except:
            return ""

    def get_icon(self, obj):
        return obj.icon.url

    #fix
    def get_points(self, obj):
        return 22


    def get_formattedTime(self, obj):
        if timezone.now() < obj.startTime:
            return  obj.startTime.strftime('%b %d, %Y %H:%M:%S')
        return  obj.endTime.strftime('%b %d, %Y %H:%M:%S')


    def get_rank(self, obj):
        try:
            res = Result.objects.all().filter(superLeague=obj)[0]
            s = RankSerializer(res, many=False)
            return s.data
        except:
            return {"currentRank": 45, "rankChangedCount": 65, "rankStatus": 1}

    class Meta:
        model = SuperLeague
        fields = (
            'name', 'superLeagueKey', 'isStarted', 'level', "rank",
            "formattedTime", "countingTime", "message", "status", "startTime", "learnerTeam",
            "endTime", "team","teamKey", 'icon','points',"timeLeft")
        depth = 1



class TeamListShortSerializer(serializers.ModelSerializer):
    teamPoint = serializers.SerializerMethodField(source='teamPoint')

    def get_teamPoint(self, obj):
        try:
            res = Result.objects.all().filter(team=obj)[0]
            return res.score
        except:
            return 0

    class Meta:
        model = Team
        fields = ( 'name', 'teamKey', 'teamLogo', 'teamSuperLeague', 'teamPoint')
        depth = 1

class InviteSerializer(serializers.ModelSerializer):
    learner = serializers.SerializerMethodField(source='learner')

    def get_learner(self, obj):
        s = LearnerSeralizer(obj.learner, many=False)
        return s.data

    class Meta:
        model = TeamInvite
        fields = ('teamInviteKey', 'status', 'learner')
        depth = 1

class RequestSerializer(serializers.ModelSerializer):
    learner = serializers.SerializerMethodField(source='learner')

    def get_learner(self, obj):
        s = LearnerSeralizer(obj.learner, many=False)
        return s.data

    class Meta:
        model = TeamRequest
        fields = ('teamRequestKey', 'status', 'learner')
        depth = 1


class CompetitionSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField(source='title')
    opponent = serializers.SerializerMethodField(source='opponent')
    status = serializers.SerializerMethodField(source='status')
    user = serializers.SerializerMethodField(source='user')

    def get_title(self, obj):
        return obj.course.title

    # def get_status(self, obj):
    #     return 1

    def get_opponent(self, obj):
        if obj.opponentKey=="":
            return {}
        competitor = Learner.objects.all().filter(learnerKey=obj.opponentKey)[0]
        s = LearnerSeralizer(competitor, many=False)
        return s.data

    def get_user(self, obj):
        s = LearnerSeralizer(obj.learner, many=False)
        return s.data

    def get_status(self, obj):
        learnerKey = self.context.get("learnerKey")


        if obj.status == -1:
            return 1

        if obj.status == 2:
            if learnerKey == obj.learner.learnerKey:
                return 4

        if obj.questions.all().count()>0:
            if obj.opponentLastQuestionKey == obj.questions.last().exampleKey and obj.userLastQuestionKey == obj.questions.last().exampleKey:
                return 8
            if obj.status==1 and learnerKey == obj.opponentKey and obj.opponentLastQuestionKey==obj.questions.last().exampleKey:
                return 5
            if obj.status==1 and learnerKey == obj.learner.learnerKey and obj.userLastQuestionKey==obj.questions.last().exampleKey:
                return 5
            if obj.status==1 and learnerKey == obj.opponentKey and obj.userLastQuestionKey==obj.questions.last().exampleKey:
                return 6
            if obj.status==1 and learnerKey == obj.learner.learnerKey and obj.opponentLastQuestionKey==obj.questions.last().exampleKey:
                return 6

        if learnerKey == obj.opponentKey:
            if obj.status == 0:
                return 3
            else:
                return 7
        else:
            if obj.status == 0:
                return 2
            if obj.status == 2:
                return 4
            else:
                return 7

    class Meta:
        model = Competition
        fields = ('title', 'opponent', 'user', 'status', 'userPoint', 'opponentPoint', 'competitionKey')
        depth = 1


class TicketSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField(source='status')
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')
    unreadCount = serializers.SerializerMethodField(source='unreadCount')
    formatedTime = serializers.SerializerMethodField(source='formatedTime')

    def get_unreadCount(self, obj):
        return TicketMessage.objects.all().filter(ticket=obj,status="unread").count()

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_formatedTime(self, obj):
        return obj.time.strftime('%Y/%m/%d %H:%M')

    def get_status(self, obj):
        if obj.status == "new":
            return 0
        else:
            return 1

    class Meta:
        model = Ticket
        fields = (
        'ticketKey', 'title', 'message', 'answer', 'time', 'unreadCount', 'fuzzyTime', 'formatedTime', 'status',
        'attachment')
        depth = 1


class TicketMessageSerializer(serializers.ModelSerializer):
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')
    formatedTime = serializers.SerializerMethodField(source='formatedTime')

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_formatedTime(self, obj):
        return obj.time.strftime('%Y/%m/%d %H:%M')


    class Meta:
        model = TicketMessage
        fields = (
        'ticketMessageKey', 'message', 'time', 'fuzzyTime', 'formatedTime', 'status', 'side',
        'attachment')
        depth = 1

class TicketDetailSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField(source='status')
    fuzzyTime = serializers.SerializerMethodField(source='fuzzyTime')
    unreadCount = serializers.SerializerMethodField(source='unreadCount')
    formatedTime = serializers.SerializerMethodField(source='formatedTime')
    messages = serializers.SerializerMethodField(source='messages')
    learner = serializers.SerializerMethodField(source='learner')

    def get_learner(self, obj):
        s = LearnerSeralizer(obj.learner, many=False)
        return s.data

    def get_unreadCount(self, obj):
        return TicketMessage.objects.all().filter(ticket=obj,status="unread").count()

    def get_fuzzyTime(self, obj):
        return naturaltime(obj.time)

    def get_formatedTime(self, obj):
        return obj.time.strftime('%Y/%m/%d %H:%M')

    def get_messages(self, obj):
        msgs = TicketMessage.objects.all().filter(ticket=obj).order_by("-time").reverse()
        serializer=TicketMessageSerializer(msgs,many=True)
        return serializer.data


    def get_status(self, obj):
        if obj.status == "new":
            return 0
        else:
            return 1

    class Meta:
        model = Ticket
        fields = (
        'ticketKey', 'title', 'message', 'answer', 'time', 'unreadCount', 'fuzzyTime', 'formatedTime', 'status',
        'attachment', 'messages', 'learner')
        depth = 1

class ProposalSerializer(serializers.ModelSerializer):
    learnerName = serializers.SerializerMethodField(source='learnerName')
    learnerKey = serializers.SerializerMethodField(source='learnerKey')
    profilePicture = serializers.SerializerMethodField(source='profilePicture')
    level = serializers.SerializerMethodField(source='level')
    projectKey = serializers.SerializerMethodField(source='projectKey')

    def get_learnerName(self, obj):
        return obj.learner.firstName+" "+obj.learner.lastName

    def get_learnerKey(self, obj):
        return obj.learner.learnerKey

    def get_profilePicture(self, obj):
        return obj.learner.profilePicture.url

    def get_level(self, obj):
        return obj.learner.level

    def get_projectKey(self, obj):
        return obj.project.projectKey

    class Meta:
        model = Proposal
        fields = ('proposalKey', 'bid', 'description', 'status' , 'projectKey','duration', 'learnerName', 'learnerKey', 'profilePicture','level')
        depth = 1



class SessionSeralizer(serializers.ModelSerializer):
    learnerKey = serializers.SerializerMethodField(source='get_learnerKey')
    learner = serializers.SerializerMethodField(source='learner')

    def get_learnerKey(self, obj):
        return obj.user.learner.learnerKey

    def get_learner(self, obj):
        s = LearnerShortSerializer(obj.user.learner, many=False)
        return s.data

    class Meta:
        model = AppSession
        fields = ('token', 'isActive','learnerKey', 'learner')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    # def validate_email(self, value):
    #     if User.objects.filter(email=value).exists():
    #         raise serializers.ValidationError('already exists')
    #     return value

    def create(self, validated_data):
        user = UserModel.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            email=validated_data['email']
        )

        return user

    class Meta:
        model = UserModel
        # Tuple of serialized model fields (see link [2])
        fields = ("username", "password", "email")


class EpisodeDetailSerializer(serializers.ModelSerializer):
    episodeType = serializers.SerializerMethodField(source='episodeType')
    episodeTitle = serializers.SerializerMethodField(source='episodeTitle')
    episodeText = serializers.SerializerMethodField(source='episodeText')
    episodeKey = serializers.SerializerMethodField(source='episodeKey')
    questions = serializers.SerializerMethodField(source='questions')
    instructor = serializers.SerializerMethodField(source='instructor')
    parentCourse = serializers.SerializerMethodField(source='parentCourse')
    currentLearner = serializers.SerializerMethodField(source='currentLearner')

    def get_episodeType(self, obj):
        if obj.videoDuration=="Text Based":
            return "textBased"
        return "videoBased"

    def get_episodeTitle(self, obj):
        return obj.videoTitle

    def get_episodeText(self, obj):
        return obj.videoDescription

    def get_episodeKey(self, obj):
        return obj.videoKey

    def get_locked(self, obj):
        return obj.isFree

    def get_instructor(self, obj):
        s = InstructorListSeralizer(obj.videoOwner.instructor, many=False)
        return s.data

    def get_questions(self, obj):
        qs= Example.objects.all().filter(relatedVideo=obj)
        s = ExampleListSerializer(qs, many=True, context={'learnerKey': self.context.get("learnerKey")})
        return s.data

    def get_parentCourse(self, obj):
        lecture = obj.lecture_set.all()[0]
        course = lecture.parentCourse
        return course.courseKey

    def get_currentLearner(self, obj):
        learner = Learner.objects.all().filter(learnerKey=self.context.get("learnerKey"))[0]
        return learner.firstName+" "+learner.lastName

    class Meta:
        model = Video
        fields = (
        'episodeKey', 'episodeTitle', 'episodeText', 'likeCount', 'episodeType', 'videoDuration', 'questions', 'creationTime',
        'instructor', 'videoFile', 'parentCourse', 'hls_url','currentLearner')
        depth = 1


class ExampleDetailSerializer(serializers.ModelSerializer):
    # Usage: Question detail service

    options = serializers.SerializerMethodField(source='options')
    passed = serializers.SerializerMethodField(source='passed')
    courseKey = serializers.SerializerMethodField(source='courseKey')
    blankFormat = serializers.SerializerMethodField(source='blankFormat')


    def get_options(self, obj):
        if obj.type=="multi" or obj.type=="pick":
            splitted=obj.options.split("\n")
            options=[o.replace('\r', '').strip() for o in splitted]
            return options
        return []

    #Fix
    def get_passed(self, obj):
        return False

    def get_courseKey(self, obj):
        return obj.lecture_set.all()[0].parentCourse.courseKey

    def get_blankFormat(self, obj):
        return obj.blank()

    class Meta:
        model = Example
        fields = (
            'exampleKey', 'title', 'type', 'subType', 'statement','blankFormat','options', 'hint', 'initCode', 'score', 'passed',
            'explanatoryAnswer', 'nextKey', 'previousKey', 'index', 'siblingQuestions','courseKey')
        depth = 1


class ComptitionQuestionSerializer(serializers.ModelSerializer):
    # Usage: Question detail service

    options = serializers.SerializerMethodField(source='options')
    passed = serializers.SerializerMethodField(source='passed')
    courseKey = serializers.SerializerMethodField(source='courseKey')
    blankFormat = serializers.SerializerMethodField(source='blankFormat')


    def get_options(self, obj):
        if obj.type=="multi" or obj.type=="pick":
            splitted=obj.options.split("\n")
            options=[o.replace('\r', '').strip() for o in splitted]
            return options
        return []

    #Fix
    def get_passed(self, obj):
        return False

    def get_courseKey(self, obj):
        return obj.lecture_set.all()[0].parentCourse.courseKey

    def get_blankFormat(self, obj):
        return obj.blank()

    class Meta:
        model = Example
        fields = (
            'exampleKey', 'title', 'type', 'subType', 'statement','blankFormat','options', 'hint', 'initCode', 'score', 'passed',
            'explanatoryAnswer', 'siblingQuestions','courseKey')
        depth = 1


class ExampleItemBasedDetailSerializer(serializers.ModelSerializer):
    # Usage: Question detail service
    passed = serializers.SerializerMethodField(source='passed')

    #Fix
    def get_passed(self, obj):
        return False

    class Meta:
        model = Example
        fields = (
            'exampleKey', 'title', 'type', 'subType', 'score', 'passed')
        depth = 1


class SubjectListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subject
        fields = ('subjectKey', 'title', 'cover', 'icon', 'description' , 'lessonsCount', 'episodeCount',  'hours')
        depth = 1

class SubjectSerializer(serializers.ModelSerializer):
    courses = CourseChildSerializerInSubject(read_only=True, many=True)

    class Meta:
        model = Subject
        fields = (
        'subjectKey', 'title', 'cover', 'icon', 'description', 'lessonsCount', 'episodeCount', 'hours', 'endTime',
        'courses', 'price', 'actualPrice', 'tax')
        depth = 1

class FiguredOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = FiguredOption
        fields = (
            'figuredOptionKey', 'figure')
        depth = 1

class CompetitionDetailSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField(source='title')
    opponent = serializers.SerializerMethodField(source='opponent')
    status = serializers.SerializerMethodField(source='status')
    user = serializers.SerializerMethodField(source='user')
    lastQuestion = serializers.SerializerMethodField(source='lastQuestion')
    questions = ComptitionQuestionSerializer(read_only=True, many=True)

    def get_lastQuestion(self, obj):
        learner_=Learner.objects.all().filter(learnerKey=obj.opponentKey)[0]
        if obj.learner == learner_:
            return obj.userLastQuestionKey
        return obj.opponentLastQuestionKey

    def get_title(self, obj):
        return obj.course.title

    # def get_status(self, obj):
    #     return 1

    def get_opponent(self, obj):
        if obj.opponentKey=="":
            return {}
        competitor = Learner.objects.all().filter(learnerKey=obj.opponentKey)[0]
        s = LearnerSeralizer(competitor, many=False)
        return s.data

    def get_user(self, obj):
        s = LearnerSeralizer(obj.learner, many=False)
        return s.data

    def get_status(self, obj):
        learnerKey = self.context.get("learnerKey")


        if obj.status == -1:
            return 1

        if obj.status == 2:
            if learnerKey == obj.learner.learnerKey:
                return 4

        if obj.questions.all().count()>0:
            if obj.opponentLastQuestionKey == obj.questions.last().exampleKey and obj.userLastQuestionKey == obj.questions.last().exampleKey:
                return 8
            if obj.status==1 and learnerKey == obj.opponentKey and obj.opponentLastQuestionKey==obj.questions.last().exampleKey:
                return 5
            if obj.status==1 and learnerKey == obj.learner.learnerKey and obj.userLastQuestionKey==obj.questions.last().exampleKey:
                return 5
            if obj.status==1 and learnerKey == obj.opponentKey and obj.userLastQuestionKey==obj.questions.last().exampleKey:
                return 6
            if obj.status==1 and learnerKey == obj.learner.learnerKey and obj.opponentLastQuestionKey==obj.questions.last().exampleKey:
                return 6

        if learnerKey == obj.opponentKey:
            if obj.status == 0:
                return 3
            else:
                return 7
        else:
            if obj.status == 0:
                return 2
            if obj.status == 2:
                return 4
            else:
                return 7

    class Meta:
        model = Competition
        fields = ('title', 'opponent', 'user', 'status', 'questions', 'userPoint', 'opponentPoint', 'competitionKey','lastQuestion')
        depth = 1

