from modeltranslation.translator import translator, TranslationOptions

from twapp.models import Tag, Category, Example, Lecture, Course, PostCategory, Post, Video, Challenge, Webinar, Subject, \
    Achievable


class PostCategoryTranslationOptions(TranslationOptions):
    fields = ('categoryName',)

class PostTranslationOptions(TranslationOptions):
    fields = ('title','duration','body')

class CategoryTranslationOptions(TranslationOptions):
    fields = ('categoryName',)

class TagTranslationOptions(TranslationOptions):
     fields = ('title',)

class ExampleTranslationOptions(TranslationOptions):
    # fields = ('title','statement','before','after','options','answer','hint',)
    fields = ('title','statement','before','after','options','answer','hint','explanatoryAnswer')

class LectureTranslationOptions(TranslationOptions):
    fields = ('title','lectureNote')

class CourseTranslationOptions(TranslationOptions):
    fields = ('title','description')

class VideoTranslationOptions(TranslationOptions):
    fields = ('videoTitle','videoDescription')

class ChallengeTranslationOptions(TranslationOptions):
    fields = ('title','subtitle','statement' , 'company')

class WebinarTranslationOptions(TranslationOptions):
    fields = ('title','description')

class SubjectTranslationOptions(TranslationOptions):
    fields = ('title','description')

class AchievableTranslationOptions(TranslationOptions):
    fields = ('title',)

translator.register(Category, CategoryTranslationOptions)
translator.register(Tag, TagTranslationOptions)
translator.register(Example, ExampleTranslationOptions)
translator.register(Lecture, LectureTranslationOptions)
translator.register(Course, CourseTranslationOptions)
translator.register(PostCategory, PostCategoryTranslationOptions)
translator.register(Post, PostTranslationOptions)
translator.register(Video, VideoTranslationOptions)
translator.register(Challenge, ChallengeTranslationOptions)
translator.register(Webinar, WebinarTranslationOptions)
translator.register(Subject, SubjectTranslationOptions)
translator.register(Achievable, AchievableTranslationOptions)