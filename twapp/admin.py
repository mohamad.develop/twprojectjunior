from django.contrib import admin
from twapp.models import AppSession, Learner, Config, Category, OTP, Notification, Instructor, Course, Lecture, \
    Comment, Tag, Ticket, Example, Thread, SubThread, CourseXp, Log, Post, PostCategory, Competition, Run, TestCase, \
    Video, Asset, Currency, Attend, Challenge, League, SuperLeague, Webinar, Subject, Shortcut, TeamRequest, Team, \
    Project, Proposal, SkillTag, Achievement, Achievable, Content, Follow, Wallet, Result, Submission, Rule, \
    NotificationRule, Benefit, ThreadReply, WebinarEnrollment, LeagueRecord, FiguredOption, TeamInvite, \
    LearnerSuperLeagueStatus, ContentComment, TicketMessage, PromoCode


class ExampleAdmin(admin.ModelAdmin):
    search_fields = ['title', 'before' ]

class CourseAdmin(admin.ModelAdmin):
    search_fields = ['title', 'description' ]

class VideoAdmin(admin.ModelAdmin):
    search_fields = ['videoTitle', 'videoKey' ]

class LearnerAdmin(admin.ModelAdmin):
    search_fields = ['firstName', 'lastName' ]

admin.site.register(Video,VideoAdmin)
admin.site.register(AppSession)
admin.site.register(Learner,LearnerAdmin)
admin.site.register(Config)
admin.site.register(Category)
admin.site.register(OTP)
admin.site.register(Notification)
admin.site.register(Instructor)
admin.site.register(Course,CourseAdmin)
admin.site.register(Lecture)
admin.site.register(Comment)
admin.site.register(Tag)
admin.site.register(Ticket)
admin.site.register(TicketMessage)
admin.site.register(Example,ExampleAdmin)
admin.site.register(FiguredOption)
admin.site.register(Thread)
admin.site.register(SubThread)
admin.site.register(ThreadReply)
admin.site.register(CourseXp)
admin.site.register(Log)
admin.site.register(Post)
admin.site.register(PostCategory)
admin.site.register(Competition)
admin.site.register(Run)
admin.site.register(TestCase)
admin.site.register(Currency)
admin.site.register(Asset)
admin.site.register(Attend)
admin.site.register(Challenge)
admin.site.register(League)
admin.site.register(LeagueRecord)
admin.site.register(SuperLeague)
admin.site.register(Webinar)
admin.site.register(WebinarEnrollment)
admin.site.register(Subject)
admin.site.register(Shortcut)
admin.site.register(TeamRequest)
admin.site.register(TeamInvite)
admin.site.register(Team)
admin.site.register(Project)
admin.site.register(Proposal)
admin.site.register(SkillTag)
admin.site.register(Achievement)
admin.site.register(Achievable)
admin.site.register(Content)
admin.site.register(ContentComment)
admin.site.register(Follow)
admin.site.register(Wallet)
admin.site.register(Result)
admin.site.register(Submission)
admin.site.register(Rule)
admin.site.register(NotificationRule)
admin.site.register(Benefit)
admin.site.register(LearnerSuperLeagueStatus)
admin.site.register(PromoCode)
