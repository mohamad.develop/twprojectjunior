import os
import urllib
import urllib.request
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import random
import uuid
from django.core.files.temp import NamedTemporaryFile
from urllib.request import urlopen
from imagekit.models import ProcessedImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from datetime import datetime

class Benefit(models.Model):
    benefitKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=200, null=True, blank=True, unique=True)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)

    lightIcon = models.ImageField(
        upload_to="benefiticons",
        max_length=1024, blank=True, null=True
    )

    darkIcon = models.ImageField(
        upload_to="benefiticons",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Benefits"

    def __str__(self):
        return str(self.title)