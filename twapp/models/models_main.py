import os
import urllib
import urllib.request
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import random
import uuid
from django.core.files.temp import NamedTemporaryFile
from urllib.request import urlopen
from imagekit.models import ProcessedImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from datetime import datetime

from .models_course_related import Benefit
from .models_tag_related import Tag, SkillTag
from .models_episode_related import Video
from .models_learner_related import Learner

class Dummy(models.Model):
    dummyKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)
    dummyTitle = models.CharField(max_length=200, null=True, blank=True, unique=True)

    class Meta:
        verbose_name_plural = "Dummy"

    def __str__(self):
        return str(self.title)




class Category(models.Model):
    categoryName = models.CharField(max_length=200, null=True, blank=True)
    categoryKey = models.CharField(max_length=100, null=True, blank=True, editable=False, unique=True,
                                   default=uuid.uuid4)
    description= models.TextField(null=True, blank=True)
    isLeaf = models.BooleanField(default=True)

    parentingNum=models.IntegerField(null=True, blank=True, default=-1)

    # Relations
    parent = models.ForeignKey("self", blank=True, null=True, on_delete=models.SET_NULL)
    icon = models.ImageField(
        upload_to="categoryicons",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return str(self.categoryName)

    def get_all_parents(self):
        r = self.categoryName
        obj = self
        while obj.parent != None:
            r = obj.parent.categoryName + " > " + r
            obj = obj.parent
        return r


class Currency(models.Model):
    currencyKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    currencyName = models.CharField(max_length=200, null=True, blank=True)

    lightIcon = models.ImageField(
        upload_to="icons",
        max_length=1024, blank=True, null=True
    )

    darkIcon = models.ImageField(
        upload_to="icons",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Currencies"

    def __str__(self):
        return str(self.currencyName)


class Asset(models.Model):
    assetKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    currency = models.ForeignKey(Currency, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    value = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name_plural = "Assets"

    def __str__(self):
        return str(self.learner.firstName) + " " + str(self.learner.lastName) + "{" + str(
            self.currency.currencyName) + "}"


class Config(models.Model):
    configKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    key = models.CharField(max_length=200, null=True, blank=True)
    value = models.TextField(null=True, blank=True)
    group = models.CharField(max_length=200, null=True, blank=True)
    side = models.CharField(max_length=200, null=True, blank=True)
    status = models.IntegerField(null=True, blank=True, default=0)
    haveFile = models.BooleanField(null=True, blank=True, default=False)
    description = models.TextField(null=True, blank=True)

    file = models.FileField(
        upload_to="configfiles",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Configs"

    def __str__(self):
        return str(self.key) + " : " + self.value


class AppSession(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)
    token = models.CharField(max_length=200, null=True, blank=True, default=uuid.uuid4)
    isActive = models.BooleanField()

    class Meta:
        verbose_name_plural = "Sessions"

    def __str__(self):
        return  self.token[:40]


class OTP(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)
    otp = models.CharField(max_length=200, null=True, blank=True, default=str(random.randint(1000, 9999)))

    class Meta:
        verbose_name_plural = "OTP LIST"

    def __str__(self):
        return str(self.user.username) + " : " + self.otp


class Example(models.Model):
    #Alias: Question
    exampleKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    subType = models.CharField(max_length=200, null=True, blank=True, default="none")  # useless
    body = models.TextField(null=True, blank=True)
    statement = models.TextField(null=True, blank=True)
    secondStatement = models.TextField(null=True, blank=True)
    before = models.TextField(null=True, blank=True)
    after = models.TextField(null=True, blank=True)
    explanatoryAnswer = models.TextField(null=True, blank=True,
                                         default="none")  # Full Text answer that shows only when question is passed
    hint = models.TextField(null=True, blank=True)
    output = models.TextField(null=True, blank=True)
    options = models.TextField(null=True, blank=True)  # Options One on each line (multi and pick types)
    answer = models.TextField(null=True, blank=True)
    language = models.TextField(null=True, blank=True, default="cpp")
    order = models.IntegerField(null=True, blank=True, default=0)
    poolExclude = models.BooleanField(null=True, blank=True, default=False)
    initCode = models.TextField(null=True, blank=True)

    testCase = models.TextField(null=True, blank=True, default="")

    score = models.IntegerField(null=True, blank=True, default=0)
    xpPrize = models.IntegerField(null=True, blank=True, default=0)

    nextKey = models.CharField(max_length=200, null=True, blank=True, default="None")
    previousKey = models.CharField(max_length=200, null=True, blank=True , default="None")
    index = models.IntegerField(null=True, blank=True, default=0)
    siblingQuestions = models.IntegerField(null=True, blank=True, default=0)


    figure = models.ImageField(
        upload_to="examplefigures",
        max_length=1024, blank=True, null=True
    )

    explanatoryFigure = models.ImageField(
        upload_to="explanatoryfigures",
        max_length=1024, blank=True, null=True
    )


    # relations
    takers = models.ManyToManyField(Learner, blank=True)
    relatedVideo = models.ForeignKey(Video, blank=True, null=True, on_delete=models.SET_NULL, related_name='EpisodeQuestions')

    def options_as_list(self):
        return self.options.split('\n')

    def blank(self):
        parts = self.statement.split('#')

        html_ = ""

        blank = False
        ind=0
        for part in parts:
            if blank:
                html_ = html_ + " " + "<input name='blank"+str(ind)+"' id='blank"+str(ind)+"' type='text' class='blank_input' >"
                ind=ind+1
            else:
                html_ = html_ + " " + part
            blank = not blank

        return html_

    class Meta:
        verbose_name_plural = "Examples"

    def __str__(self):
        return str(self.title) + " " + str(self.order)


class TestCase(models.Model):
    testCaseKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    input = models.TextField(null=True, blank=True)
    output = models.TextField(null=True, blank=True)

    initCode = models.TextField(null=True, blank=True)

    inputFile = models.FileField(
        upload_to="testcaseFiles",
        max_length=1024, blank=True, null=True
    )

    outputFile = models.FileField(
        upload_to="testcaseFiles",
        max_length=1024, blank=True, null=True
    )

    # relations
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.testCaseKey)


class FiguredOption(models.Model):
    figuredOptionKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    isTrue = models.BooleanField(null=True, blank=True, default=False)

    figure = models.FileField(
        upload_to="optionsfigures",
        max_length=1024, blank=True, null=True
    )

    # relations
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL, related_name='figOpts')

    def __str__(self):
        return str(self.figuredOptionKey)



class Run(models.Model):
    runKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    code = models.TextField(null=True, blank=True)
    compiler = models.CharField(max_length=200, null=True, blank=True, default="c++")
    verdict = models.BooleanField(null=True, blank=True, default=False)
    status = models.CharField(max_length=200, null=True, blank=True, default="running")
    results = models.CharField(max_length=200, null=True, blank=True, default="")
    userOutputs = models.TextField(null=True, blank=True, default="")

    statusChecks = models.IntegerField(null=True, blank=True, default=0) # How Many Times Status Checked for this run?

    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.runKey)


class Course(models.Model):
    courseKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    color = models.CharField(max_length=200, null=True, blank=True, default="#fcf1eb")
    description = models.TextField(null=True, blank=True)
    verified = models.BooleanField(null=True, blank=True, default=False)
    featured = models.BooleanField(null=True, blank=True, default=False)

    displayType = models.IntegerField(null=True, blank=True, default=1)
    price = models.IntegerField(null=True, blank=True, default=0)
    offPrice = models.IntegerField(null=True, blank=True, default=0)
    tax = models.IntegerField(null=True, blank=True, default=0)

    defaultLanguage = models.CharField(max_length=200, null=True, blank=True, default="en")

    # relations
    supportInstructor = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)
    icon = models.ImageField(
        upload_to="courseicons",
        max_length=1024, blank=True, null=True
    )

    cover = models.ImageField(
        upload_to="coursecovers",
        max_length=1024, blank=True, null=True
    )


    #relations
    tags = models.ManyToManyField(Tag, blank=True)
    likers = models.ManyToManyField(Learner, blank=True)
    benefits = models.ManyToManyField(Benefit, blank=True)

    # examples = models.ManyToManyField(Example, blank=True)\

    def learners_count(self):
        try:
            leaners_cnt = 0
            lectures = Lecture.objects.all().filter(parentCourse=self)
            for lec in lectures:
                for ex in lec.examples.all():
                    leaners_cnt = leaners_cnt + ex.takers.count()
            return leaners_cnt
        except:
            return 0

    def __str__(self):
        return str(self.title)


class Challenge(models.Model):
    # Type = 11
    challengeKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    subtitle = models.CharField(max_length=200, null=True, blank=True)
    statement = models.TextField(null=True, blank=True)
    secondStatement = models.TextField(null=True, blank=True)
    hint = models.TextField(null=True, blank=True)
    gift = models.IntegerField(null=True, blank=True, default=0)
    company = models.TextField(null=True, blank=True)

    views = models.IntegerField(null=True, blank=True, default=0)

    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    startTime = models.DateTimeField(null=True)
    endTime = models.DateTimeField(null=True)

    status = models.IntegerField(null=True, blank=True, default=0)  # 0-new 1-upcoming 2-active 3-finished 4-archived

    icon = models.ImageField(
        upload_to="challengeicons",
        max_length=1024, blank=True, null=True
    )

    cover = models.ImageField(
        upload_to="challengecovers", default='/challengecovers/default.jpg',
        max_length=1024, blank=True, null=True
    )

    # relations
    course = models.ManyToManyField(Course, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Challenges"

    def __str__(self):
        return str(self.title)


class Webinar(models.Model):
    # Type = 19
    webinarKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    presenters = models.CharField(max_length=200, null=True, blank=True)

    description = models.TextField(null=True, blank=True)

    price = models.IntegerField(null=True, blank=True, default=0)
    tax = models.IntegerField(null=True, blank=True, default=0)

    gemPrice = models.IntegerField(null=True, blank=True, default=0)

    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    startTime = models.DateTimeField(null=True)
    endTime = models.DateTimeField(null=True)

    streamUrl = models.CharField(max_length=200, null=True, blank=True)

    status = models.IntegerField(null=True, blank=True, default=0)  # 0-new 1-upcoming 2-active 3-finished

    cover = models.ImageField(
        upload_to="challengeicons",
        max_length=1024, blank=True, null=True
    )

    # relations
    # course = models.ManyToManyField(Course , blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Webinars"

    def __str__(self):
        return str(self.title)


class WebinarEnrollment(models.Model):
    webinarEnrollmentKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)

    title = models.CharField(max_length=200, null=True, blank=True)
    paidPrice = models.IntegerField(null=True, blank=True, default=0)

    # relations

    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    webinar = models.ForeignKey(Webinar, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Webinar Enrollments"

    def __str__(self):
        return str(self.learner)+" in "+str(self.webinar.title)


class Subject(models.Model):
    # Type = 13
    subjectKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)

    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    price = models.IntegerField(null=True, blank=True, default=0)
    actualPrice = models.IntegerField(null=True, blank=True, default=0)
    tax = models.IntegerField(null=True, blank=True, default=0)

    gemPrice = models.IntegerField(null=True, blank=True, default=0)
    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    endTime = models.DateTimeField(null=True)
    status = models.IntegerField(null=True, blank=True, default=0)  # 0-past 1-active

    lessonsCount=models.IntegerField(null=True, blank=True, default=0)
    episodeCount=models.IntegerField(null=True, blank=True, default=0)
    hours=models.IntegerField(null=True, blank=True, default=0)

    cover = models.ImageField(
        upload_to="packageicons",
        max_length=1024, blank=True, null=True
    )

    icon = models.ImageField(
        upload_to="packageicons",
        max_length=1024, blank=True, null=True
    )

    # relations
    # tags = models.ManyToManyField(Tag, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)
    courses = models.ManyToManyField(Course, blank=True)

    class Meta:
        verbose_name_plural = "Subjects"

    def __str__(self):
        return str(self.title)


class Attend(models.Model):
    attendKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    lessonCompleted = models.IntegerField(null=True, blank=True, default=0)
    unlockedLessonOrder= models.IntegerField(null=True, blank=True, default=0) #useless
    unlockedEpisodeOrder= models.IntegerField(null=True, blank=True, default=0) #useless
    unlockedQuestionOrder= models.IntegerField(null=True, blank=True, default=0) #useless

    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.learner.user.username) + " in " + str(self.course.title)


class Shortcut(models.Model):
    shortcutKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    questionCount = models.IntegerField(null=True, blank=True, default=0)
    neededToPass = models.IntegerField(null=True, blank=True, default=0)
    offAmount = models.FloatField(null=True, blank=True, default=0.0)

    def __str__(self):
        return str(self.shortcutKey)




class Lecture(models.Model):
    lectureKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    lectureNote = models.TextField(null=True, blank=True)
    views = models.IntegerField(null=True, blank=True, default=0)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")

    order = models.IntegerField(null=True, blank=True, default=0)

    # relations
    # video = models.ForeignKey(Media, blank=True, null=True, on_delete=models.SET_NULL)
    parentCourse = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL, related_name='courseLectures')
    tags = models.ManyToManyField(Tag, blank=True)
    likers = models.ManyToManyField(Learner, blank=True)
    examples = models.ManyToManyField(Example, blank=True)
    episodes = models.ManyToManyField(Video, blank=True)
    shortcut = models.ForeignKey(Shortcut, blank=True, null=True, on_delete=models.SET_NULL)

    def get_example_count(self):
        try:
            return self.examples.all()[self.examples.count() - 1].order + 1
        except:
            return 0

    def get_dir(self):
        if self.parentCourse.defaultLanguage=='fa':
            return "rtl"
        return "ltr"

    def __str__(self):
        return str(self.title)


class ShortcutTry(models.Model):
    shortcutTryKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    score = models.IntegerField(null=True, blank=True, default=0)
    finalResult = models.CharField(max_length=200, null=True, blank=True)


    shortcut = models.ForeignKey(Shortcut, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    lesson = models.ForeignKey(Lecture, blank=True, null=True, on_delete=models.SET_NULL)


    def __str__(self):
        return str(self.shortcutTryKey)

class Instructor(models.Model):
    instructorKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='instructor')
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)

    blogPermission = models.BooleanField(null=True, blank=True, default=False)
    allCoursesPermission = models.BooleanField(null=True, blank=True, default=False)


    bio = models.TextField(null=True, blank=True)

    profilePicture = models.ImageField(
        upload_to="profilepics",
        max_length=1024, blank=True, null=True
    )

    defaultLanguage = models.CharField(max_length=20, default='en')

    # relations
    courses = models.ManyToManyField(Course, blank=True)

    def __str__(self):
        return str(self.firstName + " " + self.lastName)


class Comment(models.Model):
    commentKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    message = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")

    # relations
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    lecture = models.ForeignKey(Lecture, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)
    replays = models.ManyToManyField("self", blank=True)

    def __str__(self):
        return str(self.message)


class Ticket(models.Model):
    ticketKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=300, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    answer = models.TextField(null=True, blank=True) #changed to ticket messages
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")

    type = models.CharField(max_length=100, null=True, blank=True, default="instructor") #instructor, general, support


    attachment = models.FileField(
        upload_to="attachments",
        max_length=1024, blank=True, null=True
    )

    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    lecture = models.ForeignKey(Lecture, blank=True, null=True, on_delete=models.SET_NULL)
    instructor = models.ForeignKey(Instructor, blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.title)


class TicketMessage(models.Model):
    ticketMessageKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    message = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="unread") #unread, read
    side = models.CharField(max_length=100, null=True, blank=True, default="message") #message (when client sent), answer (when the instructor or system sent)
    time = models.DateTimeField(auto_now_add=True)

    attachment = models.FileField(
        upload_to="attachments",
        max_length=1024, blank=True, null=True
    )

    # relations
    ticket = models.ForeignKey(Ticket, blank=True, null=True, on_delete=models.SET_NULL)

class Response(models.Model):
    responseKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    responseArray = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.learner) + "On" + str(self.example)


class ThreadReply(models.Model):
    #Answer in discuss part
    replyKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    body = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    upVotes = models.IntegerField(null=True, blank=True, default=0)
    views = models.IntegerField(null=True, blank=True, default=0)
    downVotes = models.IntegerField(null=True, blank=True, default=0)

    threadFile = models.FileField(
        upload_to="threadFiles",
        max_length=1024, blank=True, null=True
    )

    # relations
    tags = models.ManyToManyField(Tag, blank=True)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)

    def getVotes(self):
        return self.upVotes - self.downVotes

    def __str__(self):
        return str(self.replyKey)


class SubThread(models.Model):
    #Answer in discuss part
    subthreadKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    body = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    upVotes = models.IntegerField(null=True, blank=True, default=0)
    views = models.IntegerField(null=True, blank=True, default=0)
    downVotes = models.IntegerField(null=True, blank=True, default=0)

    threadFile = models.FileField(
        upload_to="threadFiles",
        max_length=1024, blank=True, null=True
    )

    # relations
    tags = models.ManyToManyField(Tag, blank=True)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    replies = models.ManyToManyField(ThreadReply, blank=True)

    def getVotes(self):
        return self.upVotes - self.downVotes

    def __str__(self):
        return str(self.subthreadKey)


class Thread(models.Model):
    #Question in discuss part
    threadKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=300, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")
    views = models.IntegerField(null=True, blank=True, default=0)
    upVotes = models.IntegerField(null=True, blank=True, default=0)
    downVotes = models.IntegerField(null=True, blank=True, default=0)

    threadFile = models.FileField(
        upload_to="threadFiles",
        max_length=1024, blank=True, null=True
    )

    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    tags = models.ManyToManyField(Tag, blank=True)
    subthreads = models.ManyToManyField(SubThread, blank=True)

    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)

    def getVotes(self):
        return self.upVotes - self.downVotes

    def __str__(self):
        return str(self.threadKey)


class CourseXp(models.Model):
    earnKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=get_random_string)
    xp = models.IntegerField(null=True, blank=True, default=0)
    time = models.DateTimeField(auto_now_add=True, null=True)
    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Earns"


class PostCategory(models.Model):
    categoryName = models.CharField(max_length=200, null=True, blank=True)
    postCategoryKey = models.CharField(max_length=100, null=True, blank=True, unique=True,
                                       default=get_random_string)

    class Meta:
        verbose_name_plural = "Post Categories"

    def __str__(self):
        return str(self.categoryName)


class Post(models.Model):
    postKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=get_random_string)
    title = models.CharField(max_length=300, null=True, blank=True)
    duration = models.CharField(max_length=300, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")
    views = models.IntegerField(null=True, blank=True, default=0)

    # relations
    category = models.ManyToManyField(PostCategory, blank=True)
    banner = models.ImageField(
        upload_to="banners",
        max_length=1024, blank=True, null=True
    )

    def __str__(self):
        return str(self.title)


class League(models.Model):
    # Type = 14
    leagueKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=300, null=True, blank=True)

    level = models.IntegerField(null=True, blank=True, default=1)
    rank = models.IntegerField(null=True, blank=True, default=1)

    status = models.IntegerField(null=True, blank=True, default=1)
    # 0 learner not allowed for this league.
    # 1 learner allowed in this league. Not registered yet.
    # 2 learner registered in the league.
    # 3 league finished and learner did not participate.
    # 4 league finished and learner participated.

    hint = models.CharField(max_length=300, null=True, blank=True)

    startTime = models.DateTimeField(null=True)
    endTime = models.DateTimeField(null=True)

    nextLeagueTime = models.DateTimeField(null=True)

    # icon = models.ImageField(
    #     upload_to="leagues",
    #     max_length=1024, blank=True, null=True
    # )

    # relations
    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    participants = models.ManyToManyField(Learner, blank=True)

    def __str__(self):
        return str(self.title) + " " + str(self.level)


class LeagueRecord(models.Model):
    # Used for WeeklyLeagues results
    leagueRecordKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)

    score = models.IntegerField(null=True, blank=True, default=0)
    rank = models.IntegerField(null=True, blank=True, default=1)

    # relations
    league = models.ForeignKey(League, null=True, on_delete=models.CASCADE)
    learner = models.ForeignKey(Learner, blank=True,  on_delete=models.CASCADE)

    def __str__(self):
        return str(self.learner) + " IN " + str(self.league)


class Team(models.Model):
    teamKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=300, null=True, blank=True)
    ownerKey = models.CharField(max_length=300, null=True, blank=True)

    teamLogo = models.ImageField(
        upload_to="teams",
        max_length=1024, blank=True, null=True
    )

    # relations
    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
    learners = models.ManyToManyField(Learner, blank=True, related_name='learnerTeams')

    def __str__(self):
        return str(self.name)


class SuperLeague(models.Model):
    # Type = 15
    superLeagueKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=300, null=True, blank=True)
    hint = models.CharField(max_length=300, null=True, blank=True)
    message = models.CharField(max_length=300, null=True, blank=True)

    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    teamsCount = models.IntegerField(null=True, blank=True, default=4)
    minConnection = models.IntegerField(null=True, blank=True, default=4)
    minLevel = models.IntegerField(null=True, blank=True, default=1)
    level = models.IntegerField(null=True, blank=True, default=1)
    views = models.IntegerField(null=True, blank=True, default=1)

    status = models.IntegerField(null=True, blank=True,
                                 default=1)  # 0-New 1-Finished without participate 2-Finished and participate 4-Not allowed

    startTime = models.DateTimeField(null=True)
    endTime = models.DateTimeField(null=True)

    icon = models.ImageField(
        upload_to="leagues",
        max_length=1024, blank=True, null=True
    )

    # relations
    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
    teams = models.ManyToManyField(Team, blank=True, related_name='teamSuperLeague')

    def __str__(self):
        return str(self.title)

class LearnerSuperLeagueStatus(models.Model):
    # make statuses more fast
    learnerSuperLeagueStatusKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    status = models.IntegerField(null=True, blank=True, default=0)

    learner = models.ForeignKey(Learner, null=True, on_delete=models.CASCADE)
    superLeague = models.ForeignKey(SuperLeague, null=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.learner)+" ON "+str(self.superLeague)+" status="+str(self.status)

class TeamRequest(models.Model):
    teamRequestKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=300, null=True, blank=True)
    status = models.CharField(max_length=300, null=True, blank=True, default='new')

    # relations
    learner = models.ForeignKey(Learner, null=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.learner)+" requested to "+str(self.team)+" | "+str(self.status)



class TeamInvite(models.Model):
    teamInviteKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=300, null=True, blank=True)
    status = models.CharField(max_length=300, null=True, blank=True, default='new')

    # relations
    learner = models.ForeignKey(Learner, null=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.learner)+" invited to "+str(self.team)+" | "+str(self.status)

class Project(models.Model):
    projectKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    creationTime = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    minBudget = models.IntegerField(null=True, blank=True, default=0)
    maxBudget = models.IntegerField(null=True, blank=True, default=0)

    dueTime = models.DateTimeField(blank=True, null=True)

    status = models.IntegerField(null=True, blank=True, default=0)  # 0-new 1-open 2-working 3-finished 4-rejected
    statusMessage = models.TextField(null=True, blank=True)

    attachment = models.FileField(
        upload_to="projectattachments",
        max_length=1024, blank=True, null=True
    )

    # relations
    # course = models.ManyToManyField(Course , blank=True)
    tags = models.ManyToManyField(SkillTag, blank=True)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Projects"

    def __str__(self):
        return str(self.title)

class Proposal(models.Model):
    proposalKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    description = models.TextField(null=True, blank=True)
    bid = models.IntegerField(null=True, blank=True, default=0)
    duration = models.IntegerField(null=True, blank=True, default=0)
    creationTime = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    # Relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    project = models.ForeignKey(Project, blank=True, null=True, on_delete=models.SET_NULL)
    status = models.IntegerField(null=True, blank=True, default=0)  # 0-new 1-viewed 2-accepted 3-rejected 4-done

    class Meta:
        verbose_name_plural = "Proposals"

    def __str__(self):
        return str(self.learner) + " on " + str(self.project) + " Bid:" + str(self.bid)


class Achievable(models.Model):
    achievableKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)  # "level" or "badge"
    general = models.BooleanField(null=True, blank=True, default=False)

    defaultIcon = models.ImageField(
        upload_to="achievements",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Achievables"

    def __str__(self):
        return str(self.title)


class Achievement(models.Model):
    achievementKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    value = models.CharField(max_length=200, null=True, blank=True, default="0")

    achievable = models.ForeignKey(Achievable, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)

    icon = models.ImageField(
        upload_to="achievements",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Achievements"

    def __str__(self):
        return str(self.achievable) + " : " + str(self.learner) + " : " + str(self.value)


class Content(models.Model):
    contentKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)
    time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    owner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    masterContent = models.BooleanField(null=True, blank=True, default=False)
    featured = models.BooleanField(null=True, blank=True, default=False)

    contentLanguage = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True, default="new")

    contentType = models.CharField(max_length=200, null=True, blank=True, default="image")
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    views = models.IntegerField(null=True, blank=True, default=0)
    likeCount = models.IntegerField(null=True, blank=True, default=0)

    hasVideo = models.BooleanField(null=True, blank=True, default=False)
    hasAudio = models.BooleanField(null=True, blank=True, default=False)


    contentFile = models.FileField(
        upload_to="contentFiles",
        max_length=1024, blank=True, null=True
    )

    contentVideo = models.FileField(
        upload_to="contentFiles",
        max_length=1024, blank=True, null=True
    )

    contentAudio = models.FileField(
        upload_to="contentFiles",
        max_length=1024, blank=True, null=True
    )

    coverImage = models.ImageField(upload_to='contentFiles', blank=True)

    tags = models.ManyToManyField(Tag, blank=True)

    def save(self, *args, **kwargs):
        super(Content, self).save(*args, **kwargs)
        if self.contentFile:
            # Open the contentFile using Pillow
            image = Image.open(self.contentFile)

            # Generate a thumbnail with max size of 1000 pixels (width or height)
            width, height = image.size
            max_size = 600
            if width > height:
                if width > max_size:
                    ratio = max_size / width
                    new_height = int(height * ratio)
                    image = image.resize((max_size, new_height))
            else:
                if height > max_size:
                    ratio = max_size / height
                    new_width = int(width * ratio)
                    image = image.resize((new_width, max_size))

            # Create a BytesIO object to write the thumbnail image data to
            thumb_io = BytesIO()

            # Save the thumbnail image to the BytesIO object in JPEG format with 80% quality
            image.save(thumb_io, format='JPEG', quality=60)

            # Create a new InMemoryUploadedFile object for the thumbnail image
            thumb_file = InMemoryUploadedFile(thumb_io, None, 'thumbnail.jpg', 'image/jpeg', thumb_io.tell(), None)

            # Save the thumbnail file object in the coverImage field
            self.coverImage.save('thumbnail.jpg', thumb_file, save=False)

            # Save the model instance again to update the coverImage field
            super(Content, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Contents"

    def __str__(self):
        return str(self.title)


class ContentComment(models.Model):
    contentCommentKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    message = models.TextField(null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")

    # relations
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    content = models.ForeignKey(Content, blank=True, null=True, on_delete=models.SET_NULL)
    replays = models.ManyToManyField("self", blank=True)

    def __str__(self):
        return str(self.message)



class Follow(models.Model):
    followKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)

    followerKey = models.CharField(max_length=200, null=True, blank=True)
    followedKey = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Follows"

    def __str__(self):
        return str(self.followKey)


class Wallet(models.Model):
    walletKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    deposit = models.FloatField(null=True, blank=True, default=0.0)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Wallets"

    def __str__(self):
        return str(self.learner) + " (" + str(self.deposit) + ")"


class Competition(models.Model):
    # Type = 16
    competitionKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)
    status = models.IntegerField(null=True, blank=True, default=0)  # 0-New 1-Competing 2-Finished

    questionKeys = models.TextField(null=True, blank=True, default="")

    userStarted = models.BooleanField(null=True, blank=True, default=False)
    opponentStarted = models.BooleanField(null=True, blank=True, default=False)

    userPoint = models.IntegerField(null=True, blank=True, default=0)
    opponentPoint = models.IntegerField(null=True, blank=True, default=0)

    userLastQuestionKey=models.CharField(max_length=200, null=True, blank=True)
    opponentLastQuestionKey=models.CharField(max_length=200, null=True, blank=True)

    opponentKey = models.CharField(max_length=200, null=True, blank=True)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)

    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    questions = models.ManyToManyField(Example, blank=True)

    class Meta:
        verbose_name_plural = "Competitions"

    def __str__(self):
        return "Learner:"+str(self.learner)+" Course:"+str(self.course)+" Category:"+str(self.category)+" OPP:"+str(self.opponentKey)


class Result(models.Model):
    # Used for SuperLeagues results
    resultKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)

    currentRank = models.IntegerField(null=True, blank=True, default=0)
    rankChangedCount = models.IntegerField(null=True, blank=True, default=0)
    rankStatus = models.IntegerField(null=True, blank=True, default=0)
    score = models.IntegerField(null=True, blank=True, default=0)

    status = models.IntegerField(null=True, blank=True, default=0)

    superLeague = models.ForeignKey(SuperLeague, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL) #useless
    team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "Results"

    def __str__(self):
        return str(self.learner) + " in " + str(self.superLeague)


class Submission(models.Model):
    submissionKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)
    description = models.TextField(null=True, blank=True)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    challenge = models.ForeignKey(Challenge, blank=True, null=True, on_delete=models.SET_NULL, related_name="challengeSubmissions")
    score = models.IntegerField(null=True, blank=True, default=0)


    attachment = models.FileField(
        upload_to="submissionattachmenets",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Submissions"

    def __str__(self):
        return str(self.learner) + " on "+ str(self.challenge)


class Rule(models.Model):
    ruleKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    entity = models.CharField(max_length=200, null=True, blank=True)
    target = models.CharField(max_length=200, null=True, blank=True)
    property = models.CharField(max_length=200, null=True, blank=True)
    neededAmount = models.IntegerField(null=True, blank=True, default=0)
    general = models.BooleanField(null=True, blank=True, default=False)

    lowerBound = models.IntegerField(null=True, blank=True, default=0)
    upperBound = models.IntegerField(null=True, blank=True, default=0)
    givenValue = models.IntegerField(null=True, blank=True, default=0)

    message = models.TextField(null=True, blank=True)

    achievable = models.ForeignKey(Achievable, blank=True, null=True, on_delete=models.SET_NULL)


    defaultIcon = models.ImageField(
        upload_to="notificationicons",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Rules"

    def __str__(self):
        return str(self.title)


class Log(models.Model):
    logKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    action = models.CharField(max_length=200, null=True, blank=True)
    ipAddress = models.CharField(max_length=200, null=True, blank=True)
    time = models.TimeField(auto_now_add=True, null=True, blank=True)
    date = models.DateField(auto_now_add=True, null=True, blank=True)
    integer_value = models.IntegerField(null=True, blank=True, default=0)

    # relations
    instructor = models.ForeignKey(Instructor, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL)
    video = models.ForeignKey(Video, blank=True, null=True, on_delete=models.SET_NULL)
    content = models.ForeignKey(Content, blank=True, null=True, on_delete=models.SET_NULL)
    thread = models.ForeignKey(Thread, blank=True, null=True, on_delete=models.SET_NULL)
    subThread = models.ForeignKey(SubThread, blank=True, null=True, on_delete=models.SET_NULL)
    threadReply = models.ForeignKey(ThreadReply, blank=True, null=True, on_delete=models.SET_NULL)
    lecture = models.ForeignKey(Lecture, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.action) + " " + str(self.date) + " " + str(self.time)

    class Meta:
        verbose_name_plural = "Logs"


class PromoCode(models.Model):
    promoCodeKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    token = models.CharField(max_length=200, null=True, blank=True)
    percentage = models.IntegerField(null=True, blank=True, default=0)
    status = models.CharField(max_length=100, null=True, blank=True, default="new")

    def __str__(self):
        return str(self.token) + " " + str(self.percentage) + " " + str(self.status)


class Notification(models.Model):
    notificationKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    toUser = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=200, null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    read = models.BooleanField(default=False)
    time = models.DateTimeField(auto_now_add=True)

    type = models.IntegerField(null=True, blank=True, default=0)
    code = models.IntegerField(null=True, blank=True, default=0)

    icon = models.ImageField(
        upload_to="notificons",
        max_length=1024, blank=True, null=True
    )

    # relations
    instructor = models.ForeignKey(Instructor, blank=True, null=True, on_delete=models.SET_NULL)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, blank=True, null=True, on_delete=models.SET_NULL)
    example = models.ForeignKey(Example, blank=True, null=True, on_delete=models.SET_NULL)
    video = models.ForeignKey(Video, blank=True, null=True, on_delete=models.SET_NULL)
    content = models.ForeignKey(Content, blank=True, null=True, on_delete=models.SET_NULL)
    thread = models.ForeignKey(Thread, blank=True, null=True, on_delete=models.SET_NULL)
    subThread = models.ForeignKey(SubThread, blank=True, null=True, on_delete=models.SET_NULL)
    threadReply = models.ForeignKey(ThreadReply, blank=True, null=True, on_delete=models.SET_NULL)
    lecture = models.ForeignKey(Lecture, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title

class NotificationRule(models.Model):
    notificationRuleKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True) # Notification / PopUp
    entity = models.CharField(max_length=200, null=True, blank=True) # Notification / PopUp
    target = models.CharField(max_length=200, null=True, blank=True) # Learner / Instructor
    neededAmount = models.IntegerField(null=True, blank=True, default=0)

    message = models.TextField(null=True, blank=True)

    defaultIcon = models.ImageField(
        upload_to="notificationicons",
        max_length=1024, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Notification Rules"

    def __str__(self):
        return str(self.title)

class Invoice(models.Model):
    PENDING = 'pending'
    SUCCESS = 'success'
    FAILED = 'failed'
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (SUCCESS, 'Success'),
        (FAILED, 'Failed'),
    )

    COURSE = 'course'
    SUBJECT = 'subject'
    CURRENCY = 'currency'
    CHOICES = (
        (COURSE, 'course'),
        (SUBJECT, 'subject'),
        (CURRENCY, 'currency'),
    )

    invoiceKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    learner = models.ForeignKey(Learner, blank=True, null=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, blank=True, null=True, related_name='course')
    amount = models.IntegerField()
    status = models.CharField(max_length=100, choices=STATUS_CHOICES, default=PENDING)
    create_at = models.DateTimeField(auto_now_add=True)
    promo_code = models.ForeignKey(PromoCode, on_delete=models.CASCADE, blank=True, null=True,)
    type = models.CharField(max_length=225, choices=CHOICES)
    key = models.CharField(max_length=225)


class CourseProject(models.Model):
    courseProjectKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True, blank=True, related_name='projectsOfCourse')
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField()
    deadLine = models.DateField()
    maxScore = models.IntegerField(default=0)
    createdAt = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    isDeleted = models.BooleanField(default=False)


class SubmissionCourseProject(models.Model):
    NEW = 'new'
    SCORED = 'scored'
    RIVISION = 'rivision'
    RESCORE = 'rescore'
    FINAL = 'final'
    STATUS_CHOICES = (
        (NEW, 'new'),
        (SCORED, 'scored'),
        (RIVISION, 'rivision'),
        (RESCORE, 'rescore'),
        (FINAL, 'final'),
    )


    submissionCourseProjectKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    courseProject = models.ForeignKey(CourseProject, on_delete=models.SET_NULL, null=True, blank=True,related_name='submissionProject')
    submittedBy = models.ForeignKey(Learner, on_delete=models.SET_NULL, null=True, blank=True)
    submission_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, choices=STATUS_CHOICES, default=NEW)
    point = models.IntegerField(default=0)
    similarityIndex = models.IntegerField(default=0)
    attachment = models.FileField(upload_to='submissions/')

    class Meta:
        verbose_name_plural = "Submission"

