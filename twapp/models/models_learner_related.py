import os
import urllib
import urllib.request
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import random
import uuid
from django.core.files.temp import NamedTemporaryFile
from urllib.request import urlopen
from imagekit.models import ProcessedImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from datetime import datetime
from .models_tag_related import Tag

class Learner(models.Model):
    # Type = 1
    learnerKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    gender = models.CharField(max_length=25, null=True, blank=True)
    hash = models.CharField(max_length=200, null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    isPro = models.BooleanField(default=False)
    isPrivate = models.BooleanField(default=False)
    email = models.EmailField(null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)

    pushToken= models.TextField(null=True, blank=True)

    profileViews = models.IntegerField(null=True, blank=True, default=0)

    lastStreak = models.CharField(max_length=200, null=True, blank=True)

    dailyStreak = models.IntegerField(null=True, blank=True, default=0)
    forumActivity = models.IntegerField(null=True, blank=True, default=0)
    points = models.IntegerField(null=True, blank=True, default=0)
    level = models.IntegerField(null=True, blank=True, default=0)

    settings_lang = models.CharField(max_length=4, default='en')
    profilePicture = models.ImageField(
        upload_to="profilepics",
        max_length=1024, blank=True, null=True
    )

    # profilePic=models.OneToOneField(Media,blank=True,null=True,on_delete=models.SET_NULL)
    friends = models.ManyToManyField("self", blank=True)


    profilePictureThumb = models.ImageField(upload_to='profilepics', blank=True)

    tags = models.ManyToManyField(Tag, blank=True)

    def save(self, *args, **kwargs):
        super(Learner, self).save(*args, **kwargs)
        if self.profilePicture:
            # Open the contentFile using Pillow
            image = Image.open(self.profilePicture)

            # Generate a thumbnail with max size of 1000 pixels (width or height)
            width, height = image.size
            max_size = 300
            if width > height:
                if width > max_size:
                    ratio = max_size / width
                    new_height = int(height * ratio)
                    image = image.resize((max_size, new_height))
            else:
                if height > max_size:
                    ratio = max_size / height
                    new_width = int(width * ratio)
                    image = image.resize((new_width, max_size))

            # Create a BytesIO object to write the thumbnail image data to
            thumb_io = BytesIO()

            try:
                image.save(thumb_io, format='JPEG', quality=60)
            except:
                image.save(thumb_io, format='PNG',quality=60)

            # Create a new InMemoryUploadedFile object for the thumbnail image
            thumb_file = InMemoryUploadedFile(thumb_io, None, 'thumbnail.jpg', 'image/jpeg', thumb_io.tell(), None)

            # Save the thumbnail file object in the coverImage field
            self.profilePictureThumb.save('thumbnail.jpg', thumb_file, save=False)

            # Save the model instance again to update the coverImage field
            super(Learner, self).save(*args, **kwargs)

    def __str__(self):
        return self.firstName + " " + self.lastName + " : " + self.user.username

    def type(self):
        return int(9)
