import os
import urllib
import urllib.request
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import random
import uuid
from django.core.files.temp import NamedTemporaryFile
from urllib.request import urlopen
from imagekit.models import ProcessedImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from datetime import datetime

class Tag(models.Model):
    tagKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)

    class Meta:
        verbose_name_plural = "Tags"

    def __str__(self):
        return str(self.title)


class SkillTag(models.Model):
    # Skill Tags are the tags related to freelancing projects

    skillTagKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    title = models.CharField(max_length=200, null=True, blank=True, unique=True)

    class Meta:
        verbose_name_plural = "Skill Tags"

    def __str__(self):
        return str(self.title)
