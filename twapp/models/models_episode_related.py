import os
import urllib
import urllib.request
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import random
import uuid
from django.core.files.temp import NamedTemporaryFile
from urllib.request import urlopen
from imagekit.models import ProcessedImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from datetime import datetime

from .models_tag_related import Tag


class Video(models.Model):
    # = Episode

    videoKey = models.CharField(max_length=100, null=True, blank=True, unique=True, default=uuid.uuid4)
    videoTitle = models.CharField(max_length=500)
    videoFile = models.FileField(upload_to='videos/', null=True, verbose_name="")
    videoDuration = models.CharField(max_length=100, null=True, blank=True, default="0:30:00")
    videoDescription = models.TextField(max_length=50, null=True, blank=True)
    videoOwner = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    creationTime = models.DateTimeField(auto_now_add=True, null=True)
    isFree = models.BooleanField(blank=True, null=True, default=False)

    likeCount = models.IntegerField(null=True, blank=True, default=0)

    order = models.IntegerField(null=True, blank=True, default=0)
    views = models.IntegerField(null=True, blank=True, default=0)

    cover = models.ImageField(
        upload_to="videocovers",
        max_length=1024, blank=True, null=True
    )

    videoTags = models.ManyToManyField(Tag, blank=True)

    hls_created = models.BooleanField(blank=True, null=True, default=False)
    hls_url = models.CharField(max_length=500, default="")


    def __str__(self):
        return self.videoTitle + ": " + str(self.videoFile) + "  order:" + str(self.order)