"""
ASGI config for TWProject project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from django.urls import path

from twapp.views import WebinarViewerConsumer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'TWProject.settings')



application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            [
                path("ws/webinar/<str:webinar_key>/", WebinarViewerConsumer.as_asgi()),
                # Other WebSocket consumers here if needed
            ]
        )
    ),
})
